<?php

namespace app\controllers;

use yii;
use app\components\CommonController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use app\models\Transfers;
use app\models\Voucher;
use app\models\Flights;
use app\models\Activity;
use app\models\Car;
use app\models\Email;
use app\models\Supplier;
use app\models\SupplierTourName;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;
use yii\helpers\Url;

/**
 * Default controller for the `admin` module
 */
class SupplierController extends CommonController {

    /**
     * opens the List of all the not deleted vouchers in the system
     * @return string
     * For update of Supplier
     */
    public function actionIndex() {
        $model = Supplier::find()->where(['<>', 'status', '2'])->orderBy(['id' => SORT_DESC])->all();

        return $this->render('index', ['model' => $model]);
    }

    /**
     * 
     * @param type $id
     * @return type For Create of supplier 
     */
    public function actionCreate() {
        $supply = new Supplier(['scenario' => 'create']);
        $supplytour = new SupplierTourName(['scenario' => 'create']);
        if (Yii::$app->request->isPost) {
            $supply->attributes = $_POST['Supplier'];
            $supply->created_at = date('Y-m-d H:i:s');
            $supply->status = '1';
            if ($supply->save()) {
                if (isset($_POST['SupplierTourName'])) {
                    foreach ($_POST['SupplierTourName'] as $valueSupplierTourName) {
                        $supplytour = new SupplierTourName();
                        $supplytour->attributes = $valueSupplierTourName;
                        $supplytour->supplier_id = $supply->id;
                        $supplytour->save();
                    }

                    Yii::$app->session->setFlash('Suppliercreate', 'SupplierSubmitted');
                    return $this->redirect(['index']);
                }
            }
        }
        return $this->render('create', ['model' => $supply, 'model1' => $supplytour]);
    }

    /**
     * 
     * @param type $id
     * @return type For Edit of supplier 
     */
    public function actionEdit($id) {
        $data = Supplier::find()
                ->where(['id' => $id])
                ->one();
        $tour = SupplierTourName::find()
                ->where(['supplier_id' => $id])
                ->one();
        if (Yii::$app->request->isPost) {
// $data->scenario = 'update_supplier';
            $data->load(Yii::$app->request->post());
            $data->updated_at = date('Y-m-d H:i:s');
            if ($data->save()) {
                foreach ($data->suppliertour as $index => $value) {
                    $value->delete();
                }
                foreach ($_POST['SupplierTourName'] as $valueSupplierTourName) {
                    $supplytour = new SupplierTourName();
                    $supplytour->attributes = $valueSupplierTourName;
                    $supplytour->supplier_id = $data->id;
                    $supplytour->save();
                }
                Yii::$app->getSession()->setFlash('Suppliercreate', 'updated sucessfully');
                return $this->redirect(['index']);
            }
        }
        return $this->render('edit', [
                    'model' => $data, 'model1' => $tour]);
    }

    /**
     * 
     * @param type $id
     * @return type For view of supplier 
     */
    public function actionView($id) {
        $model = Supplier::find()
                ->where(['id' => $id])
                ->one();
        return $this->render('view', ['model' => $model]);
    }

    /**
     * 
     * @param type $id
     * @delete for supplier
     */
    public function actionDelete($id) {
        $model = Supplier::find()
                ->where(['id' => $id])
                ->one();
        if ($model->delete()) {
            
        }
        return $this->redirect(Url::to(['index']));
    }

    /**
     * add view as per need in the add voucher section form
     */
    public function actionAjaxTourAdd() {

        if (Yii::$app->request->isAjax) {
            $counter = $_POST['counter'] ?? '';
            echo $this->renderPartial('add_tours_ajax', ['counter' => $counter]);
            exit();
        } else {
            $response = [
                'status' => 'FAILED',
                'code' => '-99'
            ];
        }
        echo json_encode($response);

        exit();
    }

}
