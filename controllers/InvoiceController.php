<?php

namespace app\controllers;

use app\models\InvoiceIncoming;
use app\models\InvoiceOutgoing;
use app\components\CommonController;
use yii\helpers\ArrayHelper;
use app\models\Transfers;
use app\models\Voucher;
use app\models\Flights;
use app\models\IncomingInvoiceParticulars;
use app\models\SupplierTourName;
use app\models\Activity;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii;

class InvoiceController extends \app\components\CommonController {

    /**
     * create incoming invoice from admin pannel for each invoice admin gets.
     * @return type
     */
    public function actionIncomingInvoiceCreate() {


        $invoice = new InvoiceIncoming();
        $invoice_particulars = new IncomingInvoiceParticulars();
        if (Yii::$app->request->isPost) {

            $invoice->attributes = $_POST['InvoiceIncoming'];
            $invoice->created_at = date('Y-m-d H:i:s');

            $invoice->due_date = date('Y-m-d', strtotime($_POST['InvoiceIncoming']['due_date']));
            $invoice->issue_date = date('Y-m-d', strtotime($_POST['InvoiceIncoming']['issue_date']));
            $invoice->status = '1';
            $invoice->file = UploadedFile::getInstance($invoice, 'file');

            if ($invoice->file) {
                $invoice->file->saveAs('uploads/invoice/incoming/' . $invoice->file->baseName . '.' . $invoice->file->extension);
                $invoice->upload_invoice = $invoice->file->baseName . '.' . $invoice->file->extension;
                $invoice->file = null;
            }
            if ($invoice->save()) {
                foreach ($_POST['IncomingInvoiceParticulars'] as $valueInvoiceParticulars) {
                    $invoiceParticulars = new IncomingInvoiceParticulars();
                    $invoiceParticulars->attributes = $valueInvoiceParticulars;
                    $invoiceParticulars->incoming_invoice_id = $invoice->id;
                    $invoiceParticulars->service_date = date('Y-m-d', strtotime(str_replace('/','-',$valueInvoiceParticulars['service_date'])));
                    $invoiceParticulars->created_at = date('Y-m-d H:i:s');
                    $invoiceParticulars->status = '1';
                    $invoiceParticulars->save();
                    // echo '<pre>';
                    // print_r(date('Y-m-d', strtotime(str_replace('/','-',$valueInvoiceParticulars['service_date'])))); exit();
                }
                Yii::$app->session->setFlash('InvoiceSubmitted', 'The Invoice saved Successfully.');

                return $this->redirect(['incoming-invoice-index']);
            }
        }
        $tourname = SupplierTourName::find()->all();
        $model_tour_name = ArrayHelper::map($tourname, 'id', 'tourname');
        return $this->render('create_incoming', ['model' => $invoice, 'invoice_particulars' => $invoice_particulars, 'model_tour_name' => $model_tour_name]);
    }

    /**
     * view a table of all  incoming index.
     * @return type
     */
    public function actionIncomingInvoiceIndex() {
        $model = new InvoiceIncoming();

        $data = InvoiceIncoming::find()
                ->where(['<>', 'status', '2'])
                ->orderBy('id')
                ->all();

        return $this->render('index_incoming', [
                    'model' => $data,]);
    }

    /**
     * view details of a incoming invoice
     * @param type $id
     * @return type
     */
    public function actionIncomingInvoiceView($id) {
        $model = InvoiceIncoming::find()
                ->where(['id' => $id])
                ->one();
        return $this->render('view_incoming', ['model' => $model]);
    }

    public function actionIncomingInvoiceEdit($id) {
        $data = InvoiceIncoming::find()
                ->where(['id' => $id])
                ->one();

        if (Yii::$app->request->isPost) {

            $data->issue_date = isset($_POST['InvoiceIncoming']['issue_date']) && !empty($_POST['InvoiceIncoming']['issue_date']) ? date('Y-m-d', strtotime($_POST['InvoiceIncoming']['issue_date'])) : $data->issue_date;
            $data->due_date = isset($_POST['InvoiceIncoming']['due_date']) && !empty($_POST['InvoiceIncoming']['due_date']) ? date('Y-m-d', strtotime($_POST['InvoiceIncoming']['due_date'])) : $data->due_date;


            $data->updated_at = date('Y-m-d H:i:s');
            $data->file = UploadedFile::getInstance($data, 'file');

            if ($data->load(Yii::$app->request->post()) && $data->save() && $data->file && $data->validate()) {
                $data->file->saveAs('uploads/invoice/incoming/' . $data->file->baseName . '.' . $data->file->extension);
                $data->upload_invoice = $data->file->baseName . '.' . $data->file->extension;
                $data->file = null;
            }
            if ($data->save()) {

                foreach ($data->invoiceParticulars as $index => $value) {
                    $value->delete();
                }

                foreach ($_POST['IncomingInvoiceParticulars'] as $valueInvoiceParticulars) {

                    $invoiceParticulars = new IncomingInvoiceParticulars();
                    $invoiceParticulars->incoming_invoice_id = $data->id;
                    $invoiceParticulars->created_at = date('Y-m-d H:i:s');
                    $invoiceParticulars->status = '1';
                    if (isset($valueInvoiceParticulars['id']) && !empty($valueInvoiceParticulars['id']) && $data->oneParticular($valueInvoiceParticulars['id']) !== NULL) {
                        $invoiceParticulars->attributes = $data->oneParticular($valueInvoiceParticulars['id']);
                    }
                    $invoiceParticulars->attributes = $valueInvoiceParticulars;
                    $invoiceParticulars->service_date = date('Y-m-d', strtotime(str_replace('/','-',$valueInvoiceParticulars['service_date'])));
                    $invoiceParticulars->updated_at = date('Y-m-d H:i:s');
                    if ($invoiceParticulars->save()) {
                        
                    }
                    if (isset($valueInvoiceParticulars['id']) && !empty($valueInvoiceParticulars['id']) && $data->oneParticular($valueInvoiceParticulars['id']) !== NULL) {
                        $data->oneParticular($valueInvoiceParticulars['id'])->delete();
                    }
                }

                Yii::$app->getSession()->setFlash('message', 'updated sucessfully');
                return $this->redirect(['incoming-invoice-index']);
            }
        }
        $tourname = SupplierTourName::find()->all();
        $model_tour_name = ArrayHelper::map($tourname, 'id', 'tourname');
        return $this->render('update_incoming', ['model' => $data, 'model_tour_name' => $model_tour_name]);




    }

    /**
     * 
     * @return type
     */
    public function actionOutgoingInvoiceCreate() {
        $invoice = new InvoiceOutgoing();
        if (Yii::$app->request->isPost) {
            $invoice->attributes = $_POST['InvoiceOutgoing'];
            $invoice->created_at = date('Y-m-d H:i:s');
            $invoice->tourdate = date('Y-m-d', strtotime($_POST['InvoiceOutgoing']['tourdate']));

            $invoice->due_date = date('Y-m-d', strtotime($_POST['InvoiceOutgoing']['due_date']));
            $invoice->issue_date = date('Y-m-d', strtotime($_POST['InvoiceOutgoing']['issue_date']));
            $invoice->status = '1';
            $invoice->file = UploadedFile::getInstance($invoice, 'file');
            if ($invoice->file && $invoice->validate()) {
                $invoice->file->saveAs('uploads/invoice/outgoing/' . $invoice->file->baseName . '.' . $invoice->file->extension);
                $invoice->upload_invoice = $invoice->file->baseName . '.' . $invoice->file->extension;
                $invoice->file = null;
                if ($invoice->save()) {
                    Yii::$app->session->setFlash('outgoingSubmitted', 'The Invoice saved Successfully.');
                    return $this->redirect(['outgoing-invoice-view']);
                }
            }
        }
        return $this->render('outgoingcreate', ['model' => $invoice]);
    }

    /**
     * 
     * @return type
     */

    /**
     * 
     * @return type
     * 
     */
    public function actionOutgoingInvoiceView() {
        $model = new InvoiceOutgoing();

        $data = InvoiceOutgoing::find()
                ->where(['<>', 'status', '2'])
                ->orderBy('id')
                ->all();
        /* echo'<pre>';
          print_r($data);
          echo'</pre>';
          exit(); */
        return $this->render('outgoingview', [
                    'model' => $data,]);
    }

    /**
     * 
     * invoice view
     * 
     */

    /**
     * 
     * out view
     * 
     */
    public function actionOutView($id) {
        $model = InvoiceOutgoing::find()
                ->where(['id' => $id])
                ->one();
        return $this->render('view', ['model' => $model]);
    }

    public function actionIncomingInvoiceDelete($id) {
        $model = InvoiceIncoming::find()
                ->where(['id' => $id])
                ->one();
        $model->status = '2';
        $model->update(false);

        return $this->redirect(['incoming-invoice-view']);
    }

    public function actionOutgoingInvoiceDelete($id) {
        $model = InvoiceOutgoing::find()
                ->where(['id' => $id])
                ->one();
        $model->status = '2';
        $model->update(false);

        return $this->redirect(['outgoing-invoice-view']);
    }

    public function actionOutgoingInvoiceEdit($id) {
        $data = InvoiceOutgoing::find()
                ->where(['id' => $id])
                ->one();
        $data->update();
        if (Yii::$app->request->isPost) {
            $data->due_date = date('Y-m-d', strtotime($_POST['InvoiceOutgoing']['due_date']));
            $data->issue_date = date('Y-m-d', strtotime($_POST['InvoiceOutgoing']['issue_date']));
        }
        $data->updated_at = date('Y-m-d');
        $data->file = UploadedFile::getInstance($data, 'file');
        if ($data->load(Yii::$app->request->post()) && $data->save() && $data->file && $data->validate()) {
            $data->file->saveAs('uploads/invoice/outgoing/' . $data->file->baseName . '.' . $data->file->extension);
            $data->upload_invoice = $data->file->baseName . '.' . $data->file->extension;
            $data->file = null;
            if ($data->save()) {
                Yii::$app->getSession()->setFlash('message', 'Invoice updated sucessfully');
                return $this->redirect(['outgoing-invoice-view']);
            }
        }
        return $this->render('outgoingedit', ['model' => $data,]);
    }

    public function actionAjaxAutoCompleteBookingno() {
//"suggestions": [
//{ "value": "United Arab Emirates", "data": "AE" },
// { "value": "United Kingdom", "data": "UK" },
// { "value": "United States", "data": "US" }
//]
        $query = $_GET['query'];
        $result = [
            'query' => $query,
            'suggestions' => []
        ];
        $model = Voucher::find()->where(['<>', 'status', '2'])->andWhere(['LIKE', 'billing_name', $query])->all();
        $result_raw_data = ArrayHelper::map($model, 'id', 'billing_name');
        if (!empty($result_raw_data)) {
            foreach ($result_raw_data as $index => $value) {
                $row = [
                    'value' => $value,
                    'data' => $index
                ];
                array_push($result['suggestions'], $row);
            }
        }
        echo json_encode($result, true);
        exit();
    }

    public function actionAjaxInvoiceParticularTemplateAdd() {

        if (Yii::$app->request->isAjax) {
            $counter = $_POST['counter'] ?? '';
            $tourname = SupplierTourName::find()->all();
            $model_tour_name = ArrayHelper::map($tourname, 'id', 'tourname');
            echo $this->renderPartial('add_invoice_particulars_ajax', ['counter' => $counter, 'model_tour_name' => $model_tour_name]);
            exit();
        } else {
            $response = [
                'status' => 'FAILED',
                'code' => '-99'
            ];
        }
        echo json_encode($response);

        exit();
    }

    public function actionAjaxInvoiceDetailsAdd() {
        if (Yii::$app->request->isAjax) {
            $tour = $_POST['tour'];
            $type = $_POST['type'];
            $booking_id = $_POST['booking_id'];
            $response = [];
            $model_booking = Voucher::find()->where(['id' => $booking_id])->one();
            if (!empty($model_booking)) {
                $response['adult_count'] = $model_booking->adult_count;
                $response['child_count'] = $model_booking->child_count;
                $model_tour = SupplierTourName::find()->where(['id' => $tour])->one();
                if ($type != 'PRIVATE') {
                   
                    if ($type == 'SIC') {
                        $response['amount'] = ($model_tour->sic_adult_price * $response['adult_count']) + ($model_tour->sic_child_price * $response['child_count']);
                    } else {
                        $response['amount'] = ($model_tour->ticket_adult_price * $response['adult_count']) + ($model_tour->ticket_child_price * $response['child_count']);
                    }

                    
                }
                if(isset($model_booking->activity) && !empty ($model_booking->activity)){

                    foreach($model_booking->activity as $index => $value) {
                        if($value->tour_name == $model_tour->tourname){
                                $response['service_date'] = date('d/m/Y',strtotime($value->tour_date));
                            }
                            
                        }
                    }
            }
            echo json_encode($response);
            exit();
        }
    }



}
