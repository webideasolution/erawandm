<?php

namespace app\controllers;

use yii;
use app\components\CommonController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use app\models\Transfers;
use app\models\Voucher;
use app\models\Flights;
use app\models\Activity;
use app\models\Car;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;
use yii\helpers\Url;

/**
 * Default controller for the `admin` module
 */
class CarController extends CommonController {

    /**
     * opens the List of all the not deleted Cars in the system
     * @return string
     */
    public function actionIndex() {
        $model = Car::find()->where(['<>', 'status', '2'])->orderBy(['id' => SORT_DESC])->all();
        return $this->render('index', ['model' => $model]);
    }

    public function actionCreate() {
        $car = new Car();
        if (Yii::$app->request->isPost) {
            $car->attributes = $_POST['Car'];
            $car->created_at = date('Y-m-d H:i:s');
            $car->status = '1';
            if ($car->validate()) {
                if ($car->save()) {
                    Yii::$app->session->setFlash('carSubmitted', 'The Car has been saved.');

                    return $this->redirect(['index']);
                }
            }
        }
        return $this->render('create', ['model' => $car]);
    }

    public function actionDelete($id) {
        $model = Car::find()
                ->where(['id' => $id])
                ->one();
        $model->status = '2';
        $model->update(false);

        return $this->redirect(['index']);
    }

    public function actionEdit($id) {
        $data = Car::find()
                ->where(['id' => $id])
                ->one();
        $data->update();
        $data->updated_at = date('Y-m-d H:i:s');
        if ($data->load(Yii::$app->request->post()) && $data->save() && $data->validate()) {
            if ($data->save()) {
                Yii::$app->getSession()->setFlash('message', 'Car updated sucessfully');
                return $this->redirect(['index']);
            }
        }
        return $this->render('update', ['model' => $data,]);
    }

}
