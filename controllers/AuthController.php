<?php

namespace app\controllers;

use Yii;
use app\components\CommonController;
// use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Voucher;
use yii\helpers\Url;

/**
 * Default controller for the `admin` module
 */
class AuthController extends CommonController {

    /**
     * Login action.
     *
     * @return string
     */

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'error'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['dashboard/index']);
        }

        $this->layout = 'login';
        $model = new LoginForm();
        $request = Yii::$app->request;
        if ($request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->redirect(['dashboard/index']);
            } else {

                Yii::$app->session->setFlash('user_login', 'username or password is not matched.');
            }
        }
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->redirect(['auth/login']);
    }

    public function actionViewVoucher($refId) {
        $id = str_replace('REF-', '', $refId);
        $this->layout='pdf';
        $model = Voucher::find()->where(['id' => $id])->one();
        return $this->renderPartial('../templates/mpdf_voucher_view', ['model' => $model]);
    }

}
