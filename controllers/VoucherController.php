<?php

namespace app\controllers;

use yii;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use app\components\CommonController;
use yii\base\UserException;
use app\models\Transfers;
use app\models\Voucher;
use app\models\Flights;
use app\models\Activity;
use app\models\CronemailMaster;
use app\models\SupplierTourName;

/**
 * Default controller for the `admin` module
 */
class VoucherController extends CommonController {

    /**
     * opens the List of all the not deleted vouchers in the system
     * @return string
     */
    public function actionIndex() {
        $model = Voucher::find()->where(['<>', 'status', '2'])->orderBy(['id' => SORT_DESC])->all();
        return $this->render('index', ['model' => $model]);
    }

    /**
     * delete a voucher and all related data by updating the status to 2
     * @param type $id
     */
    public function actionDelete($id) {
        $model = Voucher::find()
                ->where(['id' => $id])
                ->one();
        $model->status = '2';
        $model->updated_at = date('Y-m-d H:i:s');
        if ($model->update(false)) {
            foreach ($model->transfers as $value) {
                $value->status = '2';
                $value->updated_at = date('Y-m-d H:i:s');
                $value->update(false);
            }
            foreach ($model->activity as $value) {
                $value->status = '2';
                $value->updated_at = date('Y-m-d H:i:s');
                $value->update(false);
            }
            foreach ($model->flights as $value) {
                $value->status = '2';
                $value->updated_at = date('Y-m-d H:i:s');
                $value->update(false);
            }
        }
        $this->redirect(['index']);
    }

    /**
     * Upload a voucher in a predefined excel format so that the required data can get stored in the database.
     * @return type
     */
    public function actionExcelupload() {
        $model = new Voucher();
        if (Yii::$app->request->isPost) {
            $model->uploaded_file = UploadedFile::getInstance($model, 'uploaded_file');
            if ($model->uploaded_file->saveAs(Yii::$app->basePath . '/web/uploads/vouchers/' . $model->uploaded_file->baseName . '.' . $model->uploaded_file->extension)) {
                $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify(Yii::$app->basePath . '/web/uploads/vouchers/' . $model->uploaded_file->baseName . '.' . $model->uploaded_file->extension);
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                $reader->setReadDataOnly(true);
                $spreadsheet = $reader->load(Yii::$app->basePath . '/web/uploads/vouchers/' . $model->uploaded_file->baseName . '.' . $model->uploaded_file->extension);
                $sheetData = $spreadsheet->getActiveSheet()->toArray();
                foreach ($sheetData as $rowIndex => $sheetRow) {
                    $isBlank = true;
                    foreach ($sheetRow as $rowColumnIndex => $sheetColumn) {
                        if (!empty(trim($sheetColumn))) {
                            $isBlank = false;
                            break;
                        }
                    }
                    if ($isBlank) {
                        unset($sheetData[$rowIndex]);
                    }
                    unset($sheetData[$rowIndex][7]);
                    unset($sheetData[$rowIndex][8]);
                    unset($sheetData[$rowIndex][9]);
                    unset($sheetData[$rowIndex][10]);
                }
                $sheetData = array_values($sheetData);
                try {
                    $input['destination'] = trim(str_replace('&', ',', str_replace(':', '', str_replace('Destination', '', preg_replace('/\s+/u', '', $sheetData[0][0])))));
                    $input['name'] = trim(str_replace(':', '', str_replace('Guests Names', '', $sheetData[1][0])));
                    $input['adult'] = trim(str_replace('-', '0', str_replace(':', '', str_replace('Adult', '', str_replace('Adults', '', preg_replace('/\s+/u', '', $sheetData[2][0]))))));
                    $input['child'] = trim(str_replace('-', '0', str_replace(':', '', str_replace('Child', '', str_replace('Childs', '', str_replace('children', '', preg_replace('/\s+/u', '', $sheetData[2][1])))))));
                    $input['infant'] = str_replace('-', '0', str_replace(':', '', str_replace('Infant', '', str_replace('Infants', '', preg_replace('/\s+/u', '', $sheetData[2][2])))));
                    $input['start_date'] = str_replace('-', '0', str_replace(':', '', str_replace('-', ' ', preg_replace('/\s+/u', '', $sheetData[2][4]))));
                    $input['end_date'] = str_replace('-', '0', str_replace(':', '', str_replace('-', ' ', preg_replace('/\s+/u', '', $sheetData[2][6]))));
                    $model->scenario = 'create_voucher';
                    $model->billing_name = trim($input['name']);
                    $model->trip_start = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($input['start_date'])->format('Y-m-d');
                    $model->trip_end = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($input['end_date'])->format('Y-m-d');
                    $model->adult_count = trim($input['adult']);
                    $model->child_count = trim($input['child']);
                    $model->destination = trim($input['destination']);
                    $model->infant_count = trim($input['infant']);
                    $model->created_at = date('Y-m-d H:i:s');
                    $model->is_manual = '0';
                    $model->status = '1';
                    if ($model->save(false)) {
                        unset($sheetData[0]);
                        unset($sheetData[1]);
                        unset($sheetData[2]);
                        unset($sheetData[3]);
                        $sheetData = array_values($sheetData);
                        $var = 0;
                        foreach ($sheetData as $index => $row) {
                            if (strtoupper(trim($row[0])) == 'TRANSFERS') {
                                unset($sheetData[$index]);
                                break;
                            }
                            $model_flight = new Flights();
                            $model_flight->voucher_id = $model->id;
                            $model_flight->flight_id = trim($row[0]);
                            $model_flight->flight_date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[1])->format('Y-m-d');
                            $model_flight->source = trim($row[2]);
                            $model_flight->destination = trim($row[3]);
                            $model_flight->departure_time = empty(trim($row[5])) ? '' : \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[5])->format('H:i');
                            $model_flight->arrival_time = empty(trim($row[6])) ? '' : \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[6])->format('H:i');
                            $model_flight->created_at = date('Y-m-d H:i:s');
                            $model_flight->status = '1';
                            $model_flight->save(false);
                            unset($sheetData[$index]);
                        }
                        $sheetData = array_values($sheetData);
                        unset($sheetData[0]);
                        foreach ($sheetData as $index => $row) {
                            if (strpos(strtoupper(preg_replace('/\s+/u', '', trim($row[0]))), 'ACTIVITY&SIGHTSEEINGS') > -1) {
                                unset($sheetData[$index]);
                                break;
                            }
                            if (!empty(trim($row[0]))) {
                                $model_transfer = new Transfers();
                                $model_transfer->voucher_id = $model->id;
                                $model_transfer->transfer_date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[0])->format('Y-m-d');
                                $model_transfer->transfer_time = empty(trim($row[1])) ? '' : \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($row[1]))->format('H:i');
                                $model_transfer->transfer_time .= isset($sheetData[$index + 1][0]) && empty(trim($sheetData[$index + 1][0])) && !empty(trim($sheetData[$index + 1][1])) ? ' - ' . \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($sheetData[$index + 1][1]))->format('H:i') : '';
                                $model_transfer->transfer_objective = trim($row[2]);
                                $model_transfer->transfer_source = trim($row[3]);
                                $model_transfer->transfer_source .= isset($sheetData[$index + 1][0]) && empty(trim($sheetData[$index + 1][0])) && !empty(trim($sheetData[$index + 1][2])) ? ', ' . trim($sheetData[$index + 1][2]) : '';
                                $model_transfer->transfer_source .= isset($sheetData[$index + 1][0]) && empty(trim($sheetData[$index + 1][0])) && !empty(trim($sheetData[$index + 1][3])) ? ', ' . trim($sheetData[$index + 1][3]) : '';
                                $model_transfer->transfer_destination = trim($row[5]);
                                $model_transfer->transfer_type = trim($row[6]);
                                $model_transfer->created_at = date('Y-m-d H:i:s');
                                $model_transfer->status = '1';
                                $model_transfer->save(false);
                            }
                            unset($sheetData[$index]);
                        }
                        $sheetData = array_values($sheetData);
                        unset($sheetData[0]);
                        foreach ($sheetData as $index => $row) {
                            if (strpos(strtoupper(preg_replace('/\s+/u', '', trim($row[0]))), 'IMPORTANTNOTES') > -1) {
                                unset($sheetData[$index]);
                                break;
                            }
                            if (!empty(trim($row[0]))) {
                                $model_activity = new Activity();
                                $model_activity->voucher_id = $model->id;
                                $model_activity->tour_date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[0])->format('Y-m-d');
                                $model_activity->pickup_time = empty(trim($row[1])) ? '' : \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($row[1]))->format('H:i');
                                $model_activity->pickup_time .= isset($sheetData[$index + 1][0]) && empty(trim($sheetData[$index + 1][0])) && !empty(trim($sheetData[$index + 1][1])) ? ' - ' . \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($sheetData[$index + 1][1]))->format('H:i') : '';
                                $model_activity->destination = trim($row[2]);
                                $model_activity->tour_name = trim($row[3]);
                                $model_activity->meeting_point = trim($row[5]);
                                $model_activity->tour_type = trim($row[4]);
                                $model_activity->enroute_location = trim($row[6]);
                                $model_activity->created_at = date('Y-m-d H:i:s');
                                $model_activity->status = '1';
                                $model_activity->save(false);

                                unset($sheetData[$index]);
                            }
                        }
                        Yii::$app->session->setFlash('excelFormSubmitted', 'Excel File Uploaded successfully.');
                        $this->redirect('index');
                    }
                } catch (\yii\db\Exception $exception) {
                    return $this->render('excel_upload', ['model' => $model, 'exception' => $exception]);
                } catch (\yii\base\Exception $exception) {
                    return $this->render('excel_upload', ['model' => $model, 'exception' => $exception]);
                } catch (\Exception $exception) {
                    return $this->render('excel_upload', ['model' => $model, 'exception' => $exception]);
                }
            }
        }
        return $this->render('excel_upload', ['model' => $model]);
    }

    /**
     * view all the details and related details of a particular voucher from the database.
     * @param type $id
     * @return type
     */
    public function actionView($id) {
        $model = Voucher::find()
                ->where(['id' => $id])
                ->one();
        return $this->render('view', ['model' => $model]);
    }

    /**
     * add one voucher manually
     * @return type
     */
    public function actionAddVoucher() {
        $modelVoucher = new Voucher();
        $modelFlights = new Flights();
        $modelTransfers = new Transfers();
        $modelActivity = new Activity();
        $model = new SupplierTourName();
        if (Yii::$app->request->isPost) {
            $modelVoucher->scenario = "create_voucher_manual";
            $modelVoucher->attributes = $_POST['Voucher'];
            $modelVoucher->trip_start = date('Y-m-d', strtotime($_POST['Voucher']['trip_start']));
            $modelVoucher->trip_end = date('Y-m-d', strtotime($_POST['Voucher']['trip_end']));
            $modelVoucher->created_at = date('Y-m-d H:i:s');
            $modelVoucher->is_manual = '1';
            $modelVoucher->status = '1';
            if ($modelVoucher->save(FALSE)) {
                if (isset($_POST['Flights'])) {
//                   echo'<pre>';
//          print_r($_POST);
//          echo'</pre>';
//          exit();
                    foreach ($_POST['Flights'] as $index => $flight) {
                        foreach ($flight as $key => $flight_row) {
                            if (empty($flight_row)) {
                                unset($_POST['Flights'][$index][$key]);
                                unset($flight[$key]);
                            }
                        }
                        if (empty($flight) || count($flight) < 3) {
                            unset($_POST['Flights'][$index]);
                            continue;
                        } else {
                            $flight_model = new Flights();
                            $flight_model->attributes = $flight;
                            $flight_model->voucher_id = $modelVoucher->id;
                            $flight_model->flight_date = date('Y-m-d', strtotime($flight['flight_date']));
                            $flight_model->created_at = date('Y-m-d H:i:s');
                            $flight_model->status = '1';

                            $flight_model->save(FALSE);
                        }
                    }
                }
                if (isset($_POST['Transfers'])) {
                    foreach ($_POST['Transfers'] as $transfer) {

                        foreach ($transfer as $key => $transfer_row) {
                            if (empty($transfer_row)) {
                                unset($_POST['Transfers'][$index][$key]);
                                unset($transfer[$key]);
                            }
                        }
                        if (empty($transfer)) {
                            unset($_POST['Transfers'][$index]);
                            continue;
                        } else {
                            $transfer_model = new Transfers();
                            $transfer_model->attributes = $transfer;
                            $transfer_model->voucher_id = $modelVoucher->id;
                            $transfer_model->transfer_date = isset($transfer['transfer_date']) && !empty($transfer['transfer_date']) ? date('Y-m-d', strtotime($transfer['transfer_date'])) : '';
                            $transfer_model->created_at = date('Y-m-d H:i:s');
                            $transfer_model->status = '1';

                            $transfer_model->save(FALSE);
                        }
                    }
                }
                if (isset($_POST['Activity'])) {

                    foreach ($_POST['Activity'] as $activity) {

                        foreach ($activity as $key => $activity_row) {
                            if (empty($activity_row)) {
                                unset($_POST['Activity'][$index][$key]);
                                unset($activity[$key]);
                            }
                        }
                        if (empty($activity)) {
                            unset($_POST['Activity'][$index]);
                            continue;
                        } else {

                            $activity_model = new Activity();
                            $activity_model->attributes = $activity;
                            $activity_model->voucher_id = $modelVoucher->id;
                            $activity_model->tour_date = isset($activity['tour_date']) && !empty($activity['tour_date']) ? date('Y-m-d', strtotime($activity['tour_date'])) : '';
                            $activity_model->created_at = date('Y-m-d H:i:s');
                            $activity_model->status = '1';
                            $activity_model->save(FALSE);
                        }
                    }
                }
                $this->redirect(['view', 'id' => $modelVoucher->id]);
            }
        }

        $tourname = SupplierTourName::find()->all();
        $model = ArrayHelper::map($tourname, 'id', 'tourname');

        return $this->render('create', [
                    'modelVoucher' => $modelVoucher,
                    'modelFlights' => $modelFlights,
                    'modelTransfers' => $modelTransfers,
                    'modelActivity' => $modelActivity,
                    'model' => $model
        ]);
    }

    /**
     * add view as per need in the add voucher section form
     */
    public function actionAjaxFlightAdd() {

        if (Yii::$app->request->isAjax) {
            $type = $_POST['type'] ?? '';
            $referrer = $_POST['referrer'] ?? '';
            $counter = $_POST['counter'] ?? '';
            if (empty($type) || empty($counter)) {
                $response = [
                    'status' => 'FAILED',
                    'code' => '-1'
                ];
            } else if ($type == 'ps_flight_panel') {
                echo $this->renderPartial('add_flights_ajax', ['counter' => $counter, 'referrer' => $referrer]);
                exit();
            } else if ($type == 'ps_transfer_panel') {
                echo $this->renderPartial('add_transfer_ajax', ['counter' => $counter, 'referrer' => $referrer]);
                exit();
            } else if ($type == 'ps_activity_panel') {
                echo $this->renderPartial('add_activity_ajax', ['counter' => $counter, 'referrer' => $referrer]);
                exit();
            } else {
                $response = [
                    'status' => 'FAILED',
                    'code' => '-2'
                ];
            }
        } else {
            $response = [
                'status' => 'FAILED',
                'code' => '-99'
            ];
        }
        echo json_encode($response);

        exit();
    }

    /**
     * validation function for addvoucher
     * @return boolean if no error found else error list
     */
    public function actionValidateVoucher() {
//        echo'<pre>';
//        print_r($_POST);
//        echo'</pre>';
//        exit();
        $modelVoucher = new Voucher();
        $error = [];
        $modelVoucher->scenario = "create_voucher_manual";
        $modelVoucher->attributes = $_POST['Voucher'];
        $modelVoucher->created_at = date('Y-m-d H:i:s');
        $modelVoucher->status = '1';
        if ($modelVoucher->validate()) {
            if (isset($_POST['Flights'])) {

                foreach ($_POST['Flights'] as $index => $flight) {
                    foreach ($flight as $key => $flight_row) {
                        if (empty($flight_row)) {
                            unset($_POST['Flights'][$index][$key]);
                            unset($flight[$key]);
                        }
                    }
                    if (empty($flight) || count($flight) < 3) {
                        unset($_POST['Flights'][$index]);
                        continue;
                    } else {
                        $flight_model = new Flights();
                        $flight_model->scenario = "create_voucher_manual";
                        $flight_model->attributes = $flight;
                        $flight_model->voucher_id = $modelVoucher->id;
                        $flight_model->flight_date = date('Y-m-d', strtotime($flight['flight_date']));
                        $flight_model->created_at = date('Y-m-d H:i:s');
                        $flight_model->status = '1';
                        if (!$flight_model->validate()) {
                            $error = array_merge($error, $flight_model->getErrorSummary(TRUE));
                        }
                    }
                }
            }
            if (isset($_POST['Transfers'])) {
                foreach ($_POST['Transfers'] as $transfer) {
                    foreach ($transfer as $key => $transfer_row) {
                        if (empty($transfer_row)) {
                            unset($_POST['Transfers'][$index][$key]);
                            unset($transfer[$key]);
                        }
                    }
                    if (empty($transfer)) {
                        unset($_POST['Transfers'][$index]);
                        continue;
                    } else {
                        $transfer_model = new Transfers();
                        $transfer_model->scenario = "create_voucher_manual";
                        $transfer_model->attributes = $transfer;
                        $transfer_model->voucher_id = $modelVoucher->id;
                        $transfer_model->transfer_date = isset($transfer['transfer_date']) && !empty($transfer['transfer_date']) ? date('Y-m-d', strtotime($transfer['transfer_date'])) : '';
                        $transfer_model->created_at = date('Y-m-d H:i:s');
                        $transfer_model->status = '1';
                        if (!$transfer_model->validate()) {
                            $error = array_merge($error, $transfer_model->getErrorSummary(TRUE));
                        }
                    }
                }
            }
            if (isset($_POST['Activity'])) {
                foreach ($_POST['Activity'] as $activity) {

                    foreach ($activity as $key => $activity_row) {
                        if (empty($activity_row)) {
                            unset($_POST['Activity'][$index][$key]);
                            unset($activity[$key]);
                        }
                    }
                    if (empty($activity)) {
                        unset($_POST['Activity'][$index]);
                        continue;
                    } else {
                        $activity_model = new Activity();
                        $activity_model->scenario = "create_voucher_manual";
                        $activity_model->attributes = $activity;
//                       $activity_model->meeting_point=$activity['meeting_point'];
//                       $activity_model->tour_type=$activity['tour_type'];
                        $activity_model->voucher_id = $modelVoucher->id;
                        $activity_model->tour_date = isset($activity['tour_date']) && !empty($activity['tour_date']) ? date('Y-m-d', strtotime($activity['tour_date'])) : '';
                        $activity_model->created_at = date('Y-m-d H:i:s');
                        $activity_model->status = '1';
                        if (!$activity_model->validate()) {
//                            print_r($activity);
//                            exit();
                            $error = array_merge($error, $activity_model->getErrorSummary(TRUE));
                        }
                    }
                }
            }
        } else {
            $error = array_merge($error, $modelVoucher->getErrorSummary(TRUE));
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (empty($error)) {
            return TRUE;
        }
        return $error;
    }

    /**
     * edit a particular voucher from admin end
     * @param type $id
     * @return type
     */
    public function actionEdit($id) {
        $model = Voucher::find()
                ->where(['id' => $id])
                ->one();

        if (Yii::$app->request->isPost) {
            $model->scenario = "create_voucher_manual";
            $model->attributes = $_POST['Voucher'];
            $model->trip_start = date('Y-m-d', strtotime($_POST['Voucher']['trip_start']));
            $model->trip_end = date('Y-m-d', strtotime($_POST['Voucher']['trip_end']));
            $model->created_at = date('Y-m-d H:i:s');
            $model->is_manual = '1';
            $model->status = '1';
            if ($model->update(FALSE)) {
                if (isset($_POST['Flights'])) {
                    if (count($model->flights) > 0) {
                        foreach ($model->flights as $index => $value) {
                            $value->delete();
                        }
                    }
                    foreach ($_POST['Flights'] as $index => $flight) {
                        foreach ($flight as $key => $flight_row) {
                            if (empty($flight_row)) {
                                unset($_POST['Flights'][$index][$key]);
                                unset($flight[$key]);
                            }
                        }
                        if (empty($flight) || count($flight) < 3) {
                            unset($_POST['Flights'][$index]);
                            continue;
                        } else {
                            $flight_model = new Flights();
                            $flight_model->attributes = $flight;
                            $flight_model->voucher_id = $model->id;
                            $flight_model->flight_date = date('Y-m-d', strtotime($flight['flight_date']));
                            $flight_model->created_at = date('Y-m-d H:i:s');
                            $flight_model->status = '1';

                            $flight_model->save(FALSE);
                        }
                    }
                }
                if (isset($_POST['Transfers'])) {
                    if (count($model->transfers) > 0) {
                        foreach ($model->transfers as $index => $value) {
                            $value->delete();
                        }
                    }
                    foreach ($_POST['Transfers'] as $transfer) {
//                        echo'<pre>';
//                        print_r($transfer);
//                        echo'</pre>';
//                        exit();
                        foreach ($transfer as $key => $transfer_row) {
                            if ($transfer_row != 0 && empty($transfer_row)) {
                                unset($_POST['Transfers'][$index][$key]);
                                unset($transfer[$key]);
                            }
                        }
                        if (empty($transfer)) {
                            unset($_POST['Transfers'][$index]);
                            continue;
                        } else {

                            $transfer_model = new Transfers();
                            $transfer_model->attributes = $transfer;
                            $transfer_model->voucher_id = $model->id;
                            $transfer_model->transfer_type = $transfer['transfer_type'];
                            $transfer_model->service_status = $transfer['service_status'];
                            $transfer_model->transfer_date = isset($transfer['transfer_date']) && !empty($transfer['transfer_date']) ? date('Y-m-d', strtotime($transfer['transfer_date'])) : '';
                            $transfer_model->created_at = date('Y-m-d H:i:s');
                            $transfer_model->status = '1';
                            $transfer_model->save(FALSE);
                        }
                    }
                }
                if (isset($_POST['Activity'])) {


                    if (count($model->activity) > 0) {
                        foreach ($model->activity as $index => $value) {
                            $value->delete();
                        }
                    }
                    foreach ($_POST['Activity'] as $activity) {

                        foreach ($activity as $key => $activity_row) {
                            if ($activity_row != 0 && empty($activity_row)) {
                                unset($_POST['Activity'][$index][$key]);
                                unset($activity[$key]);
                            }
                        }
                        if (empty($activity)) {
                            unset($_POST['Activity'][$index]);
                            continue;
                        } else {
                            $activity_model = new Activity();
                            $activity_model->attributes = $activity;
                            $activity_model->voucher_id = $model->id;

                            $activity_model->tour_date = isset($activity['tour_date']) && !empty($activity['tour_date']) ? date('Y-m-d', strtotime($activity['tour_date'])) : '';
                            $activity_model->created_at = date('Y-m-d H:i:s');
                            $activity_model->status = '1';
                            $activity_model->save(FALSE);
                        }
                    }
                }


                $this->redirect(['view', 'id' => $model->id]);
            }
        }
        $tourname = SupplierTourName::find()->all();
        $tourdata = ArrayHelper::map($tourname, 'id', 'tourname');

        return $this->render('update', ['model' => $model, 'model1' => $tourdata]);
    }

    /**
     * change the payment status from the booking table through ajax
     * @throws UserException
     */
    public function actionAjaxPaymentStatusChange() {
        if (Yii::$app->request->isAjax) {
            if (!empty($_POST['model_id'])) {
                $model = Voucher::find()
                        ->where(['id' => $_POST['model_id']])
                        ->one();
                $model->is_payment_done = $_POST['status'];
                $model->update(FALSE);
            } else {
                throw new UserException("required data is not present.");
            }
        }
    }

    /**
     * getting the related transfer details when clicked in create transfer button in manual voucher create
     * @throws UserException
     */
    public function actionAjaxTransferAdd() {

        if (Yii::$app->request->isAjax) {
            $fields = $_POST['fields'] ?? '';
            $counter = $_POST['counter'] ?? '';
            if (empty($fields) || empty($counter)) {
                $response = [
                    'status' => 'FAILED',
                    'code' => '-1'
                ];
            } else if (!empty($fields)) {
                echo $this->renderPartial('add_related_transfer_ajax', ['counter' => $counter, 'fields' => $fields]);
                exit();
            } else {
                throw new UserException("required data is not present.");
            }
        } else {
            throw new UserException("Can not call this method directly.");
        }
        echo json_encode($response);

        exit();
    }

    /**
     * getting the mail template for email to supplier
     */
    public function actionAjaxCreateMailTemplateToSupplier() {
        if (Yii::$app->request->isAjax) {
            $ids = $_POST['ids'] ?? '';

            if (!empty($ids)) {
                $model = Transfers::find()->where(['in', 'id', $ids])->orderBy(['transfer_date' => SORT_ASC, 'transfer_time' => SORT_ASC])->all();
                $view = $this->renderPartial('transfer_mail_table', ['model' => $model]);
                $subject = 'Transfer Booking For ' . $model[0]->voucher->billing_name . 'Booking ID :REF-' . $model[0]->voucher->id;
                $response = [
                    'body' => $view,
                    'subject' => $subject
                ];
                echo json_encode($response);
                exit();
            }
        } else {
            throw new UserException("Can not call this method directly.");
        }
    }

    /**
     * getting the mail template for email to supplier only activity
     * @throws UserException
     * 
     */
    public function actionAjaxCreateMailTemplateToSupplierTour() {
        if (Yii::$app->request->isAjax) {
            $ids = $_POST['ids'] ?? '';
            if (!empty($ids)) {
                $model = Activity::find()->where(['in', 'id', $ids])->all();
                $view = $this->renderPartial('activity_mail_table', ['model' => $model]);
                $subject = 'Tour Booking For ' . $model[0]->voucher->billing_name . 'Booking ID :REF-' . $model[0]->voucher->id;
                $response = [
                    'body' => $view,
                    'subject' => $subject
                ];
                echo json_encode($response);
                exit();
            }
        } else {
            throw new UserException("Can not call this method directly.");
        }
    }

    /**
     * shoot the mail to the required address
     */
    public function actionAjaxSendMail($id) {
        if (Yii::$app->request->isPost) {
            $model = new Voucher();
            $model->uploaded_file = UploadedFile::getInstances($model, 'uploaded_file');
//            print_r($_POST);
//            exit();
            $message = Yii::$app->mailer->compose(['layout' => 'main']);
            $attachments = '';
//            $message->setEncoder(Swift_Encoding::get8BitEncoding());
            $message->setFrom(Yii::$app->params['adminEmail'])
                    ->setTo($_POST['to'])
                    ->setSubject($_POST['subject'])
                    ->setHtmlBody($_POST['body']);
            if ($model->uploaded_file) {
                foreach ($model->uploaded_file as $file) {
                    $filename = 'uploads/emailattachments/' . $file->baseName . '.' . $file->extension;
                    $attachments .= $file->baseName . '.' . $file->extension . ',';
                    $file->saveAs($filename);
                    $message->attach($filename);
                }
            }
            if (!isset($_POST['check_mail_schedule']) || $_POST['check_mail_schedule'] != 1) {

                if ($message->send()) {
                    if ($_POST['type'] == 'transfer') {
                        $ids = explode(',', $_POST['transferselectedid']);
                        foreach ($ids as $value) {
                           
                            $model = Transfers::find()->where(['id' => $value])->one();
                            $model->is_mail_sent = '1';
                            $model->save(FALSE);
                        }
                    } else if ($_POST['type'] == 'tour') {
                        $ids = explode(',', $_POST['transferselectedid']);
                        foreach ($ids as $value) {
                            $model = Activity::find()->where(['id' => $value])->one();
                            $model->is_mail_sent = '1';
                            $model->save(FALSE);
                        }
                    }
                    Yii::$app->session->setFlash('mail_sent', 'Mail Sent to reciepent Succesfully.');
                }
            } else {
                $model_email_cron = new CronemailMaster();
                $model_email_cron->mail_to = $_POST['to'];
                $model_email_cron->mail_from = Yii::$app->params['adminEmail'];
                $model_email_cron->subject = $_POST['subject'];
                $model_email_cron->body = $_POST['body'];
                $model_email_cron->attachment = $attachments;
                $model_email_cron->scheduled_date = date('Y-m-d H:i:s', strtotime($_POST['schedule_time']));
                $model_email_cron->is_mail_sent = '0';
                $model_email_cron->mail_type = $_POST['type'] == 'tour' ? '2' : '1';
                $model_email_cron->linked_ids = $_POST['transferselectedid'];
                $model_email_cron->created_at = date('Y-m-d');
                $model_email_cron->status = '1';
                
                if ($model_email_cron->save()) {
                    Yii::$app->session->setFlash('mail_sent', 'The mail will be sent at the given time.');
                }
            }
        }
        $this->redirect(['view', 'id' => $id]);
    }

    /**
     * create full voucher mail template
     * @throws UserException
     */
    public function actionAjaxCreateMailTemplateToSupplierVoucher() {
        if (Yii::$app->request->isAjax) {
            $id = $_POST['id'] ?? '';
            if (!empty($id)) {
                $model = Voucher::find()->where(['id' => $id])->one();
//                $message = Yii::$app->mailer->compose(['layout' => 'main']);
                $view = $this->renderPartial('voucher_full_mail_table', ['model' => $model]);
                $subject = 'Voucher Confirmation For' . $model->billing_name;
                $response = [
                    'body' => $view,
                    'subject' => $subject
                ];
                echo json_encode($response);
                exit();
            }
        } else {
            throw new UserException("Can not call this method directly.");
        }
    }

    public function actionAjaxSendMailVoucherCustomer() {
        if (Yii::$app->request->isPost) {
            $model = new Voucher();
            $model->uploaded_file = UploadedFile::getInstances($model, 'uploaded_file');
            $message = Yii::$app->mailer->compose(['layout' => 'main']);
            $body = $_POST['body'];
            $attachments = '';
            $message->setFrom(Yii::$app->params['adminEmail'])
                    ->setTo($_POST['to'])
                    ->setSubject($_POST['subject'])
                    ->setHtmlBody($_POST['body']);
            if ($model->uploaded_file) {
                foreach ($model->uploaded_file as $file) {
                    $filename = 'uploads/emailattachments/' . $file->baseName . '.' . $file->extension;
                    $attachments .= $file->baseName . '.' . $file->extension . ',';
                    $file->saveAs($filename);
                    $message->attach($filename);
                }
            }
            if (!isset($_POST['check_mail_schedule']) || $_POST['check_mail_schedule'] != 1) {
                if ($message->send()) {
                    Yii::$app->session->setFlash('mail_sent', 'Mail Sent to customer Succesfully.');
                } else {
                    Yii::$app->session->setFlash('mail_not_sent', 'Error occured while sending a email.');
                }
            } else {
                $model_email_cron = new CronemailMaster();
                $model_email_cron->mail_to = $_POST['to'];
                $model_email_cron->mail_from = Yii::$app->params['adminEmail'];
                $model_email_cron->subject = $_POST['subject'];
                $model_email_cron->body = $_POST['body'];
                $model_email_cron->attachment = $attachments;
                $model_email_cron->mail_type = '0';
                $model_email_cron->linked_ids = $_POST['transferselectedid'];

                $model_email_cron->scheduled_date = date('Y-m-d H:i:s', strtotime($_POST['schedule_time']));
                $model_email_cron->is_mail_sent = '0';
                $model_email_cron->created_at = date('Y-m-d');
                $model_email_cron->status = '1';
                if ($model_email_cron->save()) {
                    Yii::$app->session->setFlash('mail_sent', 'The mail will be sent at the given time.');
                }
            }
        }
        $this->redirect(['index']);
    }

}
