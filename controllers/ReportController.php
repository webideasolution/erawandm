<?php

namespace app\controllers;

use Yii;
use app\components\CommonController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LoginForm;
use app\models\Transfers;
use \app\models\Activity;
use app\models\Flights;
use kartik\mpdf\Pdf;

class ReportController extends CommonController {

    /**
     * Index action.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * opens the daily transfer Report
     * @return type
     */
    public function actionDailyTransferReport() {
        $upper = date('Y-m-d');
        $lower = date('Y-m-d');
        $city_name = 'BANGKOK';
        $city_key = 'BKK';
        if (Yii::$app->request->isPost) {
            $lower = isset($_POST['order-start-date']) && !empty($_POST['order-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['order-start-date'])[0])) : '0000-00-00';
            $upper = isset($_POST['order-start-date']) && !empty($_POST['order-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['order-start-date'])[1])) : '2090-12-31';
            $lower1 = isset($_POST['tour-start-date']) && !empty($_POST['tour-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['tour-start-date'])[0])) : '0000-00-00';
            $upper1 = isset($_POST['tour-start-date']) && !empty($_POST['tour-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['tour-start-date'])[1])) : '2090-12-31';
            $city = isset($_POST['city']) && !empty($_POST['city']) ? Yii::$app->request->post('city') : '';
            if ($city == 2) {
                $model = Transfers::find()->where(['<>', 'status', '2'])->andWhere(['and', "DATE(transfer_date)>='$lower1'", "DATE(transfer_date)<='$upper1'"])->andWhere(['and', "DATE(created_at)>='$lower'", "DATE(created_at)<='$upper'"])->andWhere(['or', "UPPER(transfer_source) LIKE '%PT%' OR UPPER(transfer_source) LIKE '%Pattaya%'", "UPPER(transfer_destination) LIKE '%BKK%' OR UPPER(transfer_destination) LIKE '%Bangkok%'"])->orderBy(['id' => SORT_DESC])->all();
            } else if ($city == 3) {
                $model = Transfers::find()->where(['<>', 'status', '2'])->andWhere(['and', "DATE(transfer_date)>='$lower1'", "DATE(transfer_date)<='$upper1'"])->andWhere(['and', "DATE(created_at)>='$lower'", "DATE(created_at)<='$upper'"])->andWhere(['or', "UPPER(transfer_source) LIKE '%HKT%' OR UPPER(transfer_source) LIKE '%PHUKET%'", "UPPER(transfer_destination) LIKE '%KBV%' OR UPPER(transfer_destination) LIKE '%KRABI%'"])->orderBy(['id' => SORT_DESC])->all();
            } else if ($city == 4) {
                $model = Transfers::find()->where(['<>', 'status', '2'])->andWhere(['and', "DATE(transfer_date)>='$lower1'", "DATE(transfer_date)<='$upper1'"])->andWhere(['and', "DATE(created_at)>='$lower'", "DATE(created_at)<='$upper'"])->andWhere(['or', "UPPER(transfer_source) LIKE '%KBV%' OR UPPER(transfer_source) LIKE '%KRABI%'", "UPPER(transfer_destination) LIKE '%HKT%' OR UPPER(transfer_destination) LIKE '%PHUKET%'"])->orderBy(['id' => SORT_DESC])->all();
            } else if ($city == 5) {
                $model = Transfers::find()->where(['<>', 'status', '2'])->andWhere(['and', "DATE(transfer_date)>='$lower1'", "DATE(transfer_date)<='$upper1'"])->andWhere(['and', "DATE(created_at)>='$lower'", "DATE(created_at)<='$upper'"])->andWhere(['or', "UPPER(transfer_source) LIKE '%BKK%' OR UPPER(transfer_source) LIKE '%Bangkok%' OR UPPER(transfer_destination) LIKE '%BKK%' OR UPPER(transfer_destination) LIKE '%Bangkok%'", "UPPER(transfer_source) LIKE '%PT%' OR UPPER(transfer_source) LIKE '%Pattaya%' OR UPPER(transfer_destination) LIKE '%PT%' OR UPPER(transfer_destination) LIKE '%Pattaya%'"])->orderBy(['id' => SORT_DESC])->all();
            } else if ($city == 6) {
                $model = Transfers::find()->where(['<>', 'status', '2'])->andWhere(['and', "DATE(transfer_date)>='$lower1'", "DATE(transfer_date)<='$upper1'"])->andWhere(['and', "DATE(created_at)>='$lower'", "DATE(created_at)<='$upper'"])->andWhere(['or', "UPPER(transfer_source) LIKE '%KBV%' OR UPPER(transfer_source) LIKE '%KRABI%' OR UPPER(transfer_destination) LIKE '%KBV%' OR UPPER(transfer_destination) LIKE '%KRABI%'", "UPPER(transfer_source) LIKE '%HKT%' OR UPPER(transfer_source) LIKE '%PHUKET%' OR UPPER(transfer_destination) LIKE '%HKT%' OR UPPER(transfer_destination) LIKE '%PHUKET%'"])->orderBy(['id' => SORT_DESC])->all();
            } else {
                $model = Transfers::find()->where(['<>', 'status', '2'])->andWhere(['and', "DATE(transfer_date)>='$lower1'", "DATE(transfer_date)<='$upper1'"])->andWhere(['and', "DATE(created_at)>='$lower'", "DATE(created_at)<='$upper'"])->orderBy(['id' => SORT_DESC])->all();
            }
            return $this->render('daily_transfer_report', ['model' => $model]);
        }
        $model = Transfers::find()->where(['<>', 'status', '2'])->andWhere(['and', "DATE(transfer_date)>='$lower'", "DATE(transfer_date)<='$upper'"])->orderBy(['id' => SORT_DESC])->all();

        return $this->render('daily_transfer_report', ['model' => $model]);
    }

    /**
     * opens the daily tour Report
     * @return type
     */
    public function actionDailyTourReport() {
        $upper = date('Y-m-d');
        $lower = date('Y-m-d');
        if (Yii::$app->request->isPost) {
            $lower = isset($_POST['order-start-date']) && !empty($_POST['order-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['order-start-date'])[0])) : '0000-00-00';
            $upper = isset($_POST['order-start-date']) && !empty($_POST['order-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['order-start-date'])[1])) : '2090-12-31';
            $lower1 = isset($_POST['tour-start-date']) && !empty($_POST['tour-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['tour-start-date'])[0])) : '0000-00-00';
            $upper1 = isset($_POST['tour-start-date']) && !empty($_POST['tour-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['tour-start-date'])[1])) : '2090-12-31';
            $city = isset($_POST['city']) && !empty($_POST['city']) ? Yii::$app->request->post('city') : '';
            $model = Activity::find()->where(['<>', 'status', '2'])->andWhere(['and', "tour_date>=DATE('$lower1')", "tour_date<=DATE('$upper1')"])->andWhere(['and', "DATE(created_at)>='$lower'", "DATE(created_at)<='$upper'"])->orderBy(['tour_date' => SORT_DESC, 'UPPER(tour_name)' => SORT_DESC])->andWhere(['LIKE', 'destination', $city])->all();
            return $this->render('daily_tour_report', ['model' => $model]);
        }
        $model = Activity::find()->where(['<>', 'status', '2'])->andWhere(['and', "tour_date>='$lower'", "tour_date<='$upper'"])->orderBy(['id' => SORT_DESC])->all();

        return $this->render('daily_tour_report', ['model' => $model]);
    }

    /**
     * lets you force download a pdf file for the transfers filtered by date
     * @return type
     */
    function actionDownloadTransferPdf() {

        $upper = date('Y-m-d');
        $lower = date('Y-m-d');
        if (Yii::$app->request->isPost) {

            $lower = isset($_POST['order-start-date']) && !empty($_POST['order-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['order-start-date'])[0])) : '0000-00-00';
            $upper = isset($_POST['order-start-date']) && !empty($_POST['order-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['order-start-date'])[1])) : '2090-12-31';
            $lower1 = isset($_POST['tour-start-date']) && !empty($_POST['tour-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['tour-start-date'])[0])) : '0000-00-00';
            $upper1 = isset($_POST['tour-start-date']) && !empty($_POST['tour-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['tour-start-date'])[1])) : '2090-12-31';


            if (isset($_POST['order-start-date']) && !empty($_POST['order-start-date'])) {
                $date = date('l jS \of F Y', strtotime(explode(' - ', $_POST['order-start-date'])[0]));
                $date .= ' to ' . date('l jS \of F Y', strtotime(explode(' - ', $_POST['order-start-date'])[1]));
            } else if (isset($_POST['tour-start-date']) && !empty($_POST['tour-start-date'])) {
                $date = date('l jS \of F Y', strtotime(explode(' - ', $_POST['tour-start-date'])[0]));
                $date .= ' to ' . date('l jS \of F Y', strtotime(explode(' - ', $_POST['tour-start-date'])[1]));
            } else {
                $date = date('l jS \of F Y', strtotime(explode(' - ', $lower)));
                $date = date('l jS \of F Y', strtotime(explode(' - ', $upper)));
            }
            $model = Transfers::find()->where(['<>', 'status', '2'])->andWhere(['and', "DATE(transfer_date)>='$lower1'", "DATE(transfer_date)<='$upper1'"])->andWhere(['and', "DATE(created_at)>='$lower'", "DATE(created_at)<='$upper'"])->orderBy(['id' => SORT_DESC])->all();
        } else {
            return $this->redirect(['daily-transfer-report']);
        }
        $this->layout = 'template';
//        return $this->render('../templates/mpdf_transfer_report');
        $content = $this->renderPartial('../templates/mpdf_transfer_report', ['model' => $model, 'date' => $date]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'filename' => 'Transfer Report.pdf',
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_DOWNLOAD,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.img-container img {width: 100%;overflow: hidden;}',
            'options' => ['title' => 'Transfer Report'],
            'methods' => [
                'SetHeader' => ['Erawan'],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);
        $pdf->render();
    }

    /**
     * lets you force download a pdf file for the tours filtered by date
     * @return type
     */
    function actionDownloadTourPdf() {

        $upper = date('Y-m-d');
        $lower = date('Y-m-d');
        if (Yii::$app->request->isPost) {
            $lower = isset($_POST['order-start-date']) && !empty($_POST['order-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['order-start-date'])[0])) : '0000-00-00';
            $upper = isset($_POST['order-start-date']) && !empty($_POST['order-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['order-start-date'])[1])) : '2090-12-31';
            $lower1 = isset($_POST['tour-start-date']) && !empty($_POST['tour-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['tour-start-date'])[0])) : '0000-00-00';
            $upper1 = isset($_POST['tour-start-date']) && !empty($_POST['tour-start-date']) ? date('Y-m-d', strtotime(explode(' - ', $_POST['tour-start-date'])[1])) : '2090-12-31';
            if (isset($_POST['order-start-date']) && !empty($_POST['order-start-date'])) {
                $date = date('l jS \of F Y', strtotime(explode(' - ', $_POST['order-start-date'])[0]));
                $date .= ' to ' . date('l jS \of F Y', strtotime(explode(' - ', $_POST['order-start-date'])[1]));
            } else if (isset($_POST['tour-start-date']) && !empty($_POST['tour-start-date'])) {
                $date = date('l jS \of F Y', strtotime(explode(' - ', $_POST['tour-start-date'])[0]));
                $date .= ' to ' . date('l jS \of F Y', strtotime(explode(' - ', $_POST['tour-start-date'])[1]));
            } else {
                $date = date('l jS \of F Y', strtotime(explode(' - ', $lower)));
                $date = date('l jS \of F Y', strtotime(explode(' - ', $upper)));
            }
            $city = isset($_POST['city']) && !empty($_POST['city']) ? Yii::$app->request->post('city') : '';
            $city = $model = Activity::find()->where(['<>', 'status', '2'])->andWhere(['and', "tour_date>=DATE('$lower1')", "tour_date<=DATE('$upper1')"])->andWhere(['and', "DATE(created_at)>='$lower'", "DATE(created_at)<='$upper'"])->orderBy(['tour_date' => SORT_DESC, 'UPPER(tour_name)' => SORT_DESC])->andWhere(['LIKE', 'destination', $city])->all();
        } else {
            return $this->redirect(['daily-tour-report']);
        }
        $this->layout = 'template';
//        return $this->render('../templates/mpdf_transfer_report');
        $content = $this->renderPartial('../templates/mpdf_tour_report', ['model' => $model, 'date' => $date]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'filename' => 'Tour Report.pdf',
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_DOWNLOAD,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.img-container img {width: 100%;overflow: hidden;} .info{
   background-color:#d9edf7;
   -webkit-print-color-adjust: exact; 
   } .warning{
   background-color:#fcf8e3;
   -webkit-print-color-adjust: exact; 
   }',
            'options' => ['title' => 'Tour Report'],
            'methods' => [
                'SetHeader' => ['Erawan'],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        $pdf->render();
    }

    public function getCityFilterationData($city) {
        $arr_master_data = [
            'BKK' => 'Bangkok',
            'PT' => 'Pattaya',
            'HKT' => 'Phuket',
            'KB' => 'Krabi'
        ];
        if ($city == 1) {
            return [
                'source_city_name' => 'Bangkok',
                'source_city_code' => 'BKK',
                'dest_city_name' => 'Pattaya',
                'dest_city_code' => 'PT'
            ];
        } else if ($city == 2) {
            return [
                'source_city_name' => 'Pattaya',
                'source_city_code' => 'PT',
                'dest_city_name' => 'Bangkok',
                'dest_city_code' => 'BKK'
            ];
        } else if ($city == 3) {
            return [
                'source_city_name' => 'Pattaya',
                'source_city_code' => 'PT',
                'dest_city_name' => 'Bangkok',
                'dest_city_code' => 'BKK'
            ];
        }
        $arr = [
            '1' => [
                'source_city_name' => 'Bangkok',
                'source_city_code' => 'BKK',
                'dest_city_name' => 'Bangkok',
                'dest_city_code' => 'BKK'
            ],
            '1' => [
                'source_city_name' => 'Bangkok',
                'source_city_code' => 'BKK',
                'dest_city_name' => 'Bangkok',
                'dest_city_code' => 'BKK'
            ],
            '1' => [
                'source_city_name' => 'Bangkok',
                'source_city_code' => 'BKK',
                'dest_city_name' => 'Bangkok',
                'dest_city_code' => 'BKK'
            ],
        ];
//       
    }

}
