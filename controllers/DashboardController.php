<?php

namespace app\controllers;
use app\models\InvoiceIncoming;
use app\models\InvoiceOutgoing;
use Yii;
use app\components\CommonController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class DashboardController extends CommonController {

    /**
     * Index action.
     *
     * @return string
     */
    public function actionIndex() {
        $date=date('Y-m-d');  
        $newDate = date('Y-m-d',strtotime('+15 days',strtotime($date)));
        
        $model = InvoiceIncoming::find()->where(['<>', 'status', '2'])->andWhere(['and', "DATE(due_date)>='$date'", "DATE(due_date)<='$newDate'"])->andWhere(['and',"payment_status='1'"])->orderBy(['id' => SORT_DESC])->all();
//        print_r($model) ;
//        exit();
        $model1 = InvoiceOutgoing::find()->where(['<>', 'status', '2'])->andWhere(['and',"DATE(due_date)>='$date'","DATE(due_date)<='$newDate'"])->andWhere(['and',"payment_status='1'"])->orderBy(['id' => SORT_DESC])->all(); 
//       print_r($model1) ;
//        exit();
        return $this->render('index',[ 'model' => $model,'model1' => $model1]);
       
          }
        
          }
