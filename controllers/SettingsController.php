<?php

namespace app\controllers;

use yii;
use app\components\CommonController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use app\models\SiteSettings;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;
use yii\helpers\Url;

class SettingsController extends CommonController {

    public function actionIndex() {
        $model = SiteSettings::find()->where(['<>', 'status', '2'])->orderBy(['id' => SORT_DESC])->all();
        return $this->render('index', ['model' => $model]);
    }

    public function actionUpdate($id) {
        $data = SiteSettings::find()
                ->where(['id' => $id])
                ->one();
        if (Yii::$app->request->isPost) {

            $data->load(Yii::$app->request->post());
            $data->updated_at = date('Y-m-d H:i:s');
            if ($data->save()) {
                Yii::$app->getSession()->setFlash('Currencyupdate', 'updated sucessfully');
                return $this->redirect(['index']);
            }
        }
        return $this->render('update', [
                    'model' => $data,]);
    }

}

?>