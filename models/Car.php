<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "car".
 *
 * @property string $id
 * @property string $name
 * @property int $capacity
 * @property int $fare
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 */
class Car extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'capacity', 'fare', ], 'required'],
            [['capacity', 'fare'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['status'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'capacity' => 'Capacity',
            'fare' => 'Fare',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }
}


