<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "invoice_outgoing".
 *
 * @property string $id
 * @property string $supplieir_id
 * @property double $amount
 * @property string $issue_date
 * @property string $due_date
 * @property string $upload_invoice
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 */
class InvoiceOutgoing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
     public $file;
    public static function tableName()
    {
        return 'invoice_outgoing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
         return [
             [['supplieir_id', 'name', 'destination','startdate','enddate','payment_status','amount', 'issue_date', 'due_date',  'status'], 'required'],
            [['amount'], 'number'],
            [['issue_date', 'due_date', 'created_at', 'updated_at'], 'safe'],
            [['upload_invoice', 'status'], 'string'],
            [['status'], 'required'],
            [['supplieir_id'], 'string', 'max' => 20],
              [['upload_invoice'],'file','extensions' => 'pdf'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'supplieir_id' => 'Supplieir ID',
             'name' => 'Name',
            'destination'=>'Destination',
            'startdate'=>'StartDate',
            'enddate'=>'EndDate',
            'amount' => 'Amount',
            'issue_date' => 'Issue Date',
            'due_date' => 'Due Date',
            'upload_invoice' => 'Upload Invoice',
            'payment_status'=>'Payment Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }
}
