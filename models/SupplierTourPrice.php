<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "supplier_tour_price".
 *
 * @property string $id
 * @property string $supplier_tour_name_id
 * @property double $sic_adult_price
 * @property double $sic_child_price
 * @property double $ticket_adult_price
 * @property double $ticket_child_price
 * @property string $valid_from
 * @property string $valid_upto
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 */
class SupplierTourPrice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'supplier_tour_price';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['supplier_tour_name_id', 'status'], 'required'],
            [['supplier_tour_name_id'], 'integer'],
            [['sic_adult_price', 'sic_child_price', 'ticket_adult_price', 'ticket_child_price'], 'number'],
            [['valid_from', 'valid_upto', 'created_at', 'updated_at'], 'safe'],
            [['status'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'supplier_tour_name_id' => 'Supplier Tour Name ID',
            'sic_adult_price' => 'Sic Adult Price',
            'sic_child_price' => 'Sic Child Price',
            'ticket_adult_price' => 'Ticket Adult Price',
            'ticket_child_price' => 'Ticket Child Price',
            'valid_from' => 'Valid From',
            'valid_upto' => 'Valid Upto',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }
}
