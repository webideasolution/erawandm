<?php

namespace app\models; 

use Yii; 

/** 
 * This is the model class for table "supplier_tour_name". 
 * 
 * @property string $id
 * @property int $supplier_id
 * @property string $tourname
 */ 
class SupplierTourName extends \yii\db\ActiveRecord
{ 
    /** 
     * {@inheritdoc} 
     */ 
    public static function tableName() 
    { 
        return 'supplier_tour_name'; 
    } 

    /** 
     * {@inheritdoc} 
     */ 
    public function rules() 
    { 
        return [
            [['supplier_id', 'tourname','sic_adult_price', 'sic_child_price', 'ticket_adult_price','ticket_child_price'], 'required'],
            [['supplier_id'], 'integer'],
            [['tourname'], 'string', 'max' => 255],
        ]; 
    } 

    /** 
     * {@inheritdoc} 
     */ 
    public function attributeLabels() 
    { 
        return [ 
            'id' => 'ID',
            'supplier_id' => 'Supplier ID',
            'tourname' => 'Tourname',
        ]; 
    } 

 public function getSupplier() {
        return $this->hasOne(Supplier::className(), ['id' => 'supplier_id']);
    }
}