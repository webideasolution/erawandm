<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "supplier".
 *
 * @property string $id
 * @property string $supplier_id
 * @property string $name
 * @property string $email
 * @property string $phno
 * @property string $created_at
 * @property string $upadated_at
 * @property string $status
 */
class Supplier extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'supplier';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'supplier_id', 'name', 'email', 'phno','city'], 'required','on' => 'create'],
            [[ 'supplier_id', 'name', 'email', 'phno','city'], 'required','on' => 'update'],
            [['id'], 'integer'],
            [['created_at', 'upadated_at','city'], 'safe'],
            [['status'], 'string'],
            
            [['supplier_id', 'phno'], 'string', 'max' => 20],
            [['name', 'email'], 'string', 'max' => 255],
            [['id'], 'unique'],
           
             [['email'], 'email'],
             ['phno', 'integer'],
             ['phno', 'string', 'min'=>8],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'supplier_id' => 'Supplier ID',
            'name' => 'Name',
            'email' => 'Email',
            'phno' => 'Phno',
            
            'city'=>'City',
            'created_at' => 'Created At',
            'upadated_at' => 'Upadated At',
            'status' => 'Status',
        ];
    }
    public function getSuppliertour() {
        return $this->hasMany(SupplierTourName::className(), ['supplier_id' => 'id']);
    }
}

