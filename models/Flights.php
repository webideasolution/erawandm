<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flights".
 *
 * @property string $id
 * @property string $flight_id
 * @property string $flight_date
 * @property string $source
 * @property string $destination
 * @property string $departure_time
 * @property string $arrival_time
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 */
class Flights extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'flights';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['flight_date', 'created_at', 'updated_at','visa_on_arrival'], 'safe'],
            [['source', 'destination', 'flight_date', 'flight_id', 'status'], 'required', 'on' => 'create_voucher_manual'],
            [['status', 'visa_on_arrival'], 'string'],
            [['flight_id'], 'string', 'max' => 50],
            [['source', 'destination', 'departure_time', 'arrival_time'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'flight_id' => 'Flight ID',
            'flight_date' => 'Flight Date',
            'source' => 'Source',
            'destination' => 'Destination',
            'departure_time' => 'Departure Time',
            'arrival_time' => 'Arrival Time',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }

    public function getVoucher() {
        return $this->hasOne(Voucher::className(), ['id' => 'voucher_id']);
    }
}
