<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "voucher".
 *
 * @property string $id
 * @property string $billing_name
 * @property string $trip_start
 * @property string $trip_end
 * @property int $adult_count
 * @property int $child_count
 * @property int $infant_count
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 */
class Voucher extends \yii\db\ActiveRecord {

    public $uploaded_file;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'voucher';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['adult_count', 'child_count', 'infant_count'], 'integer'],
            [['adult_count', 'child_count', 'infant_count', 'trip_start', 'trip_end', 'created_at', 'status'], 'required', 'on' => 'create_voucher'],
            [['adult_count', 'child_count', 'infant_count', 'trip_start', 'trip_end', 'created_at', 'status'], 'required', 'on' => 'create_voucher_manual'],
            [['status'], 'string'],
            [['billing_name'], 'string', 'max' => 255],
            [['created_at', 'updated_at', 'destination', 'is_payment_done'], 'safe'],
            [['uploaded_file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx', 'on' => 'create_voucher'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'billing_name' => 'Billing Name',
            'trip_start' => 'Trip Start',
            'trip_end' => 'Trip End',
            'adult_count' => 'Adult Count',
            'child_count' => 'Child Count',
            'infant_count' => 'Infant Count',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'uploaded_file' => 'Voucher'
        ];
    }

    public function getTransfers() {
        return $this->hasMany(Transfers::className(), ['voucher_id' => 'id'])->andOnCondition(['status' => '1'])->orderBy(['transfer_date' => SORT_ASC, 'transfer_time' => SORT_ASC]);
    }

    public function getFlights() {
        return $this->hasMany(Flights::className(), ['voucher_id' => 'id'])->andOnCondition(['status' => '1']);
    }

    public function getActivity() {
        return $this->hasMany(Activity::className(), ['voucher_id' => 'id'])->andOnCondition(['status' => '1'])->orderBy(['tour_date' => SORT_ASC, 'pickup_time' => SORT_ASC]);
    }

}
