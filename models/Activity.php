<?php

namespace app\models;

use Yii;
use app\models\Voucher;

/**
 * This is the model class for table "activity".
 *
 * @property string $id
 * @property string $voucher_id
 * @property string $tour_date
 * @property string $pickup_time
 * @property string $destination
 * @property string $tour_name
 * @property string $meeting_point
 * @property string $tour_type
 * @property string $enroute_location
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 */
class Activity extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'activity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['tour_date','service_status' , 'created_at', 'updated_at'], 'safe'],
            [['tour_date', 'created_at', 'status', 'pickup_time', 'tour_name', 'destination', 'meeting_point', 'tour_type','commission','cash_on_hand'], 'required', 'on' => 'create_voucher_manual'],
            [['status'], 'string'],
            [['pickup_time'], 'string', 'max' => 20],
            [['destination', 'tour_name', 'meeting_point', 'tour_type', 'enroute_location','commission','cash_on_hand'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'tour_date' => 'Tour Date',
            'voucher_id' => 'Voucher ID',
            'pickup_time' => 'Pickup Time',
            'destination' => 'Destination',
            'tour_name' => 'Tour Name',
            'meeting_point' => 'Meeting Point',
            'tour_type' => 'Tour Type',
            'enroute_location' => 'Enroute Location',
            'commission'=>'Commission',
            'cash_on_hand'=>'Cash On Hand ',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }

    public function getVoucher() {
        return $this->hasOne(Voucher::className(), ['id' => 'voucher_id']);
    }

}
