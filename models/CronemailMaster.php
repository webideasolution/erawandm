<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cronemail_master".
 *
 * @property string $id
 * @property string $mail_to
 * @property string $mail_from
 * @property string $subject
 * @property string $cc
 * @property string $body
 * @property string $attachment
 * @property string $scheduled_date
 * @property string $maildate
 * @property string $is_mail_sent
 * @property string $created_at
 * @property string $status
 */
class CronemailMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cronemail_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mail_to', 'subject', 'body', 'scheduled_date', 'is_mail_sent', 'created_at', 'status'], 'required'],
            [['mail_to', 'mail_from', 'cc', 'body', 'attachment', 'is_mail_sent', 'status'], 'string'],
            [['scheduled_date', 'maildate', 'created_at','linked_ids','mail_type'], 'safe'],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mail_to' => 'Mail To',
            'mail_from' => 'Mail From',
            'subject' => 'Subject',
            'cc' => 'Cc',
            'body' => 'Body',
            'attachment' => 'Attachment',
            'scheduled_date' => 'Scheduleddate',
            'maildate' => 'Maildate',
            'is_mail_sent' => 'Is Mail Sent',
            'created_at' => 'Created At',
            'status' => 'Status',
        ];
    }
}
