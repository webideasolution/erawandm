<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "invoice_incoming".
 *
 * @property string $id
 * @property string $supplieir_id
 * @property string $issue_date
 * @property string $due_date
 * @property string $upload_invoice
 * @property string $payment_status
 * @property string $currency
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 */
class InvoiceIncoming extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public $file;

    public static function tableName() {
        return 'invoice_incoming';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['issue_date', 'due_date', 'created_at'], 'safe'],
            [['upload_invoice', 'payment_status', 'currency', 'status'], 'string'],
            [['issue_date', 'due_date', 'payment_status', 'currency', 'status'], 'required'],
            [['supplieir_id'], 'string', 'max' => 20],
            [['file'], 'file', 'extensions' => 'pdf'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'supplieir_id' => 'Supplier ID',
            'issue_date' => 'Issue Date',
            'due_date' => 'Due Date',
            'upload_invoice' => 'Upload Invoice',
            'payment_status' => 'Payment Status',
            'currency' => 'Currency',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'file' => 'Upload invoice collected from the supplier. (PDF format Only)'
        ];
    }

    public function getInvoiceParticulars() {
        return $this->hasMany(IncomingInvoiceParticulars::className(), ['incoming_invoice_id' => 'id']);
    }

    public function getSupplier() {
        return $this->hasOne(Supplier::className(), ['supplier_id' => 'supplieir_id']);
    }

    public function getInvoiceTotalAmount() {
        $amount = 0;
        if (!empty($this->invoiceParticulars)) {
            foreach ($this->invoiceParticulars as $index => $value) {
                $amount += $value->amount;
            }
        }
        return $amount;
    }

    public function oneParticular($id) {
        $model = IncomingInvoiceParticulars::find()->where(['id' => $id])->one();
        return $model ?? NULL;
    }

}
