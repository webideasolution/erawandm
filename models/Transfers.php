<?php

namespace app\models;

use Yii;
use app\models\Voucher;
use app\models\Flights;

/**
 * This is the model class for table "transfers".
 *
 * @property string $id
 * @property string $transfer_date
 * @property string $transfer_time
 * @property string $transfer_objective
 * @property string $transfer_source
 * @property string $transfer_destination
 * @property string $transfer_type
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 */
class Transfers extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'transfers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['transfer_date','service_status' ,'created_at', 'updated_at'], 'safe'],
            [['transfer_date', 'transfer_time', 'transfer_objective', 'created_at', 'status'], 'required', 'on' => 'create_voucher_manual'],
            [['status'], 'string'],
            [['transfer_time'], 'string', 'max' => 20],
            [['transfer_objective', 'transfer_source', 'transfer_destination', 'transfer_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'transfer_date' => 'Transfer Date',
            'transfer_time' => 'Transfer Time',
            'transfer_objective' => 'Transfer Objective',
            'transfer_source' => 'Transfer Source',
            'transfer_destination' => 'Transfer Destination',
            'transfer_type' => 'Transfer Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }

    public function getVoucher() {
        return $this->hasOne(Voucher::className(), ['id' => 'voucher_id']);
    }

    public function getFlight() {
        return $this->hasOne(Flights::className(), ['voucher_id' => 'voucher_id'])->andOnCondition(['flight_date' => $this->transfer_date]);
    }

}
