<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "incoming_invoice_particulars".
 *
 * @property string $id
 * @property string $incoming_invoice_id
 * @property string $service_date
 * @property string $invoice_no
 * @property string $booking_no
 * @property string $tour_name
 * @property string $guest_name
 * @property int $adult_count
 * @property int $child_count
 * @property double $amount
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 */
class IncomingInvoiceParticulars extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'incoming_invoice_particulars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_date', 'created_at', 'updated_at'], 'safe'],
            [['booking_no', 'tour_name', 'incoming_invoice_id', 'guest_name', 'adult_count', 'child_count', 'amount', 'created_at', 'status'], 'required'],
            [['adult_count', 'child_count'], 'integer'],
            [['amount'], 'number'],
            [['status'], 'string'],
            [['invoice_no', 'booking_no'], 'string', 'max' => 20],
            [['tour_name', 'guest_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_date' => 'Service Date',
            'invoice_no' => 'Invoice No',
            'booking_no' => 'Booking No',
            'tour_name' => 'Tour Name',
            'guest_name' => 'Guest Name',
            'adult_count' => 'Adult Count',
            'child_count' => 'Child Count',
            'amount' => 'Amount',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }
}
