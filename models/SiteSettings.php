<?php

namespace app\models; 

use Yii; 

/** 
 * This is the model class for table "site_settings". 
 * 
 * @property string $id
 * @property string $slug
 * @property string $value
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 */ 
class SiteSettings extends \yii\db\ActiveRecord
{ 
    /** 
     * {@inheritdoc} 
     */ 
    public static function tableName() 
    { 
        return 'site_settings'; 
    } 

    /** 
     * {@inheritdoc} 
     */ 
    public function rules() 
    { 
        return [
            [['slug', 'value', 'created_at', 'updated_at', 'status'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['status'], 'string'],
            [['slug', 'value'], 'string', 'max' => 255],
        ]; 
    } 

    /** 
     * {@inheritdoc} 
     */ 
    public function attributeLabels() 
    { 
        return [ 
            'id' => 'ID',
            'slug' => 'Slug',
            'value' => 'Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ]; 
    } 
} 
