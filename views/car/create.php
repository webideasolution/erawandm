<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\models\Videos;
use app\components\CustomPagination;
$this->title = 'create car';
$this->params['breadcrumbs'][] = $this->title;
/* @var $this yii\web\View */
/* @var $model app\models\InvoiceIncoming */
/* @var $form ActiveForm */
?>
<div class="car-create">

    <?php $form = ActiveForm::begin(['id' => 'car', 'options' => ['enctype' => 'multipart/form-data', 'method'=>'post']]); ?>
      <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'capacity') ?>
                
                   
                    <?= $form->field($model, 'fare') ?>
    
                              
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- invoice-create -->
