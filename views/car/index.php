<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\components\CustomPagination;
use richardfan\widget\JSRegister;

$this->title = 'Car List';
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('http://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css');
$this->registerJsFile('http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
?>

<h1 class="page-title"> Car List
    <small>All car are listed here</small>
</h1>


<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h1>Car Table</h1>
                        <a class="pull-right btn btn-primary" href="<?php echo Url::to(['car/create']); ?>"> create car</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <?php if (Yii::$app->session->hasFlash('carFormSubmitted')) { ?>
                            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button> <?php echo Yii::$app->session->getFlash('carFormSubmitted'); ?></div>
                        <?php } ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th> Name</th>
                                    <th>capacity</th>
                                    <th>Fare</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($model as $val) {
                                    ?>
                                    <tr>
                                        <td><?php echo $val->name; ?></td>
                                        <td><?php echo $val->capacity; ?></td>
                                        <td><?php echo $val->fare; ?></td>
                                        <td><?php
                                            if ($val->status == '1') {
                                                echo '<span class="badge bg-green">Active</span>';
                                            } else {
                                                echo '<span class="badge bg-red">Inactive</span>';
                                            }
                                            ?></td>

                                        <td>
                                            <a href="<?= Yii::$app->request->baseUrl . '/car/edit/' . $val->id; ?> "><span class="badge bg-yellow"><i class="fa fa-pencil"></i> Edit</span></a>&nbsp;&nbsp;&nbsp;
                                            <a href="<?= Yii::$app->request->baseUrl . '/car/delete/' . $val->id; ?> "><span class="badge bg-red"><i class="fa fa-trash"></i> Delete</span></a>&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- .box-body -->

                </div>

            </div>
        </div>
    </section>
</div>
<?php JSRegister::begin(); ?>
<script>
    $(document).ready(function () {
        $("#example1").DataTable();
        //        $("#myTable1").DataTable();
    });
</script>
<?php JSRegister::end(); ?>
