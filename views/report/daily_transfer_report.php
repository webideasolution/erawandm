<style>
    .date-pick {
        width: 40%;
        display: inline-block;
    }
    .page-title-conntainer {
        margin: 1rem auto;
    }
    span.page-title {
        font-size: 3rem;
        line-height: 5rem;
        padding: 10px 10px;
        font-weight: 600;
        text-transform: capitalize;
    }
    span.page-title-2 {
        font-size: 1.6rem;
        line-height: 2rem;
        padding: 10px 10px;
        font-weight: 300;
        text-transform: capitalize;
    }
    #transfer-filter-form {
        padding: 20px;
    }
</style>

<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\models\Videos;
use app\components\CustomPagination;
use yii\base\View;
use richardfan\widget\JSRegister;

$this->title = 'Daily Transfer List';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css');
$this->registerCssFile('http://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css');
$this->registerJsFile('https://cdn.jsdelivr.net/momentjs/latest/moment.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<div class="transfer-filter">
    <div class="col-md-12 page-title-conntainer">
        <span class="page-title"> Daily Transfer list</span>
        <span class="page-title-2">Day Wise Transfers are listed here</span>
    </div>
    <div>
        <button class="btn btn-primary" type="button" id="ps_download_pdf">Download PDF</button>   
        <button class="btn btn-primary" type="button" id="copy_job_order">Copy Job Order</button>
    </div>
    <?php $form = ActiveForm::begin(['id' => 'transfer-filter-form', 'options' => ['method' => 'post']]) ?>

    <div class="row">
        <div class="col-md-6">
            <div class="date-container form-group clearfix">
                <div class="col-md-2 text-right"> Order date </div>
                <div class="col-md-10"> 
                    <input type="text" name="order-start-date" class="form-control" id="startDate-1" value="<?= $_POST['order-start-date'] ?? ''; ?>">
                </div> 
            </div>    
        </div>    
        <div class="col-md-6">
            <div class="date-container form-group clearfix">
                <div class="col-md-2 text-right">Transfer Date  </div>
                <div class="col-md-10">
                    <input type="text" name="tour-start-date" class="form-control" id="startDate-2" value="<?= $_POST['tour-start-date'] ?? ''; ?>">
                </div>
            </div> 
        </div> 
        <div class="col-md-6">
            <div class="date-container form-group clearfix">
                <div class="col-md-2 text-right">City</div>
                <div class="col-md-10">
                    <select name="city" class="form-control">
                        <option value="">Select City Filteration</option>
                        <option value="1" <?= isset($_POST['city']) && $_POST['city'] == "1" ? 'selected' : ''; ?>>Bangkok-Pattaya</option>
                        <option value="2" <?= isset($_POST['city']) && $_POST['city'] == "2" ? 'selected' : ''; ?>>Pattaya-Bangkok</option>
                        <option value="3" <?= isset($_POST['city']) && $_POST['city'] == "3" ? 'selected' : ''; ?>>Phuket-Krabi</option>
                        <option value="4" <?= isset($_POST['city']) && $_POST['city'] == "4" ? 'selected' : ''; ?>>Krabi-Phuket</option>
                        <option value="5" <?= isset($_POST['city']) && $_POST['city'] == "5" ? 'selected' : ''; ?>>All Bangkok-Pattaya</option>
                        <option value="6" <?= isset($_POST['city']) && $_POST['city'] == "6" ? 'selected' : ''; ?>>All Krabi-Phuket</option>
                    </select>
                </div>
            </div> 
        </div> 
        <div class="col-md-6 col-md-offset-3">
            <button class="btn btn-default" type="submit">Submit</button>
        </div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>

<div class="table-responsive" style="width: 100%">
<table class="table table-bordered table-striped table-condensed" id="myTable">
    <thead>
        <tr>
            <th colspan="15" class="text-center table-header">TRANSFER ITINERARY</th>
        </tr>
        <tr>
            <th>Customer Name</th>
            <th>Cars</th>
            <th class="numeric">Adult</th>
            <th class="numeric">Child</th>
            <th class="numeric">Infant</th>
            <th>Pickup Time</th>
            <th class="numeric">Pickup</th>
            <th class="numeric">Drop-Off</th>
            <th>Order Date</th>
            <th>Transfer Date</th>
            <th class="numeric">Type</th>
            <th class="numeric">Transfer Type</th>
            <th class="numeric">Booking ID</th>
            <th class="numeric">Status</th>
            <th class="numeric">View</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (count($model) > 0) {
            foreach ($model as $val) {
                ?>
                <tr>
                    <td><?= $val->voucher->billing_name; ?></td>
                    <?php
                    $total_pax = $val->voucher->adult_count + $val->voucher->child_count + $val->voucher->infant_count;
                    $output;
                    if ($total_pax > 0) {
                        if ($total_pax <= 3) {
                            $output = 'Car Sedan';
                        } else if ($total_pax <= 5) {

                            $output = 'Sedan';
                        } else if ($total_pax <= 8) {

                            $output = 'Van';
                        } else {
                            $van_count = 1;
                            $sedan_count = 0;
                            $car_sedan_count = 0;
                            $total_pax = $total_pax - 8;

                            while ($total_pax > 0) {
                                if ($total_pax <= 3) {
                                    $car_sedan_count++;
                                    $total_pax = $total_pax - 3;
                                } else if ($total_pax <= 5) {
                                    $sedan_count++;
                                    $total_pax = $total_pax - 5;
                                } else if ($total_pax <= 8) {
                                    $van_count++;
                                    $total_pax = $total_pax - 8;
                                }
                            }

                            $output = $van_count >= 1 ? $van_count . ' Vans,' : '1 Van,';
                            $output .= ' ' . $sedan_count > 0 ? $sedan_count == 1 ? '1 Sedan,' : $sedan_count . ' Sedans,' : '';
                            $output .= ' ' . $car_sedan_count > 0 ? $car_sedan_count == 1 ? '1 Car Sedan' : $van_count . 'Car Sedans' : '';
                        }
                    } else {
                        $output = 'No Car Needed';
                    }
                    ?>

                    <td class="numeric"><?= $output; ?></td>
                    <td class="numeric"><?= $val->voucher->adult_count; ?></td>
                    <td class="numeric"><?= $val->voucher->child_count; ?></td>
                    <td class="numeric"><?= $val->voucher->infant_count; ?></td>
                    <td><?= !empty($val->transfer_time) ? $val->transfer_time : 'NA'; ?></td>
                    <td class="numeric"><?= $val->transfer_source; ?></td>
                    <td class="numeric"><?= $val->transfer_destination; ?></td>
                    <td><?= date('d/m/Y',strtotime($val->created_at)); ?></td>
                    <td><?= date('d/m/Y',strtotime($val->transfer_date)); ?></td>
                    <td class="numeric"><?= $val->transfer_objective; ?></td>
                    <td class="numeric"><?= $val->transfer_type; ?></td>
                    <td class="numeric"><?= '#' . $val->voucher->id; ?></td>
                    <td class="numeric"><?php
                        if ($val->status == '1') {
                            echo '<span class="badge bg-green">Active</span>';
                        } else {
                            echo '<span class="badge bg-red">Inactive</span>';
                        }
                        ?></td>
                    <td>
                        <a href="<?= Yii::$app->request->baseUrl . '/voucher/view/' . $val->voucher->id; ?> "><span class="badge bg-yellow"><i class="fa fa-eye"></i> View</span></a>&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <?php
            }
        }
        ?>
        </tr>
    </tbody>
</table>
</div>
<div class="copy-able-div" style="display: none;" id="copy_able_div">
    <?php
    if (count($model) > 0) {
        foreach ($model as $val) {
            ?>
            Job ID:<?= 'EIC-' . sprintf('%04d', $val->id); ?>
            Name:&nbsp;&nbsp;<?= $val->voucher->billing_name; ?><br>
            Adult:&nbsp;&nbsp;<?= $val->voucher->adult_count; ?>,&nbsp;&nbsp;Child:&nbsp;&nbsp; <?= $val->voucher->child_count; ?><br>
            Tour Date: <?= date('d-m-y', strtotime($val->transfer_date)); ?>,&nbsp;&nbsp; Time: &nbsp;&nbsp;<?= $val->transfer_time ?><br>
            Pickup:&nbsp;&nbsp; <?= $val->transfer_source; ?><br>
            Drop Off :&nbsp;&nbsp; <?= $val->transfer_destination; ?><br>
            <br>
            <?php
        }
    }
    ?>
</div>


<?php JSRegister::begin(); ?>
<script>
    $(document).ready(function () {
        $("#myTable").DataTable();
        //        $("#myTable1").DataTable();
    });
    //    $(function () {
    //        $("#startDate-1").datepicker();
    //    });
    //    $(function () {
    //        $("#startDate-2").datepicker();
    //    });
    //    $(function () {
    //        $("#endDate-1").datepicker();
    //    });
    //    $(function () {
    //        $("#endDate-2").datepicker();
    //    });

    $(document).ready(function () {
        $("#ps_download_pdf").on('click', function () {
            $('<form action="<?php echo Url::to(['download-transfer-pdf']); ?>" id="pdf-transfer-filter-form" method="post"></form>').appendTo('body');
            $("#transfer-filter-form").find(":input").clone().appendTo("#pdf-transfer-filter-form");
            //            console.log($("#pdf-transfer-filter-form"));
            $("#pdf-transfer-filter-form").submit();
        });
        $("#copy_job_order").on('click', function () {
            var $temp = $("<div>");
            $("body").append($temp);
            $temp.attr("contenteditable", true)
                    .html($("#copy_able_div").html()).select()
                    .on("focus", function () {
                        document.execCommand('selectAll', false, null)
                    })
                    .focus();
            document.execCommand("copy");
            $temp.remove();
            alert('Job order copied to the clipboard.');
        });
        $("#startDate-1").val('');

    });
    $('#startDate-1').daterangepicker({
        format: 'M/DD hh:mm A',
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "alwaysShowCalendars": true,
        "cancelClass": "btn-danger"

    });
    $('#startDate-1').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    $('#startDate-2').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    $('#startDate-2').daterangepicker({
        format: 'M/DD hh:mm A',
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "alwaysShowCalendars": true,
        "cancelClass": "btn-danger"

    });
</script>
<?php JSRegister::end(); ?>