<style>
    .date-pick {
        width: 40%;
        display: inline-block;
    }
    .page-title-conntainer {
        margin: 1rem auto;
    }
    span.page-title {
        font-size: 3rem;
        line-height: 5rem;
        padding: 10px 10px;
        font-weight: 600;
        text-transform: capitalize;
    }
    span.page-title-2 {
        font-size: 1.6rem;
        line-height: 2rem;
        padding: 10px 10px;
        font-weight: 300;
        text-transform: capitalize;
    }
</style>

<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\models\Videos;
use app\components\CustomPagination;
use richardfan\widget\JSRegister;

$this->title = 'Daily Tour List';
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css');
$this->registerCssFile('http://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css');
$this->registerJsFile('https://cdn.jsdelivr.net/momentjs/latest/moment.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<div class="transfer-filter">
    <div class="col-md-12 page-title-conntainer">
        <span class="page-title"> Daily Tour list</span>
        <span class="page-title-2">Day Wise Tours are listed here</span>
    </div>
    <div>
        <button class="btn btn-primary" type="button" id="ps_download_pdf">Download PDF</button>   
    </div>
    <?php $form = ActiveForm::begin(['id' => 'transfer-filter-form', 'options' => ['method' => 'post']]) ?>

    <div class="col-md-6">
        <div class="date-container form-group clearfix">
            <div class="col-md-2 text-right"> Order date </div>
            <div class="col-md-10"> 
                <input type="text" name="order-start-date" class="form-control" id="startDate-1" value="<?= $_POST['order-start-date'] ?? ''; ?>">
            </div> 
        </div>    
    </div>    
    <div class="col-md-6">
        <div class="date-container form-group clearfix">
            <div class="col-md-2 text-right">Transfer Date  </div>
            <div class="col-md-10">
                <input type="text" name="tour-start-date" class="form-control" id="startDate-2" value="<?= $_POST['tour-start-date'] ?? ''; ?>">
            </div>
        </div> 
    </div> 
    <div class="col-md-6">
        <div class="date-container form-group clearfix">
            <div class="col-md-2 text-right">City</div>
            <div class="col-md-10">
                <select name="city" class="form-control" id="ps_filter_report_tour">
                    <option value="">All City</option>
                    <option value="phuket" <?= isset($_POST['city']) && $_POST['city'] == 'phuket' ? 'selected' : '' ?>>Phuket</option>
                    <option value="krabi" <?= isset($_POST['city']) && $_POST['city'] == 'krabi' ? 'selected' : '' ?>>Krabi</option>
                    <option value="pattaya" <?= isset($_POST['city']) && $_POST['city'] == 'pattaya' ? 'selected' : '' ?>>Pattaya</option>
                    <option value="bangkok" <?= isset($_POST['city']) && $_POST['city'] == 'bangkok' ? 'selected' : '' ?>>Bangkok</option>
                    <option value="chiangmai" <?= isset($_POST['city']) && $_POST['city'] == 'chiangmai' ? 'selected' : '' ?>>Chiangmai</option>
                    <option value="samui" <?= isset($_POST['city']) && $_POST['city'] == 'samui' ? 'selected' : '' ?>>Samui</option>
                </select>
            </div>
        </div> 
    </div> 
    <div class="col-md-6 col-md-offset-3">
        <button class="btn btn-default" type="submit">Submit</button>
    </div>
</div> 
<?php ActiveForm::end(); ?>



</div>
<div class="table-responsive" style="width: 100%">
    <table class="table table-bordered table-striped table-condensed" id="myTable">
        <thead>
            <tr>
                <th colspan="14" class="text-center table-header">TOUR ITINERARY</th>
            </tr>
            <tr>
                <th>Customer Name</th>
                <th class="numeric">Adult</th>
                <th class="numeric">Child</th>
                <th class="numeric">Infant</th>
                <th>pickup Time</th>
                <th class="numeric">Meeting Point</th>
                <th class="numeric">Destination</th>
                <th class="numeric">Tour name</th>
                <th>Order Date</th>
                <th>Tour Date</th>
                <th class="numeric">Tour Type</th>
                <th class="numeric">Booking ID</th>
                <th class="numeric">Status</th>
                <th class="numeric">View</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $flag = "info";
            $flag1 = FALSE;
            if (count($model) > 0) {
                foreach ($model as $index => $val) {
                    if ($flag1 == TRUE) {
                        ?>
                        <tr class="<?= $flag; ?>">
                            <?php
                            if (isset($model[$index + 1]) && strtoupper($model[$index + 1]->tour_date) == strtoupper($val->tour_date) && strpos(preg_replace('/\s+/u', '', strtoupper($model[$index + 1]->tour_name)), preg_replace('/\s+/u', '', strtoupper($val->tour_name)))) {

                                $flag1 = TRUE;
                            } else {
                                $flag = $flag == "info" ? "warning" : "info";
                                $flag1 = FALSE;
                            }
                        } else if (isset($model[$index + 1]) && strtoupper($model[$index + 1]->tour_date) == strtoupper($val->tour_date) && strpos(preg_replace('/\s+/u', '', strtoupper($model[$index + 1]->tour_name)), preg_replace('/\s+/u', '', strtoupper($val->tour_name)))) {
                            ?>
                        <tr class="<?= $flag; ?>">
                            <?php
                            $flag1 = TRUE;
                        } else {
                            $flag1 = FALSE;
                            ?>
                        <tr>
                        <?php }
                        ?>

                        <td><?= $val->voucher->billing_name; ?></td>
                        <td class="numeric"><?= $val->voucher->adult_count; ?></td>
                        <td class="numeric"><?= $val->voucher->child_count; ?></td>
                        <td class="numeric"><?= $val->voucher->infant_count; ?></td>
                        <td><?= !empty($val->pickup_time) ? $val->pickup_time : 'NA'; ?></td>
                        <td class="numeric"><?= $val->meeting_point; ?></td>
                        <td class="numeric"><?= $val->destination; ?></td>
                        <td class="numeric"><?= $val->tour_name; ?></td>
                        <td><?= date('d/m/Y', strtotime($val->created_at)); ?></td>
                        <td><?= date('d/m/Y', strtotime($val->tour_date)); ?></td>
                        <td class="numeric"><?= $val->tour_type; ?></td>
                        <td class="numeric"><?= '#' . $val->voucher->id; ?></td>
                        <td class="numeric"><?php
                            if ($val->status == '1') {
                                echo '<span class="badge bg-green">Active</span>';
                            } else {
                                echo '<span class="badge bg-red">Inactive</span>';
                            }
                            ?></td>
                        <td>
                            <a href="<?= Yii::$app->request->baseUrl . '/voucher/view/' . $val->voucher->id; ?> "><span class="badge bg-yellow"><i class="fa fa-eye"></i> View</span></a>&nbsp;&nbsp;&nbsp;</td>

                    </tr>
                    <?php
                }
            }
            ?>
            </tr>
        </tbody>
    </table>
</div>
<?php JSRegister::begin(); ?>
<script>
    $(document).ready(function () {
        $("#myTable").DataTable();
        $("#startDate-1").val('');
//        $("#myTable1").DataTable();
    });

    $("#ps_download_pdf").on('click', function () {

        $(document).ready(function () {
            $('<form action="<?php echo Url::to(['download-tour-pdf']); ?>" id="pdf-transfer-filter-form" method="post"></form>').appendTo('body');
            $("#transfer-filter-form").find(":input").clone().appendTo("#pdf-transfer-filter-form");
            $("#pdf-transfer-filter-form").submit();
        });
    });
    $('#startDate-1').daterangepicker({
        format: 'M/DD hh:mm A',
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "alwaysShowCalendars": true,
        "cancelClass": "btn-danger"

    });
    $('#startDate-1').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    $('#startDate-2').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    $('#startDate-2').daterangepicker({
        format: 'M/DD hh:mm A',
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "alwaysShowCalendars": true,
        "cancelClass": "btn-danger"

    });
</script>
<?php JSRegister::end(); ?>