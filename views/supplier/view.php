<div class="row">
    <div class="col-md-12">
        <h2 class="section-heading text-center">
            Supplier  For : <?= $model->supplier_id; ?>
        </h2>
    </div>

    <div class="col-md-12">
        <div class="head-text-container">
            <span class="head-text">
                <label> Name</label> : <?= $model->name; ?>
            </span>
        </div>
    </div>

    <div class="col-md-12">
        <div class="head-text-container">

            <span class="head-text">
                <label>   Email </label>: <?= $model->email; ?>
            </span>
        </div>
    </div>
    <div class="col-md-12">
        <div class="head-text-container">

            <span class="head-text">
                <label>   PhNo </label>: <?= $model->phno; ?>
            </span>
        </div>
    </div>
    <div class="col-md-12">
        <div class="head-text-container">

            <span class="head-text">
                <label>   City </label>: <?= $model->city; ?>
            </span>
        </div>
    </div>
    <div class="col-md-12">
        <div class="head-text-container">
            <span>
                <table id="ps_tour_table" class="table table-bordered table-striped">
            <thead>
                <tr>
                     <th rowspan="2">
                        Tour Name
                    </th>
                    <th colspan="2" class="text-center">SIC PRICE</th>
                    <th colspan="2" class="text-center">Ticket PRICE</th>
                    
                </tr>
                <tr>
                    <th >Adult</th>
                    <th >Child</th>
                    <th >Adult</th>
                    <th >Child</th>

                </tr>
            </thead>
            <tbody>
                 <?php
                $var = '';
                if (isset($model->suppliertour) && !empty($model->suppliertour)) {
                    foreach ($model->suppliertour as $index1 => $value1) { ?>
                        
                <tr> <td ><?=$value1->tourname ; ?></td>
               <td><?= $value1->sic_adult_price; ?></td>
                     <td><?= $value1->sic_child_price; ?></td>
                    <td><?= $value1->ticket_adult_price; ?></td>
                     <td><?= $value1->ticket_child_price; ?></td>
                <?php      
                   
                 }
                  
                } else {
                     'No tour name added yet.';
                }
              ?>
            
                </tr>
            </tbody>
             </table>
            </span>
        </div>
    </div>
</div>
