<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use richardfan\widget\JSRegister;
use app\components\CustomPagination;

$this->registerCssFile('http://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css');
$this->registerJsFile('http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->title = 'Supplier List';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1 class="page-title"> Supplier List
    <small>All Supplier are listed here</small>
</h1>


<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h1>Supplier Table</h1>
                        <a class="pull-right btn btn-primary" href="<?php echo Url::to(['supplier/create']); ?>"> create Supplier</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <?php if (Yii::$app->session->hasFlash('Suppliercreate')) { ?>
                            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close">

                                </button> <?php echo Yii::$app->session->getFlash('Suppliercreate'); ?></div>
                        <?php } ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th> SupplierId</th>
                                    <th>Email</th>
                                    <th>PhNo</th>


                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($model) && !empty($model)):
                                    foreach ($model as $val) {
                                        ?>
                                        <tr>
                                            <td><?php echo $val->supplier_id; ?></td>
                                            <td><?php echo $val->email; ?></td>
                                            <td><?php echo $val->phno; ?></td>

                                            <td>
                                                <a href="<?= Yii::$app->request->baseUrl . '/supplier/view/' . $val->id; ?> "><span class="badge bg-yellow"><i class="fa fa-eye"></i> View</span></a>&nbsp;&nbsp;&nbsp;
                                                <a href="<?= Yii::$app->request->baseUrl . '/supplier/edit/' . $val->id; ?> "><span class="badge bg-yellow"><i class="fa fa-pencil"></i> Edit</span></a>&nbsp;&nbsp;&nbsp;
                                                <a href="<?= Yii::$app->request->baseUrl . '/supplier/delete/' . $val->id; ?> "><span class="badge bg-red"><i class="fa fa-trash"></i> Delete</span></a>&nbsp;&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- .box-body -->

                </div>

            </div>
        </div>
    </section>
</div>
<?php JSRegister::begin(); ?>
<script>
    $(document).ready(function () {
        $("#example1").DataTable();
        //        $("#myTable1").DataTable();
    });
</script>
<?php JSRegister::end(); ?>


