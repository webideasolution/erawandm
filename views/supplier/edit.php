<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\BaseUrl;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\components\CustomPagination;
use richardfan\widget\JSRegister;

$this->title = 'Supplier';
//$this->params['breadcrumbs'][] = $this->title;
/* @var $this yii\web\View */
/* @var $model app\models\InvoiceIncoming */
/* @var $form ActiveForm */
?>
<?php $form = ActiveForm::begin(['id' => 'supplier', 'options' => ['method' => 'post']]); ?>
<div class="supplier-create">
    <div class="col-md-12">

        <div class="col-md-2">   <?= $form->field($model, 'supplier_id')->textInput(['autofocus' => true]) ?></div>

        <div class="col-md-2">         <?= $form->field($model, 'name') ?> </div>


        <div class="col-md-2">  <?= $form->field($model, 'email') ?> </div>

        <div class="col-md-2">      <?= $form->field($model, 'phno') ?> </div>

        <?php $city = ['BKK' => 'BANGKOK', 'PT' => 'PATTAYA', 'HKT' => 'PHUKET', 'KBV' => 'KRABI', 'USM' => 'SAMUI', 'CNX' => 'CHIANGMAI']; ?>
        <div class="col-md-2">  <?= $form->field($model, 'city')->dropDownList($city, ['prompt' => 'Select Option']); ?> </div>
    </div>
    <div class="col-md-12">
        <table id="ps_tour_table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th rowspan="2">
                        Tour Name
                    </th>
                    <th colspan="2" class="text-center">SIC PRICE</th>
                    <th colspan="2" class="text-center">Ticket PRICE</th>
                    <th rowspan="2">
                        <button type="button" id="" class="add-item btn btn-primary">Add More</button>
                    </th>
                </tr>
                <tr>
                    <th >Adult</th>
                    <th >Child</th>
                    <th >Adult</th>
                    <th >Child</th>

                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($model->suppliertour) && !empty($model->suppliertour)) {
                    foreach ($model->suppliertour as $index1 => $value1) {
                        ?>
                        <tr>
                            <td>
                                <input type="text" name="SupplierTourName[<?= $index1; ?>][tourname]" class="form-control" value="<?= $value1->tourname; ?>"/>
                            </td>
                            <td>
                                <input type="text" name="SupplierTourName[<?= $index1; ?>][sic_adult_price]" class="form-control" value="<?= $value1->sic_adult_price; ?>"/>
                            </td>
                            <td>
                                <input type="text" name="SupplierTourName[<?= $index1; ?>][sic_child_price]" class="form-control" value="<?= $value1->sic_child_price; ?>"/>
                            </td>
                            <td>
                                <input type="text" name="SupplierTourName[<?= $index1; ?>][ticket_adult_price]" class="form-control" value="<?= $value1->ticket_adult_price; ?>"/>
                            </td>
                            <td>
                                <input type="text" name="SupplierTourName[<?= $index1; ?>][ticket_child_price]" class="form-control" value="<?= $value1->ticket_child_price; ?>"/>
                            </td>
                            <td>
                                <button  type="button" id="" class="btn btn-danger">Remove</button>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>



            </tbody>
        </table>
        <div class="form-group" style="padding-top:23px">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div><!-- invoice-create -->
<?php JSRegister::begin(); ?>
<script>
    $(document).ready(function () {
        $("#ps_tour_table").on("click", ".btn.btn-danger", function (event) {
            $(this).closest("tr").remove();
        });
        var tour_counter = 1;
        $(".add-item").on('click', function () {
            $.ajax({
                type: "POST",
                evalScripts: true,
                url: '<?= Url::to(Baseurl::base() . '/supplier/ajax-tour-add'); ?>',
                data: ({_csrf: yii.getCsrfToken(), counter: tour_counter}),
                success: function (data) {
                    $("#ps_tour_table").find('tbody:last').append('<tr>').find('tr:last').append(data);
                    tour_counter++;
                }
            });
        });
    });
</script>
<?php JSRegister::end(); ?>