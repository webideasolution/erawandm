<?php

use richardfan\widget\JSRegister;

$this->registerCssFile('@web/assets/admin/js/morris-chart/morris.css');
$this->registerCssFile('@web/assets/admin/js/c3-chart/c3.css');
$this->registerCssFile('http://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css');
$this->registerJsFile('http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('http://d3js.org/d3.v3.min.js');
$this->registerJsFile('@web/assets/admin/js/c3-chart/c3.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('@web/assets/admin/js/morris-chart/morris.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('@web/assets/admin/js/morris-chart/raphael-min.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<div class="row">
    <div class="col-md-12">
        <div class="col-sm-6">
            <div class="panel">
                <header class="panel-heading bg-blue">
                    City & Code

                </header>
                <div class="panel-body">
                    <table class="table  table-hover general-table">
                        <thead>
                            <tr>
                                <th> City</th>
                                <th>CityCode</th>                               
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="#">Bangkok</a></td>
                                <td class="hidden-phone">BKK</td>

                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        Pattaya
                                    </a>
                                </td>
                                <td class="hidden-phone">KBV</td>

                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        Phuket
                                    </a>
                                </td>
                                <td class="hidden-phone">HKT</td>

                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        KRABI
                                    </a>
                                </td>
                                <td class="hidden-phone">KBV</td>

                            </tr>
                            <tr>
                                <td><a href="#">SAMUI</a></td>
                                <td class="hidden-phone">USM</td>

                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        CHINGMAI
                                    </a>
                                </td>
                                <td class="hidden-phone">CNX</td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel">
                <header class="panel-heading bg-red">
                    Recent Incoming Invoice Payment to be maid

                </header>
                <div class="panel-body">
                    <table class="table  table-hover general-table" id='myTable'>
                        <thead>
                            <tr>
                                <th> ID</th>
                                <th>Due Date</th>                             
                                <th> Remaining Date </th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($model as $index => $value) {
                                ?>

                                <tr><td bg color="red">Invoice:<?php echo $value->id ?></td>                      
                                    <td><?php echo $value->due_date ?></td>
                                    <td><span class="badge bg-yellow"><?php
                                            $date = date("Y-m-d");
                                            $data = ($value->due_date);
                                            $d_start = new DateTime($date);
                                            $d_end = new DateTime($data);
                                            $diff = $d_start->diff($d_end);
                                            echo $diff->format("%R%a days");
                                            ?></td></span>
                                    <td> <a href="<?php echo Yii::$app->request->baseurl . "/invoice/in-view?id=" . $value->id ?>"><span class="badge bg-red"><i class="fa fa-eye"></i>View </span></a>
                                    </td></tr>

                                <?php
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-sm-6">
            <div class="panel">
                <header class="panel-heading bg-red">
                    Recent Outgoing Invoice Payment to be made

                </header>
                <div class="panel-body">
                    <table class="table  table-hover general-table" id='myTable1'>
                        <thead>
                            <tr>
                                <th> ID</th>
                                <th>Due Date</th>                             
                                <th> Remaining Date </th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($model1 as $index => $value) {
                                ?>

                                <tr><td bg color="red">Invoice:<?php echo $value->id ?></td>                      
                                    <td><?php echo $value->due_date ?></td>
                                    <td><span class="badge bg-yellow"><?php
                                            $date = date("Y-m-d");
                                            $data = ($value->due_date);
                                            $d_start = new DateTime($date);
                                            $d_end = new DateTime($data);
                                            $diff = $d_start->diff($d_end);
                                            echo $diff->format("%R%a days");
                                            ?></td></span>
                                    <td> <a href="<?php echo Yii::$app->request->baseurl . "/invoice/out-view?id=" . $value->id ?>"><span class="badge bg-red"><i class="fa fa-eye"></i>View </span></a>
                                    </td></tr>

                                <?php
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="chart" id="combine-chart">
        </div>
        <div class="col-md-6">
            <div id="graph-donut"></div>
        </div>
    </div>
</div>
</div>
<?php JSRegister::begin(); ?>
<script>
    $(document).ready(function () {
        $("#myTable").DataTable();
        $("#myTable1").DataTable();
    });
    $(function () {
        var chart = c3.generate({
            bindto: '#combine-chart',
            data: {
                columns: [
                    ['data1', 30, 20, 50, 40, 60, 50],
                    ['data2', 200, 130, 90, 240, 130, 220],
                    ['data3', 300, 200, 160, 400, 250, 250],
                    ['data4', 200, 130, 90, 240, 130, 220],
                    ['data5', 130, 120, 150, 140, 160, 150]
                ],
                types: {
                    data1: 'bar',
                    data2: 'bar',
                    data3: 'spline',
                    data4: 'line',
                    data5: 'bar'
                },
                groups: [
                    ['data1', 'data2']
                ]
            },
            axis: {
                x: {
                    type: 'categorized'
                }
            }
        });

    });
    Morris.Donut({
    element: 'graph-donut',
    data: [
        {value: 70, label: 'foo', formatted: 'at least 70%' },
        {value: 15, label: 'bar', formatted: 'approx. 15%' },
        {value: 10, label: 'baz', formatted: 'approx. 10%' },
        {value: 5, label: 'A really really long label', formatted: 'at most 5%' }
    ],
    backgroundColor: '#fff',
    labelColor: '#1fb5ac',
    colors: [
        '#E67A77','#D9DD81','#79D1CF','#95D7BB'
    ],
    formatter: function (x, data) { return data.formatted; }
});
</script>
<?php JSRegister::end(); ?>