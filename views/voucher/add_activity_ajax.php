
<?php
use app\models\SupplierTourName;
use yii\helpers\ArrayHelper;
?>
<td>
    <div class="form-group">
        <input type="text" id="activity-<?= $counter; ?>-tour_date" class="datepicker form-control" name="Activity[<?= $counter; ?>][tour_date]">
    </div>
</td>
<td>
    <div class="form-group">
        <input type="text" id="activity-<?= $counter; ?>-pickup_time" class="form-control" name="Activity[<?= $counter; ?>][pickup_time]">
    </div>
</td>
<td>
    <div class="form-group">
        <input type="text" id="activity-<?= $counter; ?>-destination" class="form-control" name="Activity[<?= $counter; ?>][destination]">
    </div>
</td>
<td>
    <div class="form-group">
<!-- <input type="text" id="activity-<?= $counter; ?>-tour_name" class="form-control" name="Activity[<?= $counter; ?>][tour_name]" ">-->
 <select  id="activity-<?= $counter; ?>-tour_name" class="form-control" name="Activity[<?= $counter; ?>][tour_name]" >
                                            
                                          <?php
                                          $tourname=SupplierTourName::find()->all();
       $tourdata=ArrayHelper::map($tourname,'id','tourname');
                                          foreach($tourdata as $index =>$value): ?>
                                            
                                            <option value="<?= $value; ?>"><?php echo $value  ?></option>
                                           <?php endforeach; ?>
                                        </select>
                                        
    </div>
</td>
<td>
    <div class="form-group">
        <input type="text" id="activity-<?= $counter; ?>-meeting_point" class="form-control" name="Activity[<?= $counter; ?>][meeting_point]">
    </div>
</td>
<td>
    <div class="form-group">
        <input type="text" id="activity-<?= $counter; ?>-tour_type" class="form-control" name="Activity[<?= $counter; ?>][tour_type]">
    </div>
</td>
<td>
    <div class="form-group">
        <input type="text" id="activity-<?= $counter; ?>-enroute_location" class="form-control" name="Activity[<?= $counter; ?>][enroute_location]"+>
    </div>
</td>
 <?php if($referrer == 'edit' ){?>
<td>    
   
     <div class="form-group">
         
              <select id="activity-<?= $counter; ?>-service_status" class="form-control" name="Activity[<?= $counter; ?>][service_status]">
                        <option value=""></option>
                        <option value="0">Pending</option>
                        <option value="1"> completed</options>
                        <option value="2">No Show</option>
               </select>
     </div>
  </td>
 <?php }?>
<td>
    <div class="text-center full-width-btn">
        <button type="button" class="remove-transfer btn btn-danger"><i class="glyphicon glyphicon-minus"></i>Remove</button>
    </div>
</td>