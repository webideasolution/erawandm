<style>

    .full-width-btn {
        flex-basis: 100%;
    }

    .add-more-btn-style {
        padding: 1rem 0;
    }
    button i {
        font-size: 12px;
        line-height: 1.4;
        padding-right: 5px;
    }
    .form-horizontal .form-group {
        margin-right: 0px; 
        margin-left: 0px; 
        margin-bottom: 0;
    }
    .form-control{
        color: #000000;
    }
    .table-bordered {
        border: 2px solid #ddd;
    }
    .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
        border: 2px solid #ddd;
    }
    .table>thead>tr>th {
        vertical-align: middle;

    }
    .add-more-btn-style {
        padding: 0px !important;
    }
    #myTr th {
        padding: 2rem;
    }

</style>

<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\models\Videos;
use app\components\CustomPagination;
use richardfan\widget\JSRegister;

$this->title = 'Create Voucher';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1 class="page-title"> Create Voucher
    <small>Enter all voucher details to create a voucher</small>
</h1>


<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <!-- /.box-header -->
                    <?php
                    $form = ActiveForm::begin(['id' => 'manual-form',
                                'enableAjaxValidation' => FALSE,
                                'validationUrl' => 'validate-voucher',
                                'options' => ['class' => 'form-horizontal', 'method' => 'post']])
                    ?>
                    <?= $form->errorSummary($modelVoucher); ?>
                    <table class="table table-bordered table-striped table-condensed" id="myTable">
                        <thead>
                            <tr>
                                <th colspan="15" class="text-center table-header table-scrollable">Voucher Details</th>
                            </tr>
                            <tr id="myTr">
                                <th>Customer Name</th>
                                <th>Destination</th>
                                <th class="numeric">Adult</th>
                                <th class="numeric">Child</th>
                                <th class="numeric">Infant</th>
                                <th>Trip start</th>
                                <th class="numeric">Trip End</th>
                            </tr>
                        <tbody>
                            <tr>
                                <td>
                                    <?= $form->field($modelVoucher, 'billing_name')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>

                                </td>
                                <td>
                                    <?= $form->field($modelVoucher, 'destination')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                                </td>
                                <td>
                                    <?= $form->field($modelVoucher, 'adult_count')->textInput(['type' => 'number', 'maxlength' => true, 'class' => 'form-control'])->label(false) ?>

                                </td>
                                <td>
                                    <?= $form->field($modelVoucher, 'child_count')->textInput(['type' => 'number', 'maxlength' => true, 'class' => 'form-control'])->label(false) ?>

                                </td>
                                <td>
                                    <?= $form->field($modelVoucher, 'infant_count')->textInput(['type' => 'number', 'maxlength' => true, 'class' => 'form-control'])->label(false) ?>

                                </td>
                                <td>
                                    <?= $form->field($modelVoucher, 'trip_start')->textInput(['maxlength' => true, 'class' => 'datepicker form-control'])->label(false) ?>

                                </td>
                                <td>
                                    <?= $form->field($modelVoucher, 'trip_end')->textInput(['maxlength' => true, 'class' => 'datepicker form-control'])->label(false) ?>

                                </td>
                            </tr>
                        </tbody>
                        </thead>
                    </table>

                    <table class="table table-bordered table-striped table-condensed" id="ps_flight_panel">
                        <thead>
                            <tr>
                                <th colspan="15" class="text-center table-header table-scrollable">Flight Details</th>
                            </tr>
                            <tr>
                                <th>Flight Date</th>
                                <th>Flight ID</th>
                                <th width="20%">Visa Type Timing {VOA->Visa On Arrival, PV->Prestamped Visa}</th>
                                <th class="numeric">Source</th>
                                <th>To</th>
                                <th class="numeric">Departure Time</th>
                                <th class="numeric">Arrival Time</th>
                                <th class="numeric">
                                    <div class="text-center add-more-btn-style">
                                        <button type="button" class="add-item add-flight btn btn-primary" id="ps_add_flight"><i class="glyphicon glyphicon-plus"></i>Add More</button>
                                    </div>

                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="flights-0-flight_date" class="datepicker form-control" name="Flights[0][flight_date]">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="flights-0-flight_id" class="form-control" name="Flights[0][flight_id]" maxlength="50">
                                    </div>
                                </td>
                                <td width="20%">
                                    <div class="form-group">
                                        <input type="radio" name="Flights[0][visa_on_arrival]" value="0"> VOA
                                        <input type="radio" name="Flights[0][visa_on_arrival]" value="1" checked="checked"> PV
                                        <input type="radio" name="Flights[0][visa_on_arrival]" value="2"> Both
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="flights-0-source" class="form-control" name="Flights[0][source]">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="flights-0-destination" class="form-control" name="Flights[0][destination]">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="flights-0-departure_time" class="form-control" name="Flights[0][departure_time]" maxlength="20">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="flights-0-arrival_time" class="form-control" name="Flights[0][arrival_time]">
                                    </div>
                                </td>
                                <td>
                                    <div class="text-center full-width-btn">
                                        <button type="button" class="remove-flight btn btn-danger"><i class="glyphicon glyphicon-minus"></i>Remove</button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right text-center add-more-btn-style">
                        <button type="button" class="create-transfers btn btn-primary" onchange="create_transfer()">create related transfers</button>
                    </div>
                    <div class="clearfix">

                    </div>
                    <table class="table table-bordered table-striped table-condensed" id="ps_transfer_panel">
                        <thead>
                            <tr>
                                <th colspan="15" class="text-center table-header table-scrollable">Transfer Details</th>
                            </tr>
                            <tr>
                                <th>Transfer Date</th>
                                <th>Transfer Time</th>
                                <th class="numeric">Transfer Type</th>
                                <th class="numeric">Pickup</th>
                                <th class="numeric">Drop-Off</th>
                                <th>Operated By</th>
                                <th class="numeric">
                                    <div class="text-center add-more-btn-style">
                                        <button type="button" class="add-item add-transfer btn btn-primary" id="ps_add_transfer"><i class="glyphicon glyphicon-plus"></i>Add More</button>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="transfers-0-transfer_date" class="datepicker form-control" name="Transfers[0][transfer_date]">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="transfers-0-transfer_time" class="form-control" name="Transfers[0][transfer_time]">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="transfers-0-transfer_objective" class="form-control" name="Transfers[0][transfer_objective]">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="transfers-0-transfer_source" class="form-control" name="Transfers[0][transfer_source]">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="transfers-0-transfer_destination" class="form-control" name="Transfers[0][transfer_destination]">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="transfers-0-transfer_type" class="form-control" name="Transfers[0][transfer_type]">
<!--                                        <select  id="transfers-0-transfer_type" class="form-control" name="Transfers[0][transfer_type]" >
                                             <option value=""></option>
                                            <option value="PVT_CAR">PVT CAR</option>
                                             <option value="SIC_FERRY">SIC FERRY</option>
                                              <option value="SIC_VAN">SIC VAN</option>
                                        </select>-->
                                    </div>
                                </td>
                                <td>
                                    <div class="text-center full-width-btn">
                                        <button type="button" class="remove-transfer btn btn-danger"><i class="glyphicon glyphicon-minus"></i>Remove</button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-striped table-condensed" id="ps_activity_panel">
                        <thead>
                            <tr>
                                <th colspan="17" class="text-center table-header table-scrollable">Activity Details</th>
                            </tr>
                            <tr>
                                <th>Tour Date</th>
                                <th>Pickup Time</th>
                                <th class="numeric">Destination</th>
                                <th class="numeric">Tour Name</th>
                                <th class="numeric">Meeting Point</th>
                                <th>Tour Type</th>
                                <th>Enroute Location</th>
                                <th>Commission(Thai Baht)</th>
                                <th>Cash on Hand(Thai Baht)</th>
                                
                                
                                <th class="numeric">
                                    <div class="text-center add-more-btn-style">
                                        <button type="button" class="add-item add-activity btn btn-primary" id="ps_add_activity"><i class="glyphicon glyphicon-plus"></i>Add More</button>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="activity-0-tour_date" class="datepicker form-control" name="Activity[0][tour_date]">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="activity-0-pickup_time" class="form-control" name="Activity[0][pickup_time]">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="activity-0-destination" class="form-control" name="Activity[0][destination]">

                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">

                                        <select  id="activity-0-tour_name" class="form-control" name="Activity[0][tour_name]" >

                                            <?php foreach ($model as $index => $value): ?>

                                                <option value="<?= $value; ?>"><?php echo $value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="activity-0-meeting_point" class="form-control" name="Activity[0][meeting_point]">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="activity-0-tour_type" class="form-control" name="Activity[0][tour_type]">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="activity-0-enroute_location" class="form-control" name="Activity[0][enroute_location]">
                                    </div>
                                </td>
                                 <td>
                                    <div class="form-group">
                                        <input type="text" id="activity-0-commission" class="form-control" name="Activity[0][commission]">
                                    </div>
                                </td>
                                 <td>
                                    <div class="form-group">
                                        <input type="text" id="activity-0-cash_on_hand" class="form-control" name="Activity[0][cash_on_hand]">
                                    </div>
                                </td>
                                <td>
                                    <div class="text-center full-width-btn">
                                        <button type="button" class="remove-activity btn btn-danger"><i class="glyphicon glyphicon-minus"></i>Remove</button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- .box-body -->
                <div class="pull-right">
                    <?= Html::button($modelVoucher->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'id' => 'ps_manual_form_submit']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

        </div>
</div>
</section>
</div>
<?php JSRegister::begin(); ?>
<script>

    $(document).ready(function () {
        $(".datepicker").datepicker({//re attach
            format: "dd/mm/yyyy"
        });
        var flight_counter = 1;
        var transfer_counter = 1;
        var activity_counter = 1;
        $(".add-item").on('click', function () {
            if ($(this).hasClass('add-flight')) {
                var counter = flight_counter;
                var type = 'ps_flight_panel';
            } else if ($(this).hasClass('add-transfer')) {
                var counter = transfer_counter;
                var type = 'ps_transfer_panel';
            } else if ($(this).hasClass('add-activity')) {
                var counter = activity_counter;
                var type = 'ps_activity_panel';
            }
            var event = this;
            $.ajax({
                type: "POST",
                evalScripts: true,
                url: '<?= Url::to('ajax-flight-add'); ?>',
                data: ({_csrf: yii.getCsrfToken(), counter: counter, type: type, referrer: 'create'}),
                success: function (data) {
                    $("#" + type).find('tbody:last').append('<tr>').find('tr:last').append(data);

                    $(".datepicker").datepicker({//re attach
                        format: "dd/mm/yyyy"
                    });
                    if ($(event).hasClass('add-flight')) {
                        flight_counter++;
                    } else if ($(event).hasClass('add-transfer')) {
                        transfer_counter++;
                    } else if ($(event).hasClass('add-activity')) {
                        activity_counter++;
                    }
                }
            });
        });
        $("#ps_flight_panel, #ps_transfer_panel, #ps_activity_panel").on("click", ".btn.btn-danger", function (event) {
            $(this).closest("tr").remove();
        });

        $('#ps_manual_form_submit').on('click', function (event) {
            var form = $("#manual-form");
            var url = form.attr('action');
            var type = form.attr('method');
            var data = form.serialize();
            $.ajax({
                url: '<?= Url::to('validate-voucher'); ?>',
                type: type,
                data: data,
                success: function (result) {

                    if (result !== true) {
                        $(".error-summary").text(result);
                        $(".error-summary").addClass("error");
                        $(".error-summary").addClass("text-danger");
                        $(".error-summary").show();
                    } else {
                        form.submit();
                    }
                },
                error: function () {
                    alert('Error');
                }
            });
        });

        $(".create-transfers").on('click', function () {
            var TableData = [];
            $('#ps_flight_panel tr:gt(1)').each(function (row, tr) {
                var arr = '';
                arr = $.map($(tr).find('td'), function (el, i) {
                    if (i == 2) {
                        return [$(el).find("input[type='radio']:checked").val()];
                    } else if (i == 7) {

                    } else {
                        return [$(el).find('input').val()];
                    }
                });

//                arr[2]/=$(tr).find("input[type='radio']:checked").val();
                TableData.push(arr);
            });
            console.log(TableData);
            $.ajax({
                type: "POST",
                evalScripts: true,
                url: '<?= Url::to('ajax-transfer-add'); ?>',
                data: ({_csrf: yii.getCsrfToken(), fields: TableData, counter: transfer_counter}),
                success: function (data) {
                    var pre_count_row = $("#ps_transfer_panel").find("tr").length;
                    $("#ps_transfer_panel").find('tbody:last').append(data);
                    $("#ps_transfer_panel").find("tr").length;
                    $(".datepicker").datepicker({//re attach
                        format: "dd/mm/yyyy"
                    });
                    transfer_counter = transfer_counter + ($("#ps_transfer_panel").find("tr").length - pre_count_row);
                },
                error: function () {
                    alert('error occured.');
                }
            });
        });

    });

</script>
<?php JSRegister::end(); ?>
