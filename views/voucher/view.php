<style>
    .modal-full{
        width: 60%;
    }
    label{
        font-weight: 800 !important;
    }
    .form-control{
        border: 1px solid !important;
    }
    .compose-mail input, .compose-mail input:focus {
        border: none;
        padding: 0;
        width: 90%;
        float: left;
    }
    .ck-editor__editable {
        min-height: 165px;
    }
    .compose-mail input, .compose-mail input:focus {
        border:1px solid #e2e2e4;
    }
    .compose-mail .form-group.flex-inline  label {
        width: auto !important;
        margin-right: 30px;

    }
    .compose-mail .form-group label {
        width:100% !important;


    }
    .flex-inline{
        display: flex !important;
        align-items: center;
    }
    .flex-inline:first-child{
        margin-right: 30px;
    }
    .flexform-group.flex-inline label, .flexform-group.flex-inline input{
        display: inline-block;
    }
    .inline-checkbox {
        width: auto !important;
        display: inline-block;
    }
</style>

<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\BaseUrl;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\models\Videos;
use app\components\CustomPagination;
use richardfan\widget\JSRegister;

$this->title = 'Voucher Details';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile("@web/assets/admin/js/datetimepicker-master/jquery.datetimepicker.min.css");
$this->registerJsFile('@web/assets/admin/js/datetimepicker-master/jquery.datetimepicker.full.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js');
?>
<div class="row">
    <div class="col-md-12">
        <h2 class="section-heading text-center">
            Destination: <?= trim($model->destination); ?>
        </h2>
    </div>
    <div class="col-md-12">
        <div class="head-text-container">
            <span class="head-text">
                Guests Names : <?= $model->billing_name; ?>
            </span>
            <span class="head-text">
                Trip Start:  <?= date('d/m/Y', strtotime($model->trip_start)); ?>
            </span>

        </div>
    </div>
    <?php if (Yii::$app->session->hasFlash('mail_sent')) { ?>
        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"></span>
            </button> <?php echo Yii::$app->session->getFlash('mail_sent'); ?></div>
    <?php } ?>
    <?php if (Yii::$app->session->hasFlash('mail_not_sent')) { ?>
        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"></span>
            </button> <?php echo Yii::$app->session->getFlash('mail_not_sent'); ?></div>
    <?php } ?>
    <div class="col-md-12">
        <div class="head-text-container">
            <div class="sec-head left-sec-head">
                <span class="head-text">
                    Adults : <?= $model->adult_count; ?>
                </span>
                <span class="head-text">
                    Child  : <?= $model->child_count; ?>
                </span>
                <span class="head-text">
                    Infant  : <?= $model->infant_count; ?>
                </span>
                <span class="head-text booking-id">
                    Booking ID  : #<?= $model->id; ?>
                </span>
                <span class="head-text booking-id">
                    Payment Done  : <?= $model->is_payment_done == 0 ? 'No' : 'Yes'; ?>
                </span>
            </div>
            <div class="sec-head right-sec-head">
                <span class="head-text">
                    Trip End:  <?= date('d/m/Y', strtotime($model->trip_end)); ?>
                </span>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <section class="">
            <div class="panel-body">
                <section id="unseen">
                    <div class="table-responsive" style="width: 100%">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr >
                                    <th colspan="7" class="text-center table-header">FLIGHT ITINERARY</th>
                                </tr>
                                <tr>
                                    <th>Date</th>
                                    <th>FLIGHT</th>
                                    <th class="numeric">From</th>
                                    <th class="numeric">To</th>
                                    <th class="numeric">Departure</th>
                                    <th class="numeric">Arrival</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($model->flights)):
                                    foreach ($model->flights as $index => $value):
                                        ?>
                                        <tr>
                                            <td><?= date('d/m/Y', strtotime($value->flight_date)); ?></td>
                                            <td><?= $value->flight_id; ?></td>
                                            <td class="numeric"><?= $value->source; ?></td>
                                            <td class="numeric"><?= $value->destination; ?></td>
                                            <td class="numeric"><?= empty($value->departure_time) ? 'NA' : $value->departure_time; ?></td>
                                            <td class="numeric"><?= empty($value->arrival_time) ? 'NA' : $value->arrival_time; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </tbody>
                        </table>

                    </div>
                </section>
            </div>
        </section>


    </div>

    <div class="col-sd-12">
        <section class="">
            <div class="panel-body">
                <section id="unseen">
                    <div class="table-responsive" style="width: 100%">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr >
                                    <th colspan="11" class="text-center table-header">TRANSFERS</th>
                                </tr>
                                <tr>
                                    <th><input type="checkbox" id="checkAll"></th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Visa Type</th>

                                    <th class="numeric">Type</th>
                                    <th class="numeric">Location - Pickup</th>
                                    <th class="numeric">Drop Off</th>
                                    <th class="numeric">Transfer Type</th>
                                    <th>Flight Id</th>
                                    <th>Service Status</th>
                                    <th>Mail Sent</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($model->transfers)):
                                    foreach ($model->transfers as $index => $value):
                                        ?>
                                        <tr>
                                            <td><input class="ps-mail-select" type="checkbox" value="<?= $value->id; ?>"/></td>
                                            <td><?= date('d/m/Y', strtotime($value->transfer_date)); ?></td>
                                            <td><?= empty($value->transfer_time) ? 'NA' : $value->transfer_time; ?></td>
                                            <td> 
                                                <?php
                                                if (isset($value->flight)) {
                                                    if ($index == 0) {
                                                        if ($value->flight->visa_on_arrival == "0") {
                                                            echo 'Prestamped visa';
                                                        } else if ($value->flight->visa_on_arrival == "1") {
                                                            echo 'Visa On arrival';
                                                        } else {
                                                            if ($value->flight->arrival_time == $value->transfer_time) {

                                                                echo 'Prestamped visa';
                                                            } else {
                                                                echo 'Visa On arrival';
                                                            }
                                                        }
                                                    } else if ($index == 1) {
                                                        if ($value->flight->visa_on_arrival == "2" ) {
                                                            if ($value->flight->arrival_time == $value->transfer_time) {
                                                                echo 'Prestamped visa';
                                                            } else {
                                                                echo 'Visa On arrival';
                                                            }
                                                        } else {
                                                            echo 'NA';
                                                        }
                                                    } else {

                                                        echo 'NA';
                                                    }
                                                } else {

                                                    echo 'NA';
                                                }
                                                ?>
                                            </td>
                                            <td class="numeric"><?= $value->transfer_objective; ?></td>
                                            <td class="numeric"><?= $value->transfer_source; ?></td>
                                            <td class="numeric"><?= $value->transfer_destination; ?></td>
                                            <td class="numeric"><?= $value->transfer_type; ?></td>  
                                            <td><?= $value->flight->flight_id ?? "NA" ?></td>
                                            <td><?php
                                           $date=date("Y-m-d");
                                           $data=($value->transfer_date);
                                           $d_start = new DateTime($date);
                                            $d_end  = new DateTime($data); 
                                            $diff = $d_start->diff($d_end); 
                                          
                                            
                                                if ($value->service_status == '0') {
                                                    echo $diff->format("%R%a days") ."Remaining Days";
                                                } elseif($value->service_status == '1') {
                                                    echo "Completed";
                                                }
                                                else{
                                                    echo "No Show"; 
                                                }
                                                ?>
                                            </td>
                                            <td><?php
                                                if ($value->is_mail_sent == '1') {
                                                    echo '<span class="badge bg-green">Sent</span>';
                                                } else {
                                                    echo '<span class="badge bg-red">Not Sent</span>';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <button id="ps_mail_modal" class="btn btn-primary pull-right" disabled="disabled">Mail To Supplier</button>
                    </div>
                </section>
            </div>
        </section>


    </div>

    <div class="col-sm-12">
        <section class="">
            <div class="panel-body">
                <section id="unseen">
                    <div class="table-responsive" style="width: 100%">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr >
                                    <th colspan="12" class="text-center table-header">ACTIVITY &amp; SIGHTSEEINGS</th>
                                </tr>
                                <tr>
                                    <th><input type="checkbox" id="checkAll1"></th>
                                    <th>Tour Date</th>
                                    <th>Pickup Time</th>
                                    <th class="numeric">Destination</th>
                                    <th class="numeric">Tour Name</th>
                                    <th class="numeric">Meeting Point</th>
                                    <th class="numeric">Tour Type</th>
                                    <th>Mail Sent</th>
                                     <th>Service Status</th>
                                      <th>Commission(Thai Baht)</th>
                                     <th>Cash on Hand(Thai Baht)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($model->activity)):
                                    foreach ($model->activity as $index => $value):
                                        ?>
                                        <tr>
                                            <td><input class="ps-mail-select1" type="checkbox" value="<?= $value->id; ?>"/></td>
                                            <td><?= date('d/m/Y', strtotime($value->tour_date)); ?></td>
                                            <td><?= empty($value->pickup_time) ? 'NA' : $value->pickup_time; ?></td>
                                            <td class="numeric"><?= $value->destination; ?></td>
                                            <td class="numeric"><?= $value->tour_name; ?></td>
                                            <td class="numeric"><?= $value->meeting_point; ?></td>
                                            <td class="numeric"><?= $value->tour_type; ?></td>
                                            <td><?php
                                                if ($value->is_mail_sent == '1') {
                                                    echo '<span class="badge bg-green">Sent</span>';
                                                } else {
                                                    echo '<span class="badge bg-red">Not Sent</span>';
                                                }
                                                ?>
                                            </td>
                                             <td><?php
                                           $date=date("Y-m-d");
                                           $data=($value->tour_date);
                                           $d_start = new DateTime($date);
                                            $d_end  = new DateTime($data); 
                                            $diff = $d_start->diff($d_end); 
                                          
                                            
                                                if ($value->service_status == '0') {
                                                    echo $diff->format("%R%a days") ."Remaining Days";
                                                } elseif($value->service_status == '1') {
                                                    echo "Completed";
                                                }
                                                else{
                                                    echo "No Show"; 
                                                }
                                                ?>
                                            </td>
                                             <td class="numeric"><?= $value->commission; ?></td>
                                              <td class="numeric"><?= $value->cash_on_hand; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <button id="ps_mail_modal1" class="btn btn-primary pull-right" disabled="disabled">Mail To Supplier</button>
                    </div>
                </section>
            </div>
        </section>


    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-full">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Compose Transfers Mail to Supplier</h4>
            </div>
            <div class="modal-body">
                <section class="">
                    <div class="panel-body">
                        <div class="compose-mail">
                            <?php $form = ActiveForm::begin(['id' => 'send-email', 'action' => Url::to(['ajax-send-mail', 'id' => $model->id]), 'options' => ['class' => 'form-horizontal', 'method' => 'post']]); ?>
                            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                            <input type="hidden" name="type" id="sc_selected_type" value="transfer"/>
                            <input type="hidden" name="transferselectedid" id="sc_selected_id"  />
                            <div class="form-group flex-inline">
                                <label for="ps_mail_schedule" class="">Send This Mail Later:</label>
                                <input type="checkbox" id="ps_mail_schedule" class="inline-checkbox" name="check_mail_schedule" value="1"/>
                            </div>
                            <div class="form-group">
                                <label for="time" class="">Schedule Time:</label>
                                <input type='text' name="schedule_time" class="form-control" id='datetimepicker'/>
                            </div>
                            <div class="form-group">
                                <label for="to" class="">To:</label>
                                <input type="text" tabindex="1" id="to" class="form-control" name="to" required="required">
                            </div>
                            <div class="form-group">
                                <label for="subject" class="">Subject:</label>
                                <input type="text" tabindex="1" id="subject" class="form-control" name="subject" required="required">
                            </div>

                            <div class="compose-editor">
                                <textarea  rows="89" cols="50" name="body" id="editor"></textarea>
                                <?php
                                echo $form->field($model, 'uploaded_file[]')
                                        ->fileInput(['multiple' => 'multiple']);
                                ?>
                            </div>
                            <div class="compose-btn pull-right">
                                <button type="submit" class="btn btn-primary btn-sm" id="ps_shoot_mail"><i class="fa fa-check"></i> Send</button>
                                <button type="button" class="btn btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                </section>
            </div>
        </div>

    </div>
</div>
<div id="myModal1" class="modal fade" role="dialog">
    <div class="modal-dialog modal-full">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Compose Tour Mail to Supplier</h4>
            </div>
            <div class="modal-body">
                <section class="">
                    <div class="panel-body">
                        <div class="compose-mail">
                            <?php $form = ActiveForm::begin(['id' => 'send-email1', 'action' => Url::to(['ajax-send-mail', 'id' => $model->id]), 'options' => ['class' => 'form-horizontal', 'method' => 'post']]); ?>
                            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                            <input type="hidden" name="type" id="sc_selected_type1" value="tour"/>
                            <input type="hidden" name="transferselectedid" id="sc_selected_id1"  />
                            <div class="form-group flex-inline">
                                <label for="ps_mail_schedule" class="">Send This Mail Later:</label>
                                <input type="checkbox" id="ps_mail_schedule" class="inline-checkbox" name="check_mail_schedule" value="1"/>
                            </div>
                            <div class="form-group">
                                <label for="time" class="">Schedule Time:</label>
                                <input type='text' name="schedule_time" class="form-control" id='datetimepicker1'/>
                            </div>
                            <div class="form-group">
                                <label for="to" class="">To:</label>
                                <input type="text" tabindex="1" id="to" class="form-control" name="to" required="required">
                            </div>
                            <div class="form-group">
                                <label for="subject" class="">Subject:</label>
                                <input type="text" tabindex="1" id="subject" class="form-control" name="subject" required="required">
                            </div>

                            <div class="compose-editor">
                                <textarea  rows="89" cols="50" name="body" id="editor1"></textarea>
                                <?php
                                echo $form->field($model, 'uploaded_file[]')
                                        ->fileInput(['multiple' => 'multiple']);
                                ?>
                            </div>
                            <div class="compose-btn pull-right">
                                <button type="submit" class="btn btn-primary btn-sm" id="ps_shoot_mail1"><i class="fa fa-check"></i> Send</button>
                                <button type="button" class="btn btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>
                            </div>

                            <?php ActiveForm::end(); ?>

                        </div>
                </section>
            </div>
        </div>

    </div>
</div>
<?php JSRegister::begin(); ?>
<script>
    $(document).ready(function () {
        var editor = CKEDITOR.replace('editor');
        CKEDITOR.instances['editor'].config.allowedContent = true;
        var editor1 = CKEDITOR.replace('editor1');
        CKEDITOR.instances['editor'].config.allowedContent = true;
        $(".ps-mail-select").on('change', function () {
            checkbutton();
        });
        $(".ps-mail-select1").on('change', function () {
            checkbutton1();
        });
        $("#checkAll").click(function () {
            $('input.ps-mail-select:checkbox').not(this).prop('checked', this.checked);
            checkbutton();
        });
        $("#checkAll1").click(function () {
            $('input.ps-mail-select1:checkbox').not(this).prop('checked', this.checked);
            checkbutton1();
        });
        function checkbutton() {
            if ($(".ps-mail-select:checkbox:checked").length > 0) {
                $('#ps_mail_modal').attr('disabled', false);
            } else {
                $('#ps_mail_modal').attr('disabled', true);
            }
        }
        function checkbutton1() {
            if ($(".ps-mail-select1:checkbox:checked").length > 0) {
                $('#ps_mail_modal1').attr('disabled', false);
            } else {
                $('#ps_mail_modal1').attr('disabled', true);
            }
        }

        $('#ps_mail_modal').on('click', function () {
            var ids = $(".ps-mail-select:checkbox:checked").map(function () {
                return $(this).val();
            }).get(); // <----
            $.ajax({
                type: "POST",
                evalScripts: true,
                url: '<?= Url::to(Baseurl::base() . '/voucher/ajax-create-mail-template-to-supplier'); ?>',
                data: ({_csrf: yii.getCsrfToken(), ids: ids}),
                success: function (data) {
                    var jsonObj = JSON.parse(data);
                    $("#subject").val(jsonObj.subject);
                    console.log(jsonObj.body);
                    $("#sc_selected_id").val(ids);
                    console.log(jsonObj.body);
                    CKEDITOR.instances['editor'].setData(jsonObj.body);
//                    ClassicEditor.instances.editor.insertText('some text here');
                    $("#myModal").modal('show');

                }
            });

        });
        $('#ps_mail_modal1').on('click', function () {
            var ids = $(".ps-mail-select1:checkbox:checked").map(function () {
                return $(this).val();
            }).get(); // <----
            $.ajax({
                type: "POST",
                evalScripts: true,
                url: '<?= Url::to(Baseurl::base() . '/voucher/ajax-create-mail-template-to-supplier-tour'); ?>',
                data: ({_csrf: yii.getCsrfToken(), ids: ids}),
                success: function (data) {
                    var jsonObj = JSON.parse(data);
                    $("#subject").val(jsonObj.subject);
                    $("#sc_selected_id1").val(ids);
                    console.log(jsonObj.body);
                    CKEDITOR.instances['editor1'].setData(jsonObj.body);
//                    ClassicEditor.instances.editor.insertText('some text here');
                    $("#myModal1").modal('show');
                }
            });

        });
        $(function () {
            $('#datetimepicker').datetimepicker();
        });
        $(function () {
            $('#datetimepicker1').datetimepicker();
        });
    });
</script>
<?php JSRegister::end(); ?>

