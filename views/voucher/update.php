<style>

    .full-width-btn {
        flex-basis: 100%;
    }

    .add-more-btn-style {
        padding: 1rem 0;
    }
    button i {
        font-size: 12px;
        line-height: 1.4;
        padding-right: 5px;
    }
    .form-horizontal .form-group {
        margin-right: 0px; 
        margin-left: 0px; 
        margin-bottom: 0;
    }
    .form-control{
        color: #000000;
    }
    .table-bordered {
        border: 2px solid #ddd;
    }
    .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
        border: 2px solid #ddd;
    }
    .table>thead>tr>th {
        vertical-align: middle;

    }
    .add-more-btn-style {
        padding: 0px !important;
    }
    #myTr th {
        padding: 2rem;
    }

</style>

<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\BaseUrl;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\components\CustomPagination;
use richardfan\widget\JSRegister;

$this->title = 'Update Voucher';
$this->params['breadcrumbs'][] = $this->title;
$flight_count = count($model->flights);
$transfer_count = count($model->transfers);
$activity_count = count($model->activity);
?>

<h1 class="page-title"> Update Voucher
    <small>Edit all changes to update a voucher</small>
</h1>


<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <!-- /.box-header -->
                    <?php
                    $form = ActiveForm::begin(['id' => 'manual-edit-form',
                                'enableAjaxValidation' => FALSE,
                                'validationUrl' => 'validate-voucher',
                                'options' => ['class' => 'form-horizontal', 'method' => 'post']])
                    ?>
                    <?= $form->errorSummary($model); ?>
                    <table class="table table-bordered table-striped table-condensed" id="myTable">
                        <thead>
                            <tr>
                                <th colspan="15" class="text-center table-header table-scrollable">Voucher Details</th>
                            </tr>
                            <tr id="myTr">
                                <th>Customer Name</th>
                                <th>Destination</th>
                                <th class="numeric">Adult</th>
                                <th class="numeric">Child</th>
                                <th class="numeric">Infant</th>
                                <th>Trip start</th>
                                <th class="numeric">Trip End</th>                                
                                <th class="numeric">Payment Done</th>
                            </tr>
                        <tbody>
                            <tr>
                                <td>
                                    <?= $form->field($model, 'billing_name')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>

                                </td>
                                <td>
                                    <?= $form->field($model, 'destination')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                                </td>
                                <td>
                                    <?= $form->field($model, 'adult_count')->textInput(['type' => 'number', 'maxlength' => true, 'class' => 'form-control'])->label(false) ?>

                                </td>
                                <td>
                                    <?= $form->field($model, 'child_count')->textInput(['type' => 'number', 'maxlength' => true, 'class' => 'form-control'])->label(false) ?>

                                </td>
                                <td>
                                    <?= $form->field($model, 'infant_count')->textInput(['type' => 'number', 'maxlength' => true, 'class' => 'form-control'])->label(false) ?>

                                </td>
                                <td>
                                    <?= $form->field($model, 'trip_start')->textInput(['maxlength' => true, 'class' => 'datepicker form-control'])->label(false) ?>

                                </td>
                                <td>
                                    <?= $form->field($model, 'trip_end')->textInput(['maxlength' => true, 'class' => 'datepicker form-control'])->label(false) ?>

                                </td>
                                <td>
                                    <?= $form->field($model, 'is_payment_done')->checkbox(['class' => 'form-control', 'data-toggle' => 'toggle', 'data-on' => 'Yes', 'data-off' => 'No', 'label' => null]); ?>
                                </td>
                            </tr>
                        </tbody>
                        </thead>
                    </table>

                    <table class="table table-bordered table-striped table-condensed" id="ps_flight_panel">
                        <thead>
                            <tr>
                                <th colspan="15" class="text-center table-header table-scrollable">Flight Details</th>
                            </tr>
                            <tr>
                                <th>Flight ID</th>
                                <th>Flight Date</th>
                                <th class="numeric">Departure Time</th>
                                <th class="numeric">Arrival Time</th>
                                <th class="numeric">Source</th>
                                <th>To</th>
                                <th class="numeric">
                                    <div class="text-center add-more-btn-style">
                                        <button type="button" class="add-item add-flight btn btn-primary" id="ps_add_flight"><i class="glyphicon glyphicon-plus"></i>Add More</button>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (count($model->flights) > 0):
                                foreach ($model->flights as $index => $flight_model):
                                    ?>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="flights-<?= $index; ?>-flight_id" class="form-control" name="Flights[<?= $index; ?>][flight_id]" maxlength="50" value="<?= $flight_model->flight_id; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="flights-<?= $index; ?>-flight_date" class="datepicker form-control" name="Flights[<?= $index; ?>][flight_date]" value="<?= $flight_model->flight_date; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="flights-<?= $index; ?>-departure_time" class="form-control" name="Flights[<?= $index; ?>][departure_time]" maxlength="20" value="<?= $flight_model->departure_time; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="flights-<?= $index; ?>-arrival_time" class="form-control" name="Flights[<?= $index; ?>][arrival_time]" value="<?= $flight_model->arrival_time; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="flights-<?= $index; ?>-source" class="form-control" name="Flights[<?= $index; ?>][source]" value="<?= $flight_model->source; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="flights-<?= $index; ?>-destination" class="form-control" name="Flights[<?= $index; ?>][destination]" value="<?= $flight_model->destination; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="text-center full-width-btn">
                                                <button type="button" class="remove-flight btn btn-danger"><i class="glyphicon glyphicon-minus"></i>Remove</button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-striped table-condensed" id="ps_transfer_panel">
                        <thead>
                            <tr>
                                <th colspan="15" class="text-center table-header table-scrollable">Transfer Details</th>
                            </tr>
                            <tr>
                                <th>Transfer Date</th>
                                <th>Transfer Time</th>
                                <th class="numeric">Transfer Type</th>
                                <th class="numeric">Pickup</th>
                                <th class="numeric">Drop-Off</th>
                                <th>Operated By</th>
                                <th>Service Status</th>
                                <th class="numeric">
                                    <div class="text-center add-more-btn-style">
                                        <button type="button" class="add-item add-transfer btn btn-primary" id="ps_add_transfer"><i class="glyphicon glyphicon-plus"></i>Add More</button>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (count($model->transfers) > 0):
                                foreach ($model->transfers as $index => $transfer_model):
                                    ?>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="transfers-<?= $index; ?>-transfer_date" class="datepicker form-control" name="Transfers[<?= $index; ?>][transfer_date]" value="<?= $transfer_model->transfer_date; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="transfers-<?= $index; ?>-transfer_time" class="form-control" name="Transfers[<?= $index; ?>][transfer_time]" value="<?= $transfer_model->transfer_time; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="transfers-<?= $index; ?>-transfer_objective" class="form-control" name="Transfers[<?= $index; ?>][transfer_objective]" value="<?= $transfer_model->transfer_objective; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="transfers-<?= $index; ?>-transfer_source" class="form-control" name="Transfers[<?= $index; ?>][transfer_source]" value="<?= $transfer_model->transfer_source; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="transfers-<?= $index; ?>-transfer_destination" class="form-control" name="Transfers[<?= $index; ?>][transfer_destination]" value="<?= $transfer_model->transfer_destination; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                        <input type="text" id="transfers--transfer_type" class="form-control" name="Transfers[<?= $index; ?>][transfer_type]" value="<?= $transfer_model->transfer_type; ?>">

<!--                                                <select  id="transfers-<?= $index; ?>-transfer_type" class="form-control" name="Transfers[<?= $index; ?>][transfer_type]" >


                                                    <option value=""></option>
                                                    <option value="PVT_CAR"<?php if ($transfer_model->transfer_type == "PVT_CAR") echo "selected"; ?>>PVT CAR</option>
                                                    <option value="SIC_FERRY"<?php if ($transfer_model->transfer_type == "SIC_FERRY") echo "selected"; ?>>SIC FERRY</option>
                                                    <option value="SIC_VAN"<?php if ($transfer_model->transfer_type == "SIC_VAN") echo "selected"; ?>>SIC VAN</option>
                                                </select>
                                            </div>-->
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select id="transfers-<?= $index; ?>-service_status" class="form-control" name="Transfers[<?= $index; ?>][service_status]">
                                                    <option value=""></option>
                                                    <option value="0"<?php if ($transfer_model->service_status == "0") echo "selected" ?>>Pending</option>
                                                    <option value="1"<?php if ($transfer_model->service_status == "1") echo "selected" ?>> completed</options>
                                                    <option value="2"<?php if ($transfer_model->service_status == "2") echo "selected" ?>>No Show</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="text-center full-width-btn">
                                                <button type="button" class="remove-transfer btn btn-danger"><i class="glyphicon glyphicon-minus"></i>Remove</button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-striped table-condensed" id="ps_activity_panel">
                        <thead>
                            <tr>
                                <th colspan="15" class="text-center table-header table-scrollable">Activity Details</th>
                            </tr>
                            <tr>
                                <th>Tour Date</th>
                                <th>Pickup Time</th>
                                <th class="numeric">Destination</th>
                                <th class="numeric">Tour Name</th>
                                <th class="numeric">Meeting Point</th>
                                <th>Tour Type</th>
                                <th>Enroute Location</th>
                                <th>Service Status</th>
                                <th>Commission(Thai Baht)</th>
                                <th>Cash on Hand(Thai Baht)</th>
                                <th class="numeric">
                                    <div class="text-center add-more-btn-style">
                                        <button type="button" class="add-item add-activity btn btn-primary" id="ps_add_activity"><i class="glyphicon glyphicon-plus"></i>Add More</button>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (count($model->activity) > 0):
                                foreach ($model->activity as $index => $activity_model):
                                    ?>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="activity-<?= $index; ?>-tour_date" class="datepicker form-control" name="Activity[<?= $index; ?>][tour_date]" value="<?= $activity_model->tour_date; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="activity-<?= $index; ?>-pickup_time" class="form-control" name="Activity[<?= $index; ?>][pickup_time]" value="<?= $activity_model->pickup_time; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="activity-<?= $index; ?>-destination" class="form-control" name="Activity[<?= $index; ?>][destination]" value="<?= $activity_model->destination; ?>">

                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
<!--                                                <input type="text" id="activity-<?= $index; ?>-tour_name" class="form-control" name="Activity[<?= $index; ?>][tour_name]" value="<?= $activity_model->tour_name; ?>">-->
                                               <select  id="activity-<?= $index; ?>-tour_name" class="form-control" name="Activity[<?= $index; ?>][tour_name]" >
                                                    
                                                 <?php foreach ($model1 as $index1 => $value): ?>
                                                  
                                                    <option value="<?= $value; ?>"<?=  $activity_model->tour_name== $value?  "selected":'' ?>><?php echo $value ?></option>
                                                <?php endforeach; ?>
                                                    </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="activity-<?= $index; ?>-meeting_point" class="form-control" name="Activity[<?= $index; ?>][meeting_point]" value="<?= $activity_model->meeting_point; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="activity-<?= $index; ?>-tour_type" class="form-control" name="Activity[<?= $index; ?>][tour_type]" value="<?= $activity_model->tour_type; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="activity-<?= $index; ?>-enroute_location" class="form-control" name="Activity[<?= $index; ?>][enroute_location]" value="<?= $activity_model->enroute_location; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select id="activity-0-service_status" class="form-control" name="Activity[0][service_status]">
                                                    <option value=""></option>
                                                    <option value="0"<?php if ($activity_model->service_status == "0") echo "selected" ?>>Pending</option>
                                                    <option value="1"<?php if ($activity_model->service_status == "1") echo "selected" ?>> completed</options>
                                                    <option value="2"<?php if ($activity_model->service_status == "2") echo "selected" ?>>No Show</option>
                                                </select>
                                            </div>
                                        </td>
                                         <td>
                                            <div class="form-group">
                                                <input type="text" id="activity-<?= $index; ?>-commission" class="form-control" name="Activity[<?= $index; ?>][commission]" value="<?= $activity_model->commission; ?>">
                                            </div>
                                        </td>
                                         <td>
                                            <div class="form-group">
                                                <input type="text" id="activity-<?= $index; ?>-cash_on_hand" class="form-control" name="Activity[<?= $index; ?>][cash_on_hand]" value="<?= $activity_model->cash_on_hand; ?>">
                                            </div>
                                        </td>
                                        
                                        <td>
                                            <div class="text-center full-width-btn">
                                                <button type="button" class="remove-activity btn btn-danger"><i class="glyphicon glyphicon-minus"></i>Remove</button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <!-- .box-body -->
                <div class="pull-right">
                    <?= Html::button($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'id' => 'ps_manual_form_submit']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

        </div>
</div>
</section>
</div>
<?php JSRegister::begin(); ?>
<script>

    $(document).ready(function () {
        $(".datepicker").datepicker({//re attach
            format: "dd/mm/yyyy"
        });
        var flight_counter = <?= $flight_count; ?>;
        var transfer_counter = <?= $transfer_count; ?>;
        var activity_counter = <?= $activity_count; ?>;
        $(".add-item").on('click', function () {
            if ($(this).hasClass('add-flight')) {
                var counter = flight_counter;
                var type = 'ps_flight_panel';
            } else if ($(this).hasClass('add-transfer')) {
                var counter = transfer_counter;
                var type = 'ps_transfer_panel';
            } else if ($(this).hasClass('add-activity')) {
                var counter = activity_counter;
                var type = 'ps_activity_panel';
            }
            var event = this;
            $.ajax({
                type: "POST",
                evalScripts: true,
                url: '<?= Url::to(Baseurl::base() . '/voucher/ajax-flight-add'); ?>',
                data: ({_csrf: yii.getCsrfToken(), counter: counter, type: type, referrer:'edit'}),
                success: function (data) {
//                    console.log(data);
//                    console.log($(event));
                    $("#" + type).find('tbody:last').append('<tr>').find('tr:last').append(data);
//                    $("#" + type).append(data);

                    $(".datepicker").datepicker({//re attach
                        format: "dd/mm/yyyy"
                    });
                    if ($(event).hasClass('add-flight')) {
                        flight_counter++;
                    } else if ($(event).hasClass('add-transfer')) {
                        transfer_counter++;
                    } else if ($(event).hasClass('add-activity')) {
                        activity_counter++;
                    }
                }
            });
        });
        $("#ps_flight_panel, #ps_transfer_panel, #ps_activity_panel").on("click", ".btn.btn-danger", function (event) {
            $(this).closest("tr").remove();
        });
    });
    $('#ps_manual_form_submit').on('click', function (event) {
        var form = $("#manual-edit-form");
        var url = form.attr('action');
        var type = form.attr('method');
        var data = form.serialize();
        $.ajax({
            url: '<?= Url::to(Baseurl::base() . '/voucher/validate-voucher'); ?>',
            type: type,
            data: data,
            success: function (result) {

                if (result !== true) {
                    $(".error-summary").text(result);
                    $(".error-summary").addClass("error");
                    $(".error-summary").addClass("text-danger");
                    $(".error-summary").show();
                } else {
                    form.submit();
                }
            },
            error: function () {
                alert('Error');
            }
        });
    });

</script>
<?php JSRegister::end(); ?>
