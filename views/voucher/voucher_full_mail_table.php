
<div class="email-background" style="background: linear-gradient(to right, #ffd3d3f2, #4ad5ffdb),url(http://team.erawandmc.com/web/assets/admin/images/bunting-flag.png);padding: 13%;width: 100%;margin: 0 auto;box-sizing: border-box;font-family: sans-serif;">
    <div class="e-head" style="background: #ffffffad;padding: 5px;border-top-right-radius: 5px;border-top-left-radius: 5px;">
        <h1 class="e-head-text" style="color: #2ccaea;font-size: 2.8rem;text-align: center;line-height: 3.5rem;margin: 0;padding: 5%;background: #ffffff9e;border-top-right-radius: 5px;border-top-left-radius: 5px;">Erawan International Corporation Co. Ltd.</h1>
    </div>
    <div class="e-content" style="background: #ffffffd6;">
        <div class="banner"> <img alt="" src="http://team.erawandmc.com/web/assets/admin/images/banner.jpg" style="max-width: 100%;overflow: hidden;">
        </div>
        <div class="details" style="padding-bottom: 30px;">
            <div class="row">
                <div class="content" style="padding: 5px 30px;">
                    <div class="contact-details" style="text-align: right;width: 70%;float: right;">
                        <h5 style="padding: 5px 0;margin: 0;font-size: 12px;color: lightslategrey;line-height: 18px;letter-spacing: 0.8px;">MR. SHAWN +66637465477 </h5>
                        <h5 style="padding: 5px 0;margin: 0;font-size: 12px;color: lightslategrey;line-height: 18px;letter-spacing: 0.8px;">THAILAND Helpline : Representative, Ms. (MOMO) +66-917724692</h5>
                    </div>
                </div>
            </div>

            <div class="row"  style="clear: both";>
                <div class="content" style="padding: 5px 30px;">
                    <div class="confirm-container" style="background:#fff; padding:5%;"><img src="http://team.erawandmc.com/web/assets/admin/images/confirmation-1.png" alt="" class="confirmation" style="max-width: 100%;overflow: hidden;height: 50px;width: 50px; display:block; margin:0 auto;">
                        <h2 class="congrats" style="text-align: center;text-transform: capitalize;color: #2ccaea;margin: 0;border-radius: 5px;font-size: 2rem;margin-top: 25px;">your booking has been confirmed!</h2>
                    </div>

                    <h5 class="head-h5" style="padding: 15px 0;margin: 0;font-size: 16px;color: #615b5b;line-height: 18px;">Thank You  <span><?= $model->billing_name; ?> </span>, </h5>
                    <p style="color: #615b5b;font-size: 14px;line-height: 18px;margin-top: 0;">Thank you for your booking
                        We are please to confirm this booking. Please see below for your confirmed tours or activities.
                        We are proud to be a part of your travel plans. We wish you have an amazing with us and we appreciate your business. 
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="content" style="padding: 5px 30px;">
                    <table style="background: white;width: 100%;">
                        <caption style="font-size: 25px;line-height: 30px;padding: 15px 0;text-transform: uppercase;font-weight: 600;color: #2ccaea;border-radius: 5px;background: white;">booking details</caption>
                        <tr>
                            <th style="border-bottom: 1px solid #f5f0f0;padding: 10px;font-weight: 400;color: #899ca2;text-align: right;">Booking Number</th>
                            <td style="border-bottom: 1px solid #f5f0f0;padding: 10px;color: #615b5b;"><?= 'REF-' . $model->id; ?></td>
                        </tr>
                        <tr>
                            <th style="border-bottom: 1px solid #f5f0f0;padding: 10px;font-weight: 400;color: #899ca2;text-align: right;">Billing Person(s)</th>
                            <td style="border-bottom: 1px solid #f5f0f0;padding: 10px;color: #615b5b;"><?= $model->billing_name; ?></td>
                        </tr>
                        <tr>
                            <th style="border-bottom: 1px solid #f5f0f0;padding: 10px;font-weight: 400;color: #899ca2;text-align: right;">Check-in</th>
                            <td style="border-bottom: 1px solid #f5f0f0;padding: 10px;color: #615b5b;"><?= date("jS \of F Y", strtotime($model->trip_start)); ?></td>
                        </tr>
                        <tr>
                            <th style="border-bottom: 1px solid #f5f0f0;padding: 10px;font-weight: 400;color: #899ca2;text-align: right;">Check-out</th>
                            <td style="border-bottom: 1px solid #f5f0f0;padding: 10px;color: #615b5b;"><?= date("jS \of F Y", strtotime($model->trip_end)); ?></td>
                        </tr>

                        <tr>
                            <th style="border-bottom: 1px solid #f5f0f0;padding: 10px;font-weight: 400;color: #899ca2;text-align: right;">Destination</th>
                            <td style="border-bottom: 1px solid #f5f0f0;padding: 10px;color: #615b5b;"><?= $model->destination; ?></td>
                        </tr>
                        <tr>
                            <th style="border-bottom: 1px solid #f5f0f0;padding: 10px;font-weight: 400;color: #899ca2;text-align: right;">Activity</th>
                            <td style="border-bottom: 1px solid #f5f0f0;padding: 10px;color: #615b5b;">
                                <?php
                                if (!empty($model->activity)):
                                    $tour = '';
                                    foreach ($model->activity as $index => $value):
                                        $tour = $value->tour_name . ', ';
                                    endforeach;
                                    echo $tour;
                                endif;
                                ?>

                            </td>
                        </tr>
                        <tr>
                            <th style = "border-bottom: 1px solid #f5f0f0;padding: 10px;font-weight: 400;color: #899ca2;text-align: right;">Flights</th>
                            <td style = "border-bottom: 1px solid #f5f0f0;padding: 10px;color: #615b5b;"><?= date("jS \of F Y", strtotime($model->flights[0]->flight_date)); ?></td>
                        </tr>
                    </table>


                </div>
            </div>
            <div class = "row">
                <div class = "content" style = "padding: 5px 30px;">
                    <div class = "cta-button" style = "margin-top: 15px;">
                        <a href = "<?= Yii::$app->request->absoluteUrl . '/auth/view-voucher/REF-' . $model->id; ?>" style = "text-align: center;display: block;padding: 20px;font-size: 20px;text-transform: uppercase;text-decoration: none;font-weight: 600;letter-spacing: 1px;color: white;background: #2ccaea;width: 60%;margin: 0 auto;border-radius: 10px;box-shadow: 0px 2px 16px -4px #3b3b3c;">view full details</a>
                    </div>
                </div>
            </div>

            <div class = "row"">
                <div class="content" style="padding: 5px 30px;
                     ">
                    <div class="privacy">
                        <h4 style="color: #4d9bef;text-decoration: underline;">Privcay &amp; Policy</h4>
                        <p style = "color: #615b5b;font-size: 14px;line-height: 18px;margin-top: 0;">Lorem ipsum dolor sit amet, consectetur. </p>
                        <p style = "color: #615b5b;font-size: 14px;line-height: 18px;margin-top: 0;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum quos fugit atque quas sunt. Omnis!</p>
                    </div>
                    <div class = "cancellation">
                        <h4 style = "color: #4d9bef;text-decoration: underline;">Cancellation</h4>
                        <p style = "color: #615b5b;font-size: 14px;line-height: 18px;margin-top: 0;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam dicta explicabo, libero, deleniti quo minus animi consequatur eos totam cum praesentium, officia autem alias eaque, voluptatibus itaque reprehenderit? Culpa, voluptatibus!</p>
                    </div>
                </div>
            </div>
            <div class = "row">
                <div class = "content" style = "padding: 5px 30px;">
                    <img src = "http://team.erawandmc.com/web/assets/admin/images/logo.jpg" alt = "" style = "max-width: 100%;overflow: hidden;">
                </div>
            </div>

            <div class = "row">
                <div class = "content" style = "padding: 5px 30px;">
                    <div class = "footer-content" style = "text-align: center;">
                        <h5 class = "footer-text" style = "padding: 5px 0;margin: 0;font-size: 12px;color: #615b5b;line-height: 18px;">No 316/1 Wat Tri Thorsathep Lane, Ban Phan Thom</h5>
                        <h5 class = "footer-text" style = "padding: 5px 0;margin: 0;font-size: 12px;color: #615b5b;line-height: 18px;">District, Phra Nkahorn Bangkok 10200</h5>
                        <h5 class = "footer-text" style = "padding: 5px 0;margin: 0;font-size: 12px;color: #615b5b;line-height: 18px;">Hotline: +66637465477</h5>
                        <h5 class = "footer-text" style = "padding: 5px 0;margin: 0;font-size: 12px;color: #615b5b;line-height: 18px;">Email: contact@erawandmc.com</h5>
                    </div>
                    <hr>
                    <div class = "footer-img" style = "">
                        <img src = "http://team.erawandmc.com/web/assets/admin/images/payment-logo.png" alt = "" class = "payment-logo" style = "max-width: 100%;overflow: hidden; display:block; margin:0 auto;">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
