<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Upload Excel';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>
<?php if (isset($exception)): ?>
<?= $exception; ?> 
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('excelFormSubmitted')): ?>

    <div class="alert alert-success"><?= Yii::$app->session->getFlash('excelFormSubmitted'); ?></div>
<?php else: ?>

    <p>
        Upload a correctly formatted voucher.
    </p>

    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin(['id' => 'excel-form', 'options' => ['enctype' => 'multipart/form-data', 'method'=>'post']]) ?>


            <?= $form->field($model, 'uploaded_file')->fileInput() ?>         


            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>


<?php endif; ?>