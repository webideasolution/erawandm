
<style>
    .compose-mail input, .compose-mail input:focus {
        border:1px solid #e2e2e4;
    }
    .compose-mail .form-group.flex-inline  label {
        width: auto !important;
    margin-right: 30px;

    }
     .compose-mail .form-group label {
        width:100% !important;
    

    }
    .flex-inline{
        display: flex !important;
        align-items: center;
    }
    .flex-inline:first-child{
        margin-right: 30px;
    }
    .flexform-group.flex-inline label, .flexform-group.flex-inline input{
        display: inline-block;
    }
    .inline-checkbox {
        width: auto !important;
        display: inline-block;
    }
</style>
<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\BaseUrl;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\components\CustomPagination;
use richardfan\widget\JSRegister;
use app\models\Voucher;

$this->title = 'Voucher List';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile("@web/assets/admin/js/datetimepicker-master/jquery.datetimepicker.min.css");
$this->registerCssFile('http://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css');
$this->registerJsFile('http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('@web/assets/admin/js/datetimepicker-master/jquery.datetimepicker.full.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js');
?>

<h1 class="page-title"> Voucher list
    <small>All uploaded Vouchers are listed here</small>
</h1>


<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h1>Voucher Table</h1>
                        <a class="pull-right btn btn-primary" href="<?php echo Url::to(['voucher/excelupload']); ?>"> Upload Voucher</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <?php if (Yii::$app->session->hasFlash('excelFormSubmitted')) { ?>
                            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"></span>
                                </button> <?php echo Yii::$app->session->getFlash('excelFormSubmitted'); ?></div>
                        <?php } ?>
                        <?php if (Yii::$app->session->hasFlash('mail_sent')) { ?>
                            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"></span>
                                </button> <?php echo Yii::$app->session->getFlash('mail_sent'); ?></div>
                        <?php } ?>
                        <?php if (Yii::$app->session->hasFlash('mail_not_sent')) { ?>
                            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"></span>
                                </button> <?php echo Yii::$app->session->getFlash('mail_not_sent'); ?></div>
                        <?php } ?>
                        <div class="table-responsive" style="width: 100%">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Billing Name</th>
                                        <th>Destination</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Adults</th>
                                        <th>Children</th>
                                        <th>Infants</th>
                                        <th>Payment Done(Yes/No)</th>
                                        <th>Generation Date</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($model) > 0) {
                                        foreach ($model as $val) {
                                            ?>
                                            <tr>
                                                <td><?php echo $val->billing_name; ?></td>
                                                <td><?php echo $val->destination; ?></td>
                                                <td><?php echo date('d/m/Y', strtotime($val->trip_start)); ?></td>
                                                <td><?php echo date('d/m/Y', strtotime($val->trip_end)); ?></td>
                                                <td><?php echo $val->adult_count; ?></td>
                                                <td><?php echo $val->child_count; ?></td>
                                                <td><?php echo $val->infant_count; ?></td>
                                                <td><input class="payment-toggle" type="checkbox" data-toggle="toggle" <?= $val->is_payment_done == '1' ? 'checked' : ''; ?> data-toggle = "toggle" data-on = "Yes" data-off = "No" data-id="<?= $val->id; ?>"></td>
                                                <td><?php echo $val->created_at; ?></td>
                                                <td><?php
                                                    if ($val->status == '1') {
                                                        echo '<span class="badge bg-green">Active</span>';
                                                    } else {
                                                        echo '<span class="badge bg-red">Inactive</span>';
                                                    }
                                                    ?></td>

                                                <td>
                                                    <a href="<?= Yii::$app->request->baseUrl . '/voucher/view/' . $val->id; ?> "><span class="badge bg-yellow"><i class="fa fa-eye"></i> View</span></a>&nbsp;&nbsp;&nbsp;
                                                    <a href="<?= Yii::$app->request->baseUrl . '/voucher/edit/' . $val->id; ?> "><span class="badge bg-blue"><i class="fa fa-pencil"></i> Update</span></a>&nbsp;&nbsp;&nbsp;
                                                    <a target="_blank" href="<?= Yii::$app->request->baseUrl . '/auth/view-voucher/REF-' . $val->id; ?> "><span class="badge bg-blue"><i class="fa fa-download"></i> Download</span></a>&nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:void(0);" data="<?= Url::home(true) . 'auth/view-voucher/REF-' . $val->id; ?>" class="copy-location"  ><span class="badge bg-blue"><i class="fa fa-link"></i> Copy Share Link</span></a>&nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:void(0);" data="<?= Yii::$app->request->absoluteUrl . '/auth/view-voucher/REF-' . $val->id; ?>" class="ps-send-email" data-id="<?= $val->id; ?>"><span class="badge bg-blue"><i class="fa fa-link"></i>Email voucher</span></a>&nbsp;&nbsp;&nbsp;
                                                    <a href="<?= Yii::$app->request->baseUrl . '/voucher/delete/' . $val->id; ?> "><span class="badge bg-red"><i class="fa fa-trash"></i> Delete</span></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- .box-body -->

                </div>

            </div>
        </div>
    </section>
</div>


<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-full">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Compose Voucher Confirmation Mail to Customer</h4>
            </div>
            <div class="modal-body">
                <section class="">
                    <div class="panel-body">
                        <div class="compose-mail">
                            <?php
                            $model1 = new Voucher();
                            $form = ActiveForm::begin(['id' => 'send-email1', 'action' => Url::to(['ajax-send-mail-voucher-customer']), 'options' => ['class' => 'form-horizontal', 'method' => 'post']]);
                            ?>
                            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                            <!--<input type="hidden" name="type" id="sc_selected_type1" value="tour"/>-->
                            <input type="hidden" name="transferselectedid" id="sc_selected_id1"  />
                            <div class="form-group flex-inline">
                                <label for="ps_mail_schedule" class="">Send This Mail Later:</label>
                                <input type="checkbox" id="ps_mail_schedule" class="inline-checkbox" name="check_mail_schedule" value="1"/>
                            </div>
                            <div class="form-group">
                                <label for="time" class="">Schedule Time:</label>
                                <input type='text' name="schedule_time" class="form-control" id='datetimepicker1'/>
                            </div>
                            <div class="form-group">
                                <label for="to" class="">To:</label>
                                <input type="text" tabindex="1" id="to" class="form-control" name="to" required="required">
                            </div>
                            <div class="form-group">
                                <label for="subject" class="">Subject:</label>
                                <input type="text" tabindex="1" id="subject" class="form-control" name="subject" required="required">
                            </div>

                            <div class="compose-editor">
                                <textarea  rows="89" cols="50" name="body" id="editor"></textarea>
                                <?php
                                echo $form->field($model1, 'uploaded_file[]')
                                        ->fileInput(['multiple' => 'multiple']);
                                ?>
                            </div>
                            <div class="compose-btn pull-right">
                                <button type="submit" class="btn btn-primary btn-sm" id="ps_shoot_mail1"><i class="fa fa-check"></i> Send</button>
                                <button type="button" class="btn btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                </section>
            </div>
        </div>

    </div>
</div>
</div> 
<?php JSRegister::begin(); ?>
<script>
    $(document).ready(function () {
        $("#example1").DataTable();
        var editor = CKEDITOR.replace('editor');
        CKEDITOR.instances['editor'].config.allowedContent = true;
        //        $("#myTable1").DataTable();
        $(function () {
            $('#datetimepicker1').datetimepicker();
        });
    });

    $('.payment-toggle').change(function () {
        var status;
        if ($(this).prop('checked') === true) {
            status = 1;
        } else {
            status = 0;
        }
        var model_id = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            evalScripts: true,
            url: '<?= Url::to(Baseurl::base() . '/voucher/ajax-payment-status-change'); ?>',
            data: ({_csrf: $('meta[name="csrf-token"]').attr("content"), status: status, model_id: model_id}),
            success: function (data) {
            },
            error: function (error) {
                alert('error occured, Please reload this page.');

            }
        });
    });
    $(document).ready(function () {
        $('.copy-location').on('click', function () {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(this).attr('data')).select();
            document.execCommand("copy");
            $temp.remove();
            alert('link copied in the clipboard');
        });
        $(".ps-send-email").on("click", function () {
            console.log($(this));
            var data_id= $(this).attr('data-id');
            $.ajax({
                type: "POST",
                evalScripts: true,
                url: '<?= Url::to(Baseurl::base() . '/voucher/ajax-create-mail-template-to-supplier-voucher'); ?>',
                data: ({_csrf: $('meta[name="csrf-token"]').attr("content"), id:data_id}),
                success: function (data) {
//                    console.log(data);
                    var jsonObj = JSON.parse(data);
                    $("#subject").val(jsonObj.subject);
                    $("#sc_selected_id1").val(data_id);
                    console.log(data_id);
                    CKEDITOR.instances['editor'].setData(jsonObj.body);
//                    ClassicEditor.instances.editor.insertText('some text here');
                    $("#myModal").modal('show');
                }
            });
        });
    });

</script>
<?php JSRegister::end(); ?>