<style>
    .modal-full{
        width: 60%;
    }
    label{
        font-weight: 800 !important;
    }
    .form-control{
        border: 1px solid !important;
    }
    .compose-mail input, .compose-mail input:focus {
        border: none;
        padding: 0;
        width: 90%;
        float: left;
    }
    .ck-editor__editable {
        min-height: 165px;
    }

</style>

<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\BaseUrl;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\models\Videos;

$this->title = 'Incoming Invoice Details';
?>
<div class="row">
    <div class="col-md-12">
        <h2 class="section-heading text-center">
            Incoming InVoice

        </h2>
    </div>
    <div class="col-md-12">
        <div class="head-text-container">
            <div class="sec-head left-sec-head">

                <div class="head-text col-md-3">
                    <b>Supplier</b> : <?= print_r($model->supplier->name); ?>
                </div>
                <div class="head-text col-md-3">
                    <b>Amount</b>  : <?= $model->invoiceTotalAmount; ?>
                </div>
                <div class="head-text col-md-3 ">
                    <b>Issue Date: </b>  <?= date('d/m/Y', strtotime($model->issue_date)); ?>
                </div>
                <div class="head-text col-md-3 ">
                    <b> Due Date: </b> <?= date('d/m/Y', strtotime($model->due_date)); ?>

                </div>

                <div class="head-text col-md-3">
                    <b> Payment: </b> <?php
                    if ($model->payment_status == '0') {
                        echo '<div class="badge bg-green">Paid</div>';
                    } else {
                        echo '<div class="badge bg-red"> Not Paid</div>';
                    }
                    ?>
                </div>
                <div class="head-text col-md-3">
                    <b>Currency: </b> <?php
                    if ($model->payment_status == '1') {
                        echo '<div class="badge bg-green">Thai Baht</div>';
                    } else {
                        echo '<div class="badge bg-green"> USD</div>';
                    }
                    ?>
                </div>
            </div>

        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <table class="table table-bordered table-striped dataTable no-footer" id="ps_supplier_particulars">
            <thead>
                <tr>
                    <th colspan="100" class="text-center table-header">Particulars For this Invoice</th>
                </tr>
                <tr>
                    <th rowspan="2">Date Of service</th>
                    <th rowspan="2">Invoice No</th>
                    <th rowspan="2">Booking No</th>
                    <th rowspan="2">Tour Name</th>
                    <th rowspan="2">Guest Name</th>
                    <th colspan="2" class="text-center">PAX</th>
                    <th rowspan="2">Amount</th>
                </tr>
                <tr>
                    <th>Adult</th>
                    <th>Child</th>

                </tr>
            </thead>
            <tbody>
                <?php if (!empty($model->invoiceParticulars)) {
                    foreach ($model->invoiceParticulars as $index => $value) {
                        ?>
                <tr>
                <td><?= date('d/m/Y', strtotime($value->service_date)); ?></td>
                <td><?= $value->invoice_no; ?></td>
                <td><?= $value->booking_no; ?></td>
                <td><?= $value->tour_name; ?></td>
                <td><?= $value->guest_name; ?></td>
                <td><?= $value->adult_count; ?></td>
                <td><?= $value->child_count; ?></td>
                <td><?= $value->amount; ?></td>
                </tr>
                    <?php
                    }
                }else{
                ?>
                <tr>No data To Show </tr>
                <?php } ?>
            </tbody>
        </table>

    </div>
    <div class="col-md-12">
        <label>Uploaded File</label>
    <!--<iframe src="http://docs.google.com/viewer?url=http://soluciones.toshiba.com/media/downloads/products/4555c-5055cBrochure.pdf&embedded=true" width="600" height="780" style="border: none;"></iframe>-->
        <center><iframe src="http://docs.google.com/viewer?url=<?= Url::Base(true) . '/uploads/invoice/incoming/' . $model->upload_invoice; ?>&embedded=true" width="600" height="780" style="border: none;"></iframe></center>
    </div>











