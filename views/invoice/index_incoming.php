<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\base\View;
use yii\helpers\Url;
use yii\helpers\BaseUrl;

$this->title = 'Incoming Invoice List';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('http://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css');
$this->registerJsFile('http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<h1 class="page-title"> Incoming Invoice list
    <small>All incoming Invoices are listed here</small>
</h1>


<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h1>Incoming Invoice Table</h1>
                        <a class="pull-right btn btn-primary" href="<?php echo Url::to(['invoice/incoming-invoice-create']); ?>"> Create Invoice</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <?php if (Yii::$app->session->hasFlash('excelFormSubmitted')) { ?>
                            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button> <?php echo Yii::$app->session->getFlash('excelFormSubmitted'); ?></div>
                        <?php } ?>
                        <div class="table-responsive" style="width: 100%">
                            <table class="table table-bordered table-striped" id='myTable'>
                                <thead>
                                    <tr>
                                        <th>issue_date</th>
                                        <th>due_date</th>
                                        <th>credit Time Line</th>
                                        <th>Payment Status</th>
                                        <th>Amount</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>


                                <tbody>

                                    <?php
                                    foreach ($model as $index => $value) {
                                        ?>
                                        <tr>
                                            <td><?php echo $value->issue_date ?></td>
                                            <td><?php echo $value->due_date ?></td>
                                            <td><?php
                                                $date = date("Y-m-d");
                                                $data = ($value->due_date);
                                                $d_start = new DateTime($date);
                                                $d_end = new DateTime($data);
                                                $diff = $d_start->diff($d_end);
                                                echo $diff->format("%R%a days");
                                                ?></td>
                                            <td><?php
                                                if ($value->payment_status == '1') {
                                                    echo '<span class="badge bg-green">Not Paid</span>';
                                                } else {
                                                    echo '<span class="badge bg-red"> Paid</span>';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?=$value->invoiceTotalAmount;?>
                                            </td>
                                            <td>
                                                <a href="<?php echo Yii::$app->request->baseurl . "/invoice/incoming-invoice-edit/" . $value->id ?>"><span class="badge bg-blue"><i class="fa fa-pencil"></i> Update</span></a>&nbsp;&nbsp;&nbsp;
                                                <a href="<?php echo Yii::$app->request->baseurl . "/invoice/incoming-invoice-delete/" . $value->id ?>"><span class="badge bg-red"><i class="fa fa-trash"></i>Delete </span></a>
                                                <a href="<?php echo Yii::$app->request->baseurl . "/invoice/incoming-invoice-view/" . $value->id ?>"><span class="badge bg-red"><i class="fa fa-eye"></i>View </span></a>
                                            </td>

                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- .box-body -->

                </div>

            </div>
        </div>
    </section>
</div>
<?php
$this->registerJs(
        '$(document).ready( function () {
    $("#myTable").DataTable();
} );'
);
?>



