<style>
    .autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
    .autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
    .autocomplete-selected { background: #F0F0F0; }
    .autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
    .autocomplete-group { padding: 2px 5px; }
    .autocomplete-group strong { display: block; border-bottom: 1px solid #000; }

</style>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\BaseUrl;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use app\models\Supplier;
use yii\grid\ActionColumn;
use app\components\CustomPagination;
use richardfan\widget\JSRegister;

$this->title = 'Invoice Update';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('@web/assets/admin/js/jquery.autocomplete.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<h1><?= Html::encode($this->title) ?></h1>
<p>
    Update a correctly formatted Invoice.
</p>

<div class="row">
    <?php $form = ActiveForm::begin(['id' => 'incoming', 'options' => ['enctype' => 'multipart/form-data', 'method' => 'post']]); ?>
    <div class="col-lg-12">
        <div class="col-md-2">
            <?= $form->field($model, 'supplieir_id')->dropDownList(ArrayHelper::map(Supplier::find()->where(['<>', 'status', '2'])->asArray()->all(), 'supplier_id', 'name'), ['prompt' => 'Choose...']) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'issue_date')->textInput(['maxlength' => true, 'class' => 'datepicker form-control']) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'due_date')->textInput(['maxlength' => true, 'class' => 'datepicker form-control']) ?> 
        </div>
        <div class="col-md-2">
            <?php echo $form->field($model, 'currency')->radioList(array('0' => 'usd', '1' => 'Thaibaht')); ?>
        </div>
        <div class="col-md-2">
            <?php echo $form->field($model, 'payment_status')->radioList(array('0' => 'Paid', '1' => 'Not Paid')); ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'file')->fileInput() ?>         
        </div>
        <div class="col-md-12">
            <table class="table table-bordered table-striped dataTable no-footer" id="ps_supplier_particulars">
                <thead>
                    <tr>
                        <th colspan="100" class="text-center">Enter Each Particulars</th>
                    </tr>
                    <tr>
                        <th rowspan="2">Date Of service</th>
                        <th rowspan="2">Invoice No</th>
                        <th rowspan="2">Booking No</th>
                        <th rowspan="2">Tour Name</th>
                        <th rowspan="2">Type</th>
                        <th rowspan="2">Guest Name</th>
                        <th colspan="2" class="text-center">PAX</th>
                        <th rowspan="2">Amount</th>
                        <th rowspan="2"><button type="button" id="" class="add-item btn btn-primary">Add More</button></th>
                    </tr>
                    <tr>
                        <th >Adult</th>
                        <th >Child</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($model->invoiceParticulars)) {
                        foreach ($model->invoiceParticulars as $index => $value) {
                            ?>

                            <tr>
                        <input type="hidden" class="datepicker form-control" name="IncomingInvoiceParticulars[<?= $index; ?>][id]" value="<?= $value->id; ?>">
                        <td><input type="text" class="datepicker form-control" name="IncomingInvoiceParticulars[<?= $index; ?>][service_date]" value="<?=  date('d/m/Y',strtotime($value->service_date)); ?>"></td>
                        <td><input type="text" class="form-control" name="IncomingInvoiceParticulars[<?= $index; ?>][invoice_no]" value="<?= $value->invoice_no; ?>"></td>
                        <td><input type="text" class="ps-auto-booking-id form-control" name="IncomingInvoiceParticulars[<?= $index; ?>][booking_no]" value="<?= $value->booking_no; ?>"></td>
                        <td><select  class="ps-tour-name form-control" name="IncomingInvoiceParticulars[<?= $index; ?>][tour_name]" >

                                <?php foreach ($model_tour_name as $index1 => $value1): ?>

                                    <option value="<?= $value1; ?>"<?= $value->tour_name == $value1 ? 'selected' : ''; ?>><?php echo $value1 ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <select class="form-control" name="IncomingInvoiceParticulars[0][type]">
                                <option value="">
                                    Choose Type
                                </option>
                                <option value="PRIVATE">
                                    PRIVATE
                                </option>
                                <option value="SIC">
                                    SIC
                                </option>
                                <option value="TICKETONLY">
                                    Ticket Only
                                </option>
                            </select>
                        </td>
                        

                        
                        <td><input type="text" class="ps-guest-name form-control" name="IncomingInvoiceParticulars[<?= $index; ?>][guest_name]" value="<?= $value->guest_name; ?>"></td>
                        <td><input type="number" class="form-control" name="IncomingInvoiceParticulars[<?= $index; ?>][adult_count]" value="<?= $value->adult_count; ?>"></td>
                        <td><input type="number" class="form-control" name="IncomingInvoiceParticulars[<?= $index; ?>][child_count]" value="<?= $value->child_count; ?>"></td>
                        <td><input type="number" class="form-control" name="IncomingInvoiceParticulars[<?= $index; ?>][amount]" step="0.01" value="<?= $value->amount; ?>"></td>
                        <td><button  type="button" id="" class="btn btn-danger">Remove</button></td>
                        <tr>
                            <?php
                        }
                    }else {
                        ?>
                    <tr>No data To Show </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>



        <div class="col-md-12 form-group">
            <?= Html::submitButton('Submit', ['class' => 'pull-right btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php JSRegister::begin(); ?>
<script>
    $(".datepicker").datepicker({

        dateFormat: 'yy-mm-dd'
    });
    $('.ps-auto-booking-id').devbridgeAutocomplete({
        serviceUrl: '<?= Url::to(Baseurl::base() . '/invoice/ajax-auto-complete-bookingno'); ?>',
        onSelect: function (suggestion) {
            $(this).val(suggestion.data);
            $(this).parents("tr").first().find(".ps-guest-name").val(suggestion.value);
        }
    });
    $(document).ready(function () {
        $("#ps_supplier_particulars").on("click", ".btn.btn-danger", function (event) {
            $(this).closest("tr").remove();
        });
        var supplier_counter = 1;
        $(".add-item").on('click', function () {
            $.ajax({
                type: "POST",
                evalScripts: true,
                url: '<?= Url::to(Baseurl::base() . '/invoice/ajax-invoice-particular-template-add'); ?>',
                data: ({_csrf: yii.getCsrfToken(), counter: supplier_counter}),
                success: function (data) {
                    $("#ps_supplier_particulars").find('tbody:last').append('<tr>').find('tr:last').append(data);
                    supplier_counter++;
                    $('.ps-auto-booking-id').devbridgeAutocomplete({
                        serviceUrl: '<?= Url::to(Baseurl::base() . '/invoice/ajax-auto-complete-bookingno'); ?>',
                        onSelect: function (suggestion) {
                            $(this).val(suggestion.data);
                            $(this).parents("tr").first().find(".ps-guest-name").val(suggestion.value);
                        }
                    });
                    $(".datepicker").datepicker({
                        dateFormat: 'yy-mm-dd'
                    });
                }
            });
        });
    });
</script>
<?php JSRegister::end(); ?>