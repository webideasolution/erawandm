<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\BaseUrl;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use app\models\Supplier;
use yii\grid\ActionColumn;
use app\components\CustomPagination;
use richardfan\widget\JSRegister;
?>

<td>
    <input type="text" class="datepicker form-control" name="IncomingInvoiceParticulars[<?= $counter; ?>][service_date]">
</td>
<td>
    <input type="text" class="form-control" name="IncomingInvoiceParticulars[<?= $counter; ?>][invoice_no]">
</td>
<td><input type="text" class="ps-auto-booking-id form-control" name="IncomingInvoiceParticulars[<?= $counter; ?>][booking_no]"></td>
<td>
    <select  class="ps-tour-name form-control" name="IncomingInvoiceParticulars[<?= $counter; ?>][tour_name]" >

        <?php foreach ($model_tour_name as $index => $value): ?>

            <option value="<?= $value; ?>" data-id="<?= $index; ?>"><?php echo $value ?></option>
        <?php endforeach; ?>
    </select>
</td>
<td>
    <select class="ps-tour-type form-control" name="IncomingInvoiceParticulars[<?= $counter; ?>][type]">
        <option value="">
            Choose Type
        </option>
        <option value="PRIVATE">
            PRIVATE
        </option>
        <option value="SIC">
            SIC
        </option>
        <option value="TICKETONLY">
            Ticket Only
        </option>
    </select>
</td>
<td><input type="text" class="ps-guest-name form-control" name="IncomingInvoiceParticulars[<?= $counter; ?>][guest_name]"></td>
<td><input type="number" class="ps-adult-count form-control" name="IncomingInvoiceParticulars[<?= $counter; ?>][adult_count]"></td>
<td><input type="number" class="ps-child-count  form-control" name="IncomingInvoiceParticulars[<?= $counter; ?>][child_count]"></td>
<td><input type="number" class="ps-amount form-control" name="IncomingInvoiceParticulars[<?= $counter; ?>][amount]" step="<?= $counter; ?>.<?= $counter; ?>1"></td>
<td><button  type="button" id="" class="btn btn-danger">Remove</button></td>

<?php JSRegister::begin(); ?>
<script>
    $(".ps-tour-type").on('change', function () {
        var type = $(this).find(':selected').val();
        var item = $(this);
        var tour = $(this).parents("tr").first().find(".ps-tour-name").find(':selected').attr('data-id');
        var booking_id = $(this).parents("tr").first().find(".ps-auto-booking-id").val();
        $.ajax({
            type: "POST",
            evalScripts: true,
            dataType:'html/text',
            url: '<?= Url::to(Baseurl::base() . '/invoice/ajax-invoice-details-add'); ?>',
            data: ({_csrf: yii.getCsrfToken(), tour: tour, type: type, booking_id: booking_id}),
            success: function (data) {
                console.log(data);
                if (type == 'PRIVATE') {
                    $(item).parents("tr").first().find(".ps-adult-count").attr('disabled', true);
                    $(item).parents("tr").first().find(".ps-child-count").attr('disabled', true);
                } else {
                    var jsonobj = JSON.parse(data);
                    $(item).parents("tr").first().find(".ps-adult-count").val(jsonobj.adult_count);
                    $(item).parents("tr").first().find(".ps-child-count").val(jsonobj.child_count);
                    $(item).parents("tr").first().find(".ps-amount").val(jsonobj.amount);
                }
            }
        });
    });
</script>
<?php JSRegister::end(); ?>