<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use app\models\Supplier;
$this->title = 'Invoice Edit';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<p>
    UPdate a already uploaded Invoice.
</p>


<div class="row">
    <div class="col-lg-5">

        <?php $form = ActiveForm::begin(['id' => 'incoming', 'options' => ['enctype' => 'multipart/form-data', 'method' => 'post']]); ?>

         <?= $form->field($model, 'supplieir_id') ->dropDownList( ArrayHelper::map(Supplier::find()->asArray()->all(), 'supplier_id', 'name') ) ?>
        <?= $form->field($model, 'name')->textInput(['type' => 'text', 'maxlength' => true, 'class' => 'form-control']) ?>
          <?= $form->field($model, 'destination')->textInput(['type' => 'text', 'maxlength' => true, 'class' => 'form-control']) ?>
            <?= $form->field($model, 'startdate')->textInput(['maxlength' => true, 'class' => 'datepicker form-control']) ?>
         <?= $form->field($model, 'enddate')->textInput(['maxlength' => true, 'class' => 'datepicker form-control']) ?>

        <?= $form->field($model, 'amount')->textInput(['type' => 'number', 'maxlength' => true, 'class' => 'form-control']) ?>


        <?= $form->field($model, 'issue_date')->textInput(['maxlength' => true, 'class' => 'datepicker form-control']) ?>

        <?= $form->field($model, 'due_date')->textInput(['maxlength' => true, 'class' => 'datepicker form-control']) ?>
        <?php echo $model->upload_invoice ?>" 
        <a href="<?php echo Yii::$app->request->baseurl . "/uploads/invoice/outgoing/" . $model->upload_invoice ?>" download>See previously uploaded file</a> 

        <?= $form->field($model, 'file')->fileInput() ?>         
       <?= $form->field($model, 'payment_status')->radioList(array('0' => 'Paid', '1' => 'Not Paid')); ?>
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
<?php JSRegister::begin(); ?>
<script>
    $(".datepicker").datepicker({
        format: "yy-mm-dd"
    });
</script>
<?php JSRegister::end(); ?>