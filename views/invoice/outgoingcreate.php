<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\grid\ActionColumn;
use app\models\Supplier;
use app\components\CustomPagination;
use richardfan\widget\JSRegister;

$this->title = 'Invoice Create';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<p>
    Create a correctly formatted Invoice.
</p>

<div class="row">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin(['id' => 'incoming', 'options' => ['enctype' => 'multipart/form-data', 'method' => 'post']]); ?>
         <?= $form->field($model, 'supplieir_id') ->dropDownList( ArrayHelper::map(Supplier::find()->asArray()->all(), 'supplier_id', 'name'),['prompt'=>'Choose...']) ?>
         <?= $form->field($model, 'name')->textInput(['type' => 'text', 'maxlength' => true, 'class' => 'form-control']) ?>
          <?= $form->field($model, 'destination')->textInput(['type' => 'text', 'maxlength' => true, 'class' => 'form-control']) ?>
        <?= $form->field($model, 'amount')->textInput(['type' => 'number', 'maxlength' => true, 'class' => 'form-control']) ?>
           <?= $form->field($model, 'startdate')->textInput(['maxlength' => true, 'class' => 'datepicker form-control']) ?>
 <?= $form->field($model, 'enddate')->textInput(['maxlength' => true, 'class' => 'datepicker form-control']) ?>

        <?= $form->field($model, 'issue_date')->textInput(['maxlength' => true, 'class' => 'datepicker form-control']) ?>

        <?= $form->field($model, 'due_date')->textInput(['maxlength' => true, 'class' => 'datepicker form-control']) ?>
         <?= $form->field($model, 'payment_status')->radioList(array('0' => 'Paid', '1' => 'Not Paid')); ?>
        <?= $form->field($model, 'file')->fileInput() ?>         
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php JSRegister::begin(); ?>
<script>
    $(".datepicker").datepicker({
        format: "yy-mm-dd"
    });
</script>
<?php JSRegister::end(); ?>