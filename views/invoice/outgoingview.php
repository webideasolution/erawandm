<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\base\View;
use yii\helpers\Url;
use yii\helpers\BaseUrl;

$this->title = 'Outgoing Invoice List';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('http://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css');
$this->registerJsFile('http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<h1 class="page-title"> Outgoing Invoice list
    <small>All Outgoing Invoices are listed here</small>
</h1>


<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h1>Outgoing Invoice Table</h1>
                        <a class="pull-right btn btn-primary" href="<?php echo Url::to(['invoice/outgoing-invoice-create']); ?>"> Create Invoice</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <?php if (Yii::$app->session->hasFlash('excelFormSubmitted')) { ?>
                            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button> <?php echo Yii::$app->session->getFlash('excelFormSubmitted'); ?></div>
                        <?php } ?>
                        <div class="table-responsive" style="width: 100%">
                            <table class="table table-bordered table-striped" id='myTable'>
                                <thead>
                                    <tr>
                                        <th>supplier_id</th>
                                        <th>Name</th>
                                        <th>Destination</th>
                                        <th>start Date</th>
                                        <th>End Date</th>
                                        <th>amount</th>
                                        <th>issue_date</th>
                                        <th>due_date</th>
                                         <th>credit Time Line</th>
                                        <th>Payment Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>


                                <tbody>

                                    <?php
                                    foreach ($model as $index => $value) {
                                        ?>
                                        <tr>
                                            <td><?php echo $value->supplieir_id ?></td>
                                             <td><?php echo $value->name ?></td>
                                            <td><?php echo $value->destination ?></td>
                                            <td><?php echo $value->startdate ?></td>
                                            <td><?php echo $value->enddate ?></td>
                                            <td><?php echo $value->amount ?></td>
                                            <td><?php echo $value->issue_date ?></td>
                                            <td><?php echo $value->due_date ?></td>
<!--                                            <td><a class="badge bg-info" href="<?php echo Yii::$app->request->baseurl . "/uploads/invoice/outgoing/" . $value->upload_invoice ?>" download><?php echo $value->upload_invoice ?></a> </td>-->
                                           <td><?php
                                           $date=date("Y-m-d");
                                           $data=($value->due_date);
                                           $d_start = new DateTime($date);
                                            $d_end  = new DateTime($data); 
                                            $diff = $d_start->diff($d_end); 
                                           echo $diff->format("%R%a days");?></td>
                                            <td><?php
                                                if ($value->payment_status == '1') {
                                                    echo '<span class="badge bg-green">NotPaid</span>';
                                                } else {
                                                    echo '<span class="badge bg-red"> Paid</span>';
                                                }
                                                ?>
                                            </td>


                                            <td>
                                                <a href="<?php echo Yii::$app->request->baseurl . "/invoice/outgoing-invoice-edit?id=" . $value->id ?>"><span class="badge bg-blue"><i class="fa fa-trash"></i> Update</span></a>&nbsp;&nbsp;&nbsp;
                                                <a href="<?php echo Yii::$app->request->baseurl . "/invoice/outgoing-invoice-delete?id=" . $value->id ?>"><span class="badge bg-red"><i class="fa fa-pencil"></i>Delete </span></a>
                                               <a href="<?php echo Yii::$app->request->baseurl . "/invoice/out-view?id=" . $value->id ?>"><span class="badge bg-red"><i class="fa fa-eye"></i>View </span></a>
                                            </td>

                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- .box-body -->

                </div>

            </div>
        </div>
    </section>
</div>
<?php
$this->registerJs(
        '$(document).ready( function () {
    $("#myTable").DataTable();
} );'
);
?>



