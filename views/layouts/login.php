<?php

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\web\AssetManager;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <?= Html::csrfMetaTags() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="WebIdeasole">
        <?php $this->registerLinkTag(['href' => Yii::$app->request->baseUrl . 'web/assets/admin/favicon.ico']); ?>

        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <!--Core CSS -->
        <?php $this->registerCssFile("@web/assets/admin/bs3/css/bootstrap.min.css"); ?>
        <?php $this->registerCssFile("@web/assets/admin/css/bootstrap-reset.css"); ?>
        <?php $this->registerCssFile("@web/assets/admin/font-awesome/css/font-awesome.css"); ?>

        <!-- Custom styles for this template -->
        <?php $this->registerCssFile("@web/assets/admin/css/style.css"); ?>
        <?php $this->registerCssFile("@web/assets/admin/css/style-responsive.css"); ?>

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]>
        <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="login-body">
        <?php $this->beginBody() ?>
        <div class="container">

                    <?= $content; ?>
            

        </div>
        <!-- Placed js at the end of the document so the pages load faster -->

        <!--Core js-->
        <?php $this->registerJsFile('@web/assets/admin/js/jquery.js'); ?>
        <?php $this->registerJsFile('@web/assets/admin/bs3/js/bootstrap.min.js'); ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

