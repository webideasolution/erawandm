<?php

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <?= Html::csrfMetaTags() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="WebIdeaSole">

        <meta http-equiv=”X-UA-Compatible” content=”IE=EmulateIE9”>
        <meta http-equiv=”X-UA-Compatible” content=”IE=9”>
        <?php $this->head() ?>
        <?php $this->registerLinkTag(['href' => Yii::$app->request->baseUrl . 'web/assets/admin/images/favicon.png']); ?>
        <title><?= Html::encode($this->title) ?></title>

        <!-- Custom styles for this template -->
        <?php $this->registerCssFile("@web/assets/admin/css/pdf.css"); ?>

    </head>
    <body>
        <?php $this->beginBody() ?>
        <section id="container">
            <section id="main-content">
                <section class="wrapper">
                    <?= $content; ?>
                </section>
            </section>
            <!--main content end-->
            <!--right sidebar start-->
            <?php ///echo $this->render('/layouts/_sidebar_right'); ?>
            <!--right sidebar end-->
        </section>

        <!-- END HEAD -->


        <!-- END CORE PLUGINS -->

        <!-- END THEME LAYOUT SCRIPTS -->


        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

