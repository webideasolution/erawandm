<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a class="<?= Yii::$app->controller->id == 'dashboard' ? 'active' : ''; ?>" href="<?= Yii::$app->request->baseUrl . '/dashboard/index'; ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;" class="<?= Yii::$app->controller->id == 'voucher' ? 'active' : ''; ?>">
                        <i class="fa fa-laptop"></i>
                        <span>Vouchers</span>
                    </a>
                    <ul class="sub">
                        <li class="<?= Yii::$app->controller->id == 'voucher' && Yii::$app->controller->action->id == 'index' ? 'active' : ''; ?>"><a href="<?= Yii::$app->request->baseUrl . '/voucher/index'; ?>">Booking</a></li>
                        <li class="<?= Yii::$app->controller->id == 'voucher' && Yii::$app->controller->action->id == 'excelupload' ? 'active' : ''; ?>"><a href="<?= Yii::$app->request->baseUrl . '/voucher/excelupload'; ?>">Upload EXCEL</a></li>
                        <li class="<?= Yii::$app->controller->id == 'voucher' && Yii::$app->controller->action->id == 'add-voucher' ? 'active' : ''; ?>"><a href="<?= Yii::$app->request->baseUrl . '/voucher/add-voucher'; ?>">Add Voucher</a></li>
                    </ul>
                </li>
                <li>
                    <a class="<?= Yii::$app->controller->id == 'invoice' ? 'active' : ''; ?>" href="<?= Yii::$app->request->baseUrl . '/dashboard/index'; ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Invoice</span>
                    </a>
                    <ul class="sub">
                        <li class="<?= Yii::$app->controller->id == 'invoice' && Yii::$app->controller->action->id == 'incoming-invoice-index' ? 'active' : ''; ?>"><a href="<?= Yii::$app->request->baseUrl . '/invoice/incoming-invoice-index'; ?>">Incoming Invoices</a></li>
                        <li class="<?= Yii::$app->controller->id == 'invoice' && Yii::$app->controller->action->id == 'outgoing-invoice-view' ? 'active' : ''; ?>"><a href="<?= Yii::$app->request->baseUrl . '/invoice/outgoing-invoice-view'; ?>">Outgoing Invoices</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;" class="<?= Yii::$app->controller->id == 'report' ? 'active' : ''; ?>">
                        <i class="fa fa-laptop"></i>
                        <span>Reports</span>
                    </a>
                    <ul class="sub">
                        <li class="<?= Yii::$app->controller->id == 'report' && Yii::$app->controller->action->id == 'daily-transfer-report' ? 'active' : ''; ?>"><a href="<?= Yii::$app->request->baseUrl . '/report/daily-transfer-report'; ?>">Daily Transfers</a></li>
                        <li class="<?= Yii::$app->controller->id == 'report' && Yii::$app->controller->action->id == 'daily-tour-report' ? 'active' : ''; ?>"><a href="<?= Yii::$app->request->baseUrl . '/report/daily-tour-report'; ?>">Daily Tours</a></li>
                        <li><a href="language_switch.html">Booking</a></li>
                        <li><a href="language_switch.html">Supplier</a></li>
                    </ul>
                </li>
                 <li>
                    <a class="<?= Yii::$app->controller->id == 'supplier' ? 'active' : ''; ?>" href="<?= Yii::$app->request->baseUrl . '/supplier/index'; ?>" >
                        <i class="fa fa-user"></i>
                        <span>Supplier</span>
                    </a>
                      
                </li>

                <li>
                    <a class="<?= Yii::$app->controller->id == 'car' ? 'active' : ''; ?>" href="<?= Yii::$app->request->baseUrl . '/car/index'; ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Available Cars</span>
                    </a>
                </li>
                <li>
                    <a class="<?= Yii::$app->controller->id == 'agent' ? 'active' : ''; ?>" href="<?= Yii::$app->request->baseUrl . '/dashboard/index'; ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Agents</span>
                    </a>
                </li>
                 <li>
                    <a class="<?= Yii::$app->controller->id == 'settings' ? 'active' : ''; ?>" href="<?= Yii::$app->request->baseUrl . '/dashboard/index'; ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Site Settings</span>
                    </a>
                    <ul class="sub">
                        <li class="<?= Yii::$app->controller->id == 'settings' && Yii::$app->controller->action->id == 'sight' ? 'active' : ''; ?>"><a href="<?= Yii::$app->request->baseUrl . '/settings/index'; ?>"> Currency</a></li>
                       
                    </ul>
                </li>
            </ul>         
        </div>
        <!-- sidebar menu end-->
    </div>
</aside>