<?php

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <?= Html::csrfMetaTags() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="WebIdeaSole">

        <meta http-equiv=”X-UA-Compatible” content=”IE=EmulateIE9”>
        <meta http-equiv=”X-UA-Compatible” content=”IE=9”>
        <?php $this->head() ?>
        <?php $this->registerLinkTag(['href' => Yii::$app->request->baseUrl . 'web/assets/admin/images/favicon.png']); ?>
        <title><?= Html::encode($this->title) ?></title>

        <!--Core CSS -->
        <?php $this->registerCssFile("@web/assets/admin/bs3/css/bootstrap.min.css"); ?>
        <?php $this->registerCssFile("@web/assets/admin/js/jquery-ui/jquery-ui-1.10.1.custom.min.css"); ?>
        <?php $this->registerCssFile("@web/assets/admin/css/bootstrap-reset.css"); ?>
        <?php $this->registerCssFile("@web/assets/admin/font-awesome/css/font-awesome.css"); ?>
        <?php $this->registerCssFile("@web/assets/admin/js/jvector-map/jquery-jvectormap-1.2.2.css"); ?>
        <?php $this->registerCssFile("@web/assets/admin/css/clndr.css"); ?>
        <?php $this->registerCssFile("https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css"); ?>
        <!--clock css-->
        <?php // $this->registerCssFile("@web/assets/admin/js/css3clock/css/style.css"); ?>
        <!--Morris Chart CSS -->
        <?php // $this->registerCssFile("@web/assets/admin/js/morris-chart/morris.css"); ?>
        <!-- Custom styles for this template -->
        <?php $this->registerCssFile("@web/assets/admin/css/style.css"); ?>
        <?php $this->registerCssFile("@web/assets/admin/css/style-responsive.css"); ?>
        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]>
        <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php $this->beginBody() ?>
        <section id="container">
            <!--header start-->
            <?= $this->render('/layouts/_header'); ?>

            <!--header end-->
            <!--sidebar start-->
            <?= $this->render('/layouts/_sidebar_left'); ?>
            <!--sidebar end-->
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <?= $content; ?>
                </section>
            </section>
            <!--main content end-->
            <!--right sidebar start-->
            <?php ///echo $this->render('/layouts/_sidebar_right'); ?>
            <!--right sidebar end-->
        </section>

        <!-- END HEAD -->


        <?=
        Breadcrumbs::widget([
            'homeLink' => [
                'label' => Yii::t('yii', 'Dashboard'),
                'url' => Yii::$app->request->baseUrl . '/dashboard/index',
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
        ?>

<!-- END CONTAINER -->
<!-- BEGIN CORE PLUGINS -->
<?php $this->registerJsFile('@web/assets/admin/js/jquery.js'); ?>
<?php $this->registerJsFile('@web/assets/admin/js/jquery-ui/jquery-ui-1.10.1.custom.min.js'); ?>
<?php $this->registerJsFile('@web/assets/admin/bs3/js/bootstrap.min.js'); ?>
<?php $this->registerJsFile('@web/assets/admin/js/jquery.dcjqaccordion.2.7.js'); ?>
<?php $this->registerJsFile('@web/assets/admin/js/jquery.scrollTo.min.js'); ?>
<?php $this->registerJsFile('@web/assets/admin/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js'); ?>
<?php $this->registerJsFile('@web/assets/admin/js/jquery.nicescroll.js'); ?>
<?php $this->registerJsFile('@web/assets/admin/js/scripts.js'); ?>
<?php $this->registerJsFile('https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js'); ?>


<!-- END CORE PLUGINS -->

<!-- END THEME LAYOUT SCRIPTS -->


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

