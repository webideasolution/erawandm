<?php

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <?= Html::csrfMetaTags() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="WebIdeaSole">

        <meta http-equiv=”X-UA-Compatible” content=”IE=EmulateIE9”>
        <meta http-equiv=”X-UA-Compatible” content=”IE=9”>
        <?php $this->head() ?>
        <?php $this->registerLinkTag(['href' => Yii::$app->request->baseUrl . 'web/assets/admin/images/favicon.png']); ?>
        <title><?= Html::encode($this->title) ?></title>

        <!--Core CSS -->
        <?php $this->registerCssFile("@web/assets/admin/bs3/css/bootstrap.min.css"); ?>
        <?php $this->registerCssFile("@web/assets/admin/css/bootstrap-reset.css"); ?>
        <?php $this->registerCssFile("@web/assets/admin/font-awesome/css/font-awesome.css"); ?>
        <!--clock css-->
        <?php // $this->registerCssFile("@web/assets/admin/js/css3clock/css/style.css"); ?>
        <!--Morris Chart CSS -->
        <?php // $this->registerCssFile("@web/assets/admin/js/morris-chart/morris.css"); ?>
        <!-- Custom styles for this template -->
        <?php $this->registerCssFile("@web/assets/admin/css/style.css"); ?>
        <?php $this->registerCssFile("@web/assets/admin/css/style-responsive.css"); ?>
        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]>
        <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="body-500">
        <?php $this->beginBody() ?>
        <div class="error-head"> </div>
        <div class="container ">
            <section class="error-wrapper text-center">
                <div class="error-desk">
                    <?= $content; ?>
                </div>
            </section>
            <?php $this->endBody() ?>
        </div>
    </div>
    </body>
</html>
<?php $this->endPage() ?>

