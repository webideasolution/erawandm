<html class="hawkExperience" dir="ltr" lang="_" data-langpack="/invoice/">

    <head>
        <title>Transfer Report</title>

    </head>


    <body>

        <div class="container">
            <div class="report-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="report-heading text-center" style="width: 100%;background-color: #00b5b1;margin-bottom: 50px;font-size: 40px;color: white;text-transform: uppercase;padding: 40px;line-height: 1.5;">
                            Erawan International Corporation Co.
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="img-container">
                                <!--<img src="images/chart.JPG" alt="" class="img-responsive">-->
                        </div>
                    </div>
                    <div class="">
                        <div class="col-md-12">
                            <h2 class="section-heading text-center"style="font-size: 2rem;background-color: #dedede;padding: 10px 0;color: #626265;font-weight: 600;letter-spacing: .5px;">
                                Here is your  Transfer Report From <?= $date; ?>.
                                <!--Tour report from 12/03/2018 to 20/03/2018-->
                            </h2>
                        </div>
                        Report Generated On <?= date('Y-m-d H:i:s'); ?>.

                        <!--                        <div class="col-md-12">
                                                    <div class="head-text-container" style="display: flex;justify-content: space-between;align-items: center;padding: 5px 0;width: 95%;margin: 0 auto;">
                                                        <div class="sec-head left-sec-head" style="box-sizing: border-box;">
                                                            <span class="head-text"style="    width: 90px;display: inline-block;">
                                                                Demo Field
                                                            </span>
                                                            <span class="head-text">
                                                                Demo Field
                                                            </span>
                                                            <span class="head-text booking-id" style="width: 250px;">
                                                                Demo Field
                                                            </span>
                                                        </div>
                                                        <div class="sec-head right-sec-head" style="box-sizing: border-box;">
                                                            <span class="head-text">
                                                                Demo Field
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>-->

                        <div class="col-sm-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <section id="unseen">
                                        <table class="table table-bordered table-striped table-condensed" id="myTable">
                                            <thead>
                                                <tr>
                                                    <th colspan="15" class="text-center table-header">TRANSFER ITINERARY</th>
                                                </tr>
                                                <tr>
                                                    <th>Customer Name</th>
                                                    <th>Cars</th>
                                                    <th class="numeric">Adult</th>
                                                    <th class="numeric">Child</th>
                                                    <th class="numeric">Infant</th>
                                                    <th>Pickup Time</th>
                                                    <th class="numeric">Pickup</th>
                                                    <th class="numeric">Drop-Off</th>
                                                    <th>Order Date</th>
                                                    <th>Transfer Date</th>
                                                    <th class="numeric">Type</th>
                                                    <th class="numeric">Transfer Type</th>
                                                    <th class="numeric">Booking ID</th>
                                                    <th class="numeric">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (count($model) > 0) {
                                                    foreach ($model as $val) {
                                                        ?>
                                                <tr>
                                                    <td><?= $val->voucher->billing_name; ?></td>
                                                    <?php
                                                    $total_pax = $val->voucher->adult_count + $val->voucher->child_count + $val->voucher->infant_count;
                                                    $output;
                                                    if ($total_pax > 0) {
                                                        if ($total_pax <= 3) {
                                                            $output = 'Car Sedan';
                                                        } else if ($total_pax <= 5) {

                                                            $output = 'Sedan';
                                                        } else if ($total_pax <= 8) {

                                                            $output = 'Van';
                                                        } else {
                                                            $van_count = 1;
                                                            $sedan_count = 0;
                                                            $car_sedan_count = 0;
                                                            $total_pax = $total_pax - 8;

                                                            while ($total_pax > 0) {
                                                                if ($total_pax <= 3) {
                                                                    $car_sedan_count++;
                                                                    $total_pax = $total_pax - 3;
                                                                } else if ($total_pax <= 5) {
                                                                    $sedan_count++;
                                                                    $total_pax = $total_pax - 5;
                                                                } else if ($total_pax <= 8) {
                                                                    $van_count++;
                                                                    $total_pax = $total_pax - 8;
                                                                }
                                                            }

                                                            $output = $van_count >= 1 ? $van_count . ' Vans,' : '1 Van,';
                                                            $output .= ' ' . $sedan_count > 0 ? $sedan_count == 1 ? '1 Sedan,' : $sedan_count . ' Sedans,' : '';
                                                            $output .= ' ' . $car_sedan_count > 0 ? $car_sedan_count == 1 ? '1 Car Sedan' : $van_count . 'Car Sedans' : '';
                                                        }
                                                    } else {
                                                        $output = 'No Car Needed';
                                                    }
                                                    ?>
                                                    <td class="numeric"><?= $output; ?></td>
                                                    <td class="numeric"><?= $val->voucher->adult_count; ?></td>
                                                    <td class="numeric"><?= $val->voucher->child_count; ?></td>
                                                    <td class="numeric"><?= $val->voucher->infant_count; ?></td>
                                                    <td><?= !empty($val->transfer_time) ? $val->transfer_time : 'NA'; ?></td>
                                                    <td class="numeric"><?= $val->transfer_source; ?></td>
                                                    <td class="numeric"><?= $val->transfer_destination; ?></td>
                                                    <td><?= date('d/m/Y', strtotime($val->created_at)); ?></td>
                                                    <td><?= date('d/m/Y', strtotime($val->transfer_date)); ?></td>
                                                    <td class="numeric"><?= $val->transfer_objective; ?></td>
                                                    <td class="numeric"><?= $val->transfer_type; ?></td>
                                                    <td class="numeric"><?= '#' . $val->voucher->id; ?></td>
                                                    <td class="numeric"><?php
                                                        if ($val->status == '1') {
                                                            echo '<span class="badge bg-green">Active</span>';
                                                        } else {
                                                            echo '<span class="badge bg-red">Inactive</span>';
                                                        }
                                                        ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </section>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>


        </div>	
    </body>

</html>