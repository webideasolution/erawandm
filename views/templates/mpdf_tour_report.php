
<html class="hawkExperience" dir="ltr" lang="_" data-langpack="/invoice/">

    <head>
        <title>Tour Report</title>

    </head>


    <body>

        <div class="container">
            <div class="report-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="report-heading text-center" style="width: 100%;background-color: #00b5b1;margin-bottom: 50px;font-size: 40px;color: white;text-transform: uppercase;padding: 40px;line-height: 1.5;">
                            Erawan International Corporation Co.
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="img-container">
                                <!--<img src="images/chart.JPG" alt="" class="img-responsive">-->
                        </div>
                    </div>
                    <div class="">
                        <div class="col-md-12">
                            <h2 class="section-heading text-center"style="font-size: 2rem;background-color: #dedede;padding: 10px 0;color: #626265;font-weight: 600;letter-spacing: .5px;">
                                Here is your custom Tour Report.
                                <!--Tour report from 12/03/2018 to 20/03/2018-->
                            </h2>
                        </div>
                        <div class="col-sm-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <section id="unseen">
                                        <table class="table table-bordered table-striped table-condensed" id="myTable">
                                            <thead>
                                                <tr>
                                                    <th colspan="14" class="text-center table-header">TOUR ITINERARY</th>
                                                </tr>
                                                <tr>
                                                    <th>Customer Name</th>
                                                    <th class="numeric">Adult</th>
                                                    <th class="numeric">Child</th>
                                                    <th class="numeric">Infant</th>
                                                    <th>pickup Time</th>
                                                    <th class="numeric">Meeting Point</th>
                                                    <th class="numeric">Destination</th>
                                                    <th class="numeric">Tour name</th>
                                                    <th>Order Date</th>
                                                    <th>Tour Date</th>
                                                    <th class="numeric">Tour Type</th>
                                                    <th class="numeric">Booking ID</th>
                                                    <th class="numeric">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $flag = "info";
                                                $flag1 = FALSE;
                                                if (count($model) > 0) {
                                                    foreach ($model as $index => $val) {

                                                        if ($flag1 == TRUE) {
                                                            ?>
                                                            <tr class="<?= $flag; ?>">
                                                                <?php
                                                                if (isset($model[$index + 1]) && strtoupper($model[$index + 1]->tour_date) == strtoupper($val->tour_date) && strpos(preg_replace('/\s+/u', '', strtoupper($model[$index + 1]->tour_name)), preg_replace('/\s+/u', '', strtoupper($val->tour_name)))) {

                                                                    $flag1 = TRUE;
                                                                } else {
                                                                    $flag = $flag == "info" ? "warning" : "info";
                                                                    $flag1 = FALSE;
                                                                }
                                                            } else if (isset($model[$index + 1]) && strtoupper($model[$index + 1]->tour_date) == strtoupper($val->tour_date) && strpos(preg_replace('/\s+/u', '', strtoupper($model[$index + 1]->tour_name)), preg_replace('/\s+/u', '', strtoupper($val->tour_name)))) {
                                                                ?>
                                                            <tr class="<?= $flag; ?>">
                                                                <?php
                                                                $flag1 = TRUE;
                                                            } else {
                                                                $flag1 = FALSE;
                                                                ?>
                                                            <tr>
                                                            <?php }
                                                            ?>
                                                            <td><?= $val->voucher->billing_name; ?></td>
                                                            <td class="numeric"><?= $val->voucher->adult_count; ?></td>
                                                            <td class="numeric"><?= $val->voucher->child_count; ?></td>
                                                            <td class="numeric"><?= $val->voucher->infant_count; ?></td>
                                                            <td><?= !empty($val->pickup_time) ? $val->pickup_time : 'NA'; ?></td>
                                                            <td class="numeric"><?= $val->meeting_point; ?></td>
                                                            <td class="numeric"><?= $val->destination; ?></td>
                                                            <td class="numeric"><?= $val->tour_name; ?></td>
                                                            <td><?= date('d/m/Y', strtotime($val->created_at)); ?></td>
                                                            <td><?= date('d/m/Y', strtotime($val->tour_date)); ?></td>
                                                            <td class="numeric"><?= $val->tour_type; ?></td>
                                                            <td class="numeric"><?= '#' . $val->voucher->id; ?></td>
                                                            <td class="numeric"><?php
                                                                if ($val->status == '1') {
                                                                    echo '<span class="badge bg-green">Active</span>';
                                                                } else {
                                                                    echo '<span class="badge bg-red">Inactive</span>';
                                                                }
                                                                ?></td>
                                                        </tr>

                                                        <?php
                                                    }
                                                }
                                                ?>

                                            </tbody>
                                        </table>
                                    </section>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>


        </div>	
    </body>

</html>