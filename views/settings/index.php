<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use richardfan\widget\JSRegister;
use app\components\CustomPagination;

$this->registerCssFile('http://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css');
$this->registerJsFile('http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->title = 'Currency List';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1 class="page-title"> Currency List
    <small>Currency are listed here</small>
</h1>


<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h1>Currency Table</h1>
                       
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

<?php if (Yii::$app->session->hasFlash('Currencyupdate')) { ?>
                            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close">

                                </button> <?php echo Yii::$app->session->getFlash('Currencyupdate'); ?></div>
<?php } ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
<!--                                    <th> SupplierId</th>
                                    <th>TourName</th>
                                    <th>Email</th>-->
                                    <th>value</th>
                                   <th>slug</th>
                                    
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($model as $val) {
                                    ?>
                                    <tr>
                                       
                                        <td><?php echo $val->value; ?></td>
                                        <td><?php 
                                         $value=$val->slug;
                                         $slug= ucwords(str_replace('_',' ', $value));
                                         echo $slug;
                                        ?></td>
                                        <td>
<!--                                            <a href="<?= Yii::$app->request->baseUrl . '/supplier/view/' . $val->id; ?> "><span class="badge bg-yellow"><i class="fa fa-eye"></i> View</span></a>&nbsp;&nbsp;&nbsp;-->
                                            <a href="<?= Yii::$app->request->baseUrl . '/settings/update/' . $val->id; ?> "><span class="badge bg-yellow"><i class="fa fa-pencil"></i> Update</span></a>&nbsp;&nbsp;&nbsp;
<!--                                            <a href="<?= Yii::$app->request->baseUrl . '/supplier/delete/' . $val->id; ?> "><span class="badge bg-red"><i class="fa fa-trash"></i> Delete</span></a>&nbsp;&nbsp;&nbsp;-->
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- .box-body -->

                </div>

            </div>
        </div>
    </section>
</div>
<?php JSRegister::begin(); ?>
<script>
    $(document).ready(function () {
        $("#example1").DataTable();
       
    });
</script>
<?php JSRegister::end(); ?>


