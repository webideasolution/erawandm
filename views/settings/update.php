<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\components\CustomPagination;

$this->title = 'Currency';

/* @var $this yii\web\View */
/* @var $model app\models\InvoiceIncoming */
/* @var $form ActiveForm */
?>
<div class="supplier-create">
    <div class="col-md-12">
<?php $form = ActiveForm::begin(['id' => 'settings', 'options' => ['enctype' => 'multipart/form-data', 'method' => 'post']]); ?>
        

        <div class="col-md-4">         <?= $form->field($model, 'value') ?> </div>

        <div class="form-group " >
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'style'=>"margin-top:23px "]) ?>
        </div>
<?php ActiveForm::end(); ?>
    </div>
</div><!-- invoice-create -->

