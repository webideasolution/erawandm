<td>
    <div class="form-group">
        <input type="text" id="flights-<?= $counter; ?>-flight_date" class="datepicker form-control" name="Flights[<?= $counter; ?>][flight_date]">
    </div>
</td>
<td>
    <div class="form-group">
        <input type="text" id="flights-<?= $counter; ?>-flight_id" class="form-control" name="Flights[<?= $counter; ?>][flight_id]" maxlength="50">
    </div>
</td>
<td width="20%">
    <div class="form-group">
        <input type="radio" name="Flights[<?= $counter; ?>][visa-on-arrival]" value="0"> VOA
        <input type="radio" name="Flights[<?= $counter; ?>][visa-on-arrival]" value="1" checked="checked"> PV
        <input type="radio" name="Flights[<?= $counter; ?>][visa-on-arrival]" F value="2"> Both
    </div>
</td>
<td>
    <div class="form-group">
        <input type="text" id="flights-<?= $counter; ?>-source" class="form-control" name="Flights[<?= $counter; ?>][source]">
    </div>
</td>
<td>
    <div class="form-group">
        <input type="text" id="flights-<?= $counter; ?>-destination" class="form-control" name="Flights[<?= $counter; ?>][destination]">
    </div>
</td>
<td>
    <div class="form-group">
        <input type="text" id="flights-<?= $counter; ?>-departure_time" class="form-control" name="Flights[<?= $counter; ?>][departure_time]" maxlength="20">
    </div>
</td>
<td>
    <div class="form-group">
        <input type="text" id="flights-<?= $counter; ?>-arrival_time" class="form-control" name="Flights[<?= $counter; ?>][arrival_time]">
    </div>
</td>
<td>
    <div class="text-center full-width-btn">
        <button type="button" class="remove-flight btn btn-danger"><i class="glyphicon glyphicon-minus"></i>Remove</button>
    </div>
</td>

