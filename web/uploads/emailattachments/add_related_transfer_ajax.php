<?php

use richardfan\widget\JSRegister;
?>

<?php
if (!empty($fields)):
    $flag_visa = FALSE;
    $flag_domestic = FALSE;
    ?>
    <?php foreach ($fields as $index => $value): ?>
        <?php
        if ($index == 0) {
            //first innternational flight
            if ($value[2] == 0) {
                //visa on arrival selected
                $pickup_time = strtotime(trim($value[6])) + 60 * 90;
            } else if ($value[2] == 1) {
                //Prestamped visa selected
                $pickup_time = strtotime(trim($value[6]));
            } else {
                //Both type of timing should occur in transfer
                $flag_visa = TRUE;
                $pickup_time = strtotime(trim($value[6]));
                $pickup_time_voa = strtotime(trim($value[6])) + 60 * 90;
                ?>

                <tr>
                    <td>
                        <div class="form-group">
                            <input type="text" id="transfers-<?= $counter; ?>-transfer_date" class="datepicker form-control" name="Transfers[<?= $counter; ?>][transfer_date]" value="<?= $value[0]; ?>">
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input type="text" id="transfers-<?= $counter; ?>-transfer_time" class="form-control" name="Transfers[<?= $counter; ?>][transfer_time]"  value="<?= date('H:i', $pickup_time_voa); ?>" >
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input type="text" id="transfers-<?= $counter; ?>-transfer_objective" class="form-control" name="Transfers[<?= $counter; ?>][transfer_objective]" value="PICKUP">
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input type="text" id="transfers-<?= $counter; ?>-transfer_source" class="form-control" name="Transfers[<?= $counter; ?>][transfer_source]" value="<?= $value[4]; ?>">
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input type="text" id="transfers-<?= $counter; ?>-transfer_destination" class="form-control" name="Transfers[<?= $counter; ?>][transfer_destination]">
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input type="text" id="transfers-<?= $counter; ?>-transfer_type" class="form-control" name="Transfers[<?= $counter; ?>][transfer_type]">
                        </div>
                    </td>
                    <td>
                        <div class="text-center full-width-btn">
                            <button type="button" class="remove-transfer btn btn-danger"><i class="glyphicon glyphicon-minus"></i>Remove</button>
                        </div>
                    </td>
                </tr>
                <?php $counter++; ?>
                <?php
            }
        } else if ($index + 1 == count($fields)) {
            //last international flight
            $pickup_time = strtotime(trim($value[5])) - 60 * 180;
        } else {
            //intermediate domestic flights
            $flag_domestic = TRUE;
            $pickup_time_voa = strtotime(trim($value[5])) - 60 * 180;
            $pickup_time = strtotime(trim($value[6]));
            ?>

            <tr>
                <td>
                    <div class="form-group">
                        <input type="text" id="transfers-<?= $counter; ?>-transfer_date" class="datepicker form-control" name="Transfers[<?= $counter; ?>][transfer_date]" value="<?= $value[0]; ?>">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input type="text" id="transfers-<?= $counter; ?>-transfer_time" class="form-control" name="Transfers[<?= $counter; ?>][transfer_time]"  value="<?= date('H:i', $pickup_time_voa); ?>" >
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input type="text" id="transfers-<?= $counter; ?>-transfer_objective" class="form-control" name="Transfers[<?= $counter; ?>][transfer_objective]" value="PICKUP">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input type="text" id="transfers-<?= $counter; ?>-transfer_source" class="form-control" name="Transfers[<?= $counter; ?>][transfer_source]">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input type="text" id="transfers-<?= $counter; ?>-transfer_destination" class="form-control" name="Transfers[<?= $counter; ?>][transfer_destination]" value="<?= $value[3]; ?>">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input type="text" id="transfers-<?= $counter; ?>-transfer_type" class="form-control" name="Transfers[<?= $counter; ?>][transfer_type]">
                    </div>
                </td>
                <td>
                    <div class="text-center full-width-btn">
                        <button type="button" class="remove-transfer btn btn-danger"><i class="glyphicon glyphicon-minus"></i>Remove</button>
                    </div>
                </td>
            </tr>
            <?php $counter++; ?>
            <?php
        }
        ?>
        <tr>
            <td>
                <div class="form-group">
                    <input type="text" id="transfers-<?= $counter; ?>-transfer_date" class="datepicker form-control" name="Transfers[<?= $counter; ?>][transfer_date]" value="<?= $value[0]; ?>">
                </div>
            </td>
            <td>
                <div class="form-group">
                    <input type="text" id="transfers-<?= $counter; ?>-transfer_time" class="form-control" name="Transfers[<?= $counter; ?>][transfer_time]"  value="<?= date('H:i', $pickup_time); ?>" >
                </div>
            </td>
            <td>
                <div class="form-group">
                    <input type="text" id="transfers-<?= $counter; ?>-transfer_objective" class="form-control" name="Transfers[<?= $counter; ?>][transfer_objective]" value="PICKUP">
                </div>
            </td>
            <td>
                <div class="form-group">
                    <input type="text" id="transfers-<?= $counter; ?>-transfer_source" class="form-control" name="Transfers[<?= $counter; ?>][transfer_source]" value="<?= $index + 1 == count($fields) ? '' : $value[4]; ?>">
                </div>
            </td>
            <td>
                <div class="form-group">
                    <input type="text" id="transfers-<?= $counter; ?>-transfer_destination" class="form-control" name="Transfers[<?= $counter; ?>][transfer_destination]" value="<?= $index + 1 == count($fields) ? $value[3] : ''; ?>">
                </div>
            </td>
            <td>
                <div class="form-group">
                    <input type="text" id="transfers-<?= $counter; ?>-transfer_type" class="form-control" name="Transfers[<?= $counter; ?>][transfer_type]">
                </div>
            </td>
            <td>
                <div class="text-center full-width-btn">
                    <button type="button" class="remove-transfer btn btn-danger"><i class="glyphicon glyphicon-minus"></i>Remove</button>
                </div>
            </td>
        </tr>
        <?php $counter++; ?>
    <?php endforeach; ?>
<?php endif; ?>





<?php JSRegister::begin(); ?>
<script>
    $("#flights-0-flight_id").val(<?= $counter; ?>);
</script>
<?php JSRegister::end(); ?>