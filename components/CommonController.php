<?php

namespace app\components;

use yii;
use yii\web\Controller;
use yii\helpers\BaseUrl;
use yii\helpers\Url;

class CommonController extends Controller {

    public $layout = 'main';

    public function goAdminhome() {
        return Yii::$app->getResponse()->redirect(BaseUrl::base());
    }

    public function beforeAction($action) {

        $public_access = ['login','view-voucher'];
        if (Yii::$app->user->isGuest && (Yii::$app->controller->id != 'auth' || !in_array(Yii::$app->controller->action->id, $public_access))) {
            $this->layout = 'login';
            $this->redirect(['auth/login']);
            return false;
        }
        return parent::beforeAction($action);
    }

}
