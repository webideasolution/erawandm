-----------------23-7-2018-----------

ALTER TABLE `invoice_incoming` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT NULL;


-----------------24-7-2018-----------

ALTER TABLE `supplier_tour_name` ADD `sic_adult_price` FLOAT(8,2) NULL AFTER `tourname`, ADD `sic_child_price` FLOAT(8,2) NULL AFTER `sic_adult_price`, ADD `ticket_adult_price` FLOAT(8,2) NULL AFTER `sic_child_price`, ADD `ticket_child_price` FLOAT(8,2) NULL AFTER `ticket_adult_price`;
ALTER TABLE `supplier_tour_name` DROP `sic_adult_price`, DROP `sic_child_price`, DROP `ticket_adult_price`, DROP `ticket_child_price`;


-----------------25-7-2018-------------
ALTER TABLE `activity` ADD `commission` FLOAT(8,2) NOT NULL AFTER `enroute_location`, ADD `cash_on_hand` FLOAT(8,2) NOT NULL AFTER `commission`;
